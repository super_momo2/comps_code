#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 10:50:42 2019

@author: molnarad
"""

import radynpy as radynpy
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from RHlib import RADYN_atmos


def animate(i):
    vz.set_ydata([el / 1e5 for el in rad.vz[i, :]])
    vz.set_xdata([el / 1e5 for el in rad.z[i, :]])
    Ly.set_ydata(I_Ly[i, :])
    Ha.set_ydata(I_Ha[i, :])
    Ca.set_ydata(I_Ca[i, :])
    fig.suptitle(f'Time = {rad.time[i]:5.2f} [seconds]')


simulation_name = 'radyn_1'

rad = RADYN_atmos('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
                  + 'Comps_2/comps_code/' + simulation_name + '.cdf')

#fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(10, 6), dpi=250)



mu_Idx = -1# for straight up

wl_Ly, I_Ly = rad.line_intensity_with_cont(0, mu_Idx)
wl_Ha, I_Ha = rad.line_intensity_with_cont(2, mu_Idx)
wl_Ca, I_Ca = rad.line_intensity_with_cont(20, mu_Idx)

#vz = ax[0, 0].plot([ el / 1e5 for el in rad.z[0, :]],
#               [el / 1e5 for el in rad.vz[0, :]], 'r.--', lw=2)[0]

# Calculate the total radiative losses

indices_bf = []
for ii in range(41):
    if rad.line_intensity_with_cont(ii, -1) != 1:
        indices_bf.append(ii)

rl = np.zeros(rad.rad.outint.shape[0])

for ii in indices_bf:
    wl, I = rad.line_intensity_with_cont(ii, mu_Idx)
    I  = I * 1e-7 * 1e4  # make the I in J/(s.sr.A.m-2)

    #coeff = 4 * 3.1415
    dI = np.zeros(I.shape[0])
    for ll in range(I.shape[0]):
        dI[ll] = sp.integrate.simps(I[ll, :], wl)

    print(f'The rad losses are {np.mean(dI)} for transition {ii}')
    rl = rl + dI

fig, ax = plt.subplots(figsize=(6,3.5), dpi=250)

plt.xlabel('Time [seconds]')
plt.ylabel('Radiative losses')
plt.plot(rad.rad.time,rl)
plt.yscale('log')
plt.show()

