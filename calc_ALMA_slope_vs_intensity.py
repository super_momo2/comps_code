#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Calculate the slopes of different diagnostics of their PSDs

@author: molnarad
"""

import numpy as np
import scipy as sp
import matplotlib as mp
import matplotlib.pyplot as pl
from scipy import io
from scipy import signal

#TURBO CHARGE YOUR PYTHON -- Make it parallel
import multiprocessing
from joblib import Parallel, delayed

import matplotlib.colors as mcolors
from skimage.transform import downscale_local_mean


pl.style.use('science')

from numpy.random import multivariate_normal

def calc_median_hist(yedges, n):
    '''
    Calculate the median of a histogram column.

    Parameters
    ----------
    yedges : TYPE
        DESCRIPTION.
    n : TYPE
        DESCRIPTION.

    Returns
    -------
    y_edge : TYPE
        DESCRIPTION.

    '''
    y_edge = 0
    n_count = 0
    n_total = np.sum(n)

    while n_count < n_total/2.0:
        n_count = n_count + n[y_edge]
        y_edge = y_edge + 1

    return y_edge


def load_ALMA_Band3(time_block):
    '''

    Parameters
    ----------
    time_block : int
        Which timeblock of the ALMA dataset to be used for the analysis (2-5);

    Returns
    -------
    freqs: float array
        Frequency grid
    ALMA6_fft_cut: float array
        PSD of the pixels chosenc

    '''

    file_name  = ('ALMA_band3_t' + str(time_block)
                  +'_csclean_feathered.sav')
    sav_file   = io.readsav(data_dir + 'ALMA/' + file_name)
    ALMA3_data = np.zeros((360, 360, 299))

    for ii in range(299):
        ALMA3_data[:, :, ii] = np.rot90(sav_file['maps'][ii][0].transpose())

    cut = 90
    dx1 = ALMA3_data.shape[0]//2 - cut
    dx2 = ALMA3_data.shape[0]//2 + cut
    ddx = dx2 - dx1

    ALMA3_fft = [signal.periodogram(el-np.mean(el))[1]
                           for line in ALMA3_data[dx1:dx2, dx1:dx2, :]
                                                   for el in line]

    ALMA3_fft_cut = np.reshape(ALMA3_fft, (ddx**2,
                                           ALMA3_data.shape[2]//2 + 1))
    freqs    = signal.periodogram(ALMA3_data[0, 0, :], fs = 1/2.0)[0]
    freqs    = freqs * 1e3

    return np.array(freqs), np.array(ALMA3_fft_cut), np.array(ALMA3_data[dx1:dx2,
                                                                         dx1:dx2,
                                                                         :])


def load_ALMA_Band6(time_block):
    '''

    Parameters
    ----------

    time_block : int
        Which timeblock of the ALMA dataset to be used for the analysis (2-5);

    Returns
    -------
    freqs: float array
        Frequency grid
    ALMA6_fft_cut: float array
        PSD of the pixels chosenc


    '''

    file_name  = ('aligned_ALMA_20170423_band6_dc_t' + str(time_block)
                  +'_feathered_snr3.sav')
    sav_file   = io.readsav(data_dir + 'ALMA/' + file_name)
    map_ex     = sav_file['maps'][0][0].transpose()
    ALMA6_data = np.zeros((map_ex.shape[0],
                           map_ex.shape[1],
                           238))

    for ii in range(238):
        ALMA6_data[:, :, ii] = np.rot90(
            sav_file['maps'][ii][0].transpose())

    cut = 60

    dx1 = ALMA6_data.shape[0]//2 - cut
    dx2 = ALMA6_data.shape[0]//2 + cut

    ddx = dx2 - dx1

    ALMA6_fft = [signal.periodogram(el-np.mean(el))[1]
                           for line in ALMA6_data[dx1:dx2, dx1:dx2, :]
                                                   for el in line]

    ALMA6_fft_cut = np.reshape(ALMA6_fft, (ddx**2,
                                           ALMA6_data.shape[2]//2 + 1))

    freqs    = signal.periodogram(ALMA6_data[0, 0, :], fs = 1/2.0)[0]
    freqs    = freqs * 1e3
    print(f"dx1 and dx2 are {dx1} and {dx2}")
    return np.array(freqs), np.array(ALMA6_fft_cut), np.array(ALMA6_data[dx1:dx2,
                                                                         dx1:dx2,
                                                                         :])


def construct_name(spec_property, time):
    '''
    Generate names for the save files to be retrieved from the
    .sav library.

    Parameters
    ----------
    spec_property : string
        Spectral property to be investigated.

    time : string
        Time of observations

    Returns
    -------
    name : string
        DESCRIPTION.

    '''
    if int(time) < 150000:
        target = '.23Apr2017.target1.all.'
    else:
        target = '.23Apr2017.target2.all.'

    name = ("fft.nb."+ spec_property + target
            + filter_dic[time] + '.ser_'
            + str(time) + ".sav")
    return name

def load_data(spec_obs, time):
    '''
    Load the data from the .sav files
    Parameters
    ----------
    time: int
        time of the observation to be plot
    spec_obs : string
        one of spec_obs

    Returns
    -------
    fft_freq: float array
        frequencies of the PSDs
    fft_data: 2D float array
        PSDs
    '''
    data_file     = construct_name(spec_obs, time)
    pix_lim       = 60
    dx            = 1

    if filter_dic[time] == "8542":
        corr_coeff = ca_v_coeff
    elif filter_dic[time] == "6563":
        corr_coeff = ha_v_coeff

    fft_dict = sp.io.readsav(data_dir + 'IBIS/'
                             + data_file, python_dict=True)
    fft_data = downscale_local_mean(fft_dict['fft_data'], (1, 2, 2))
    fft_data = fft_data[:, pix_lim:-pix_lim:dx, pix_lim:-pix_lim:dx]
    fft_freq = fft_dict['freq1'] * 1e3 #make it in mHz

    fft_data = fft_data.reshape(fft_data.shape[0],
                                fft_data.shape[1]*fft_data.shape[2])
    fft_data = np.swapaxes(fft_data, 0, 1)
    print('Loaded ' + spec_obs + f' {filter_dic[time]}')

    return np.array(fft_freq), np.array(fft_data)


filter_dic = {141344:"8542", 141807:"6563", 151321:"8542", 151857:"6563",
              153954:"8542", 154624:"6563", 170444:"8542", 171115:"6563",
              181227:"6563", 2:"", 4:""}

data_dir        = ('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
                   + 'Comps_2/comps_data/')

ALMA3_time_block = 2
ALMA6_time_block = 2


ca_v_coeff = 1233.5
ha_v_coeff = 2089

fig, ax = pl.subplots(2, 1, figsize=(4, 6))
fig.set_dpi(300)
fontsize_legend = 7
fig.subplots_adjust(hspace=.205, wspace=.225)


data_int   = ['ALMA Band 3', 'ALMA Band 6']
time_stamp = [2, 2]

for el in range(2):

    if el == 0: # Plot ALMA Band 3
        freqs, data, ALMA_data = load_ALMA_Band3(time_stamp[el])

    else: # Plot ALMA Band 6
        freqs, data, ALMA_data = load_ALMA_Band6(time_stamp[el])

    data_len = data.shape[0]
    print(f"Data length is {data.shape}")

    if el == 0:
        freq_0 = 4
        freq_1 = 28
    if el == 1:
        freq_0 = 3
        freq_1 = 23

    print(f"Frequencies range for plot #{el+1} from {freqs[freq_0]:.2f} mHz"
          + f" to {freqs[freq_1]:.2f} mHz")

    # y = slopes[0] * x + slopes[1]

    slopes = Parallel(n_jobs=4)(delayed(np.polyfit)(np.log10(freqs[freq_0:freq_1]),
                                                    np.log10(data[ii, freq_0:freq_1]),
                                                    1) for ii
                                in range(data_len))
    slopes = np.array(slopes)

    total_power = Parallel(n_jobs=4)(delayed(np.sum)(data[ii, freq_0:freq_1])
                                     for ii in range(data_len))

    gamma = 1.1


    #Left column would be power vs intensity

    if el == 0:
        range1 = [[6500, 11500], [5.8, 7.0]]
        range2 = [[6500, 11500], [-4.2, 0]]
    if el == 1:
        range1 = [[6500, 9500], [5.9, 7.5]]
        range2 = [[6500, 9500], [-4.2, 0]]

    h1, xedges1, yedges1, im = ax[el].hist2d(np.max(ALMA_data,
                                                        axis=2).ravel(),
                                                np.log10(total_power),
                                                bins=[70, 70],
                                                range=range1)


    #Right column would be slope vs intensity
    # h2, xedges2, yedges2, im = ax[el, 1].hist2d(np.max(ALMA_data,
    #                                                    axis=2).ravel(),
    #                                             slopes[:, 0],
    #                                             bins=[70, 70],
    #                                             range=range2)
    # axobj.imshow(h, extent=[xedges[0], xedges[-1],
    #                         yedges[0], yedges[-1]], origin='lower')

    # Log weighted average PSD energy
    # mean_P = [np.log10(np.sum(el* 10**yedges[:-1])/np.sum(el)) for el in h]

    #Linear weighted PSD
    # mean_P = ([np.sum(el[15:-15] * yedges[15:-16] )/np.sum(el[15:-16]) for el in h])

    mean_P1 = np.array([calc_median_hist(yedges1, el) for el in h1])
    #mean_P2 = np.array([calc_median_hist(yedges2, el) for el in h2])
    mean_P1 = yedges1[mean_P1]
    #mean_P2 = yedges2[mean_P2]
    ofc = np.array([[8, -10], [10, -10]])
    ax[el].plot(xedges1[(ofc[el, 0]+1):ofc[el, -1]],
                mean_P1[ofc[el, 0]:ofc[el, -1]], 'r--',
               label=(str(data_int[el])
                      + ' ' + str(filter_dic[time_stamp[el]])),
               linewidth=2.5)
    if el==0:
        title = "a) Band 3"
        ax[el].text(6600, 6.9, title, color="white",
                    fontsize=fontsize_legend*2)
    else:
        title = "b) Band 6"
        ax[el].text(6600, 7.35, title, color="white",
                    fontsize=fontsize_legend*2)
    #ax[el].set_title(title)

    #ax[el, 1].plot(xedges2[1:], mean_P2, 'r--',
    #           label=(str(data_int[el])
    #                  + ' ' + str(filter_dic[time_stamp[el]])),
    #           linewidth=2.5)

    #ax[el, 1].legend(prop={"size":fontsize_legend})
    #ax[el, 1].set_ylim(-4, -.5)

    ax[el].set_ylabel("Log10(PSD power [K$^2$/Hz])")
    #ax[el, 1].set_ylabel("PSD slope")


#ax[0, 0].set_xlim(2e5, 4e6)
#ax[1, 0].set_xlim(5e6, 5e6)

ax[1].set_xlabel("Brightness Temperature $T_b$ [K]")
#ax[1, 1].set_xlabel("Brightness Temperature Tb [K]")

ax[1].set_ylabel("Log10(PSD power [K$^2$/Hz])")
# ax[1, 1].set_ylabel("PSD power [K$^2$/Hz]")
pl.tight_layout()

pl.savefig("../../../Manuscripts/2021a_Molnar_et_al_High_freq/git/git/"
           +"Figures/ALMA_PSDpower_vs_temperature.png")

#pl.ylim(0, 0.00075)
pl.show()
