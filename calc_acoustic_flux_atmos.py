"""
Calculate the acoustic flux across a RADYN atmosphere

MoMo - 12/9/19
"""

import numpy as np
import radynpy as rp

import radynpy as radynpy
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from RHlib import RADYN_atmos
from scipy import signal

simulation_name = 'radyn_vtab'
data_dir = ('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
            + 'Comps_2/comps_code/')

rad = RADYN_atmos(data_dir + simulation_name + '.cdf')

rad.calc_Flux_equidistant(1200, 400)

plt.plot(rad.z_eq,
         [F/1000 for F in rad.Flux_eq], 'b.--')  # make in W/m^2
plt.yscale('log')
plt.ylabel('Acoustic flux [erg/cm$^{-2}$]')
plt.xlabel('Height [Mm]')
plt.savefig('../RADYN_calculations/Acoustic_flux.png')
plt.show()

power_spectrum_height = [signal.periodogram(el-np.mean(el), fs=2) for
                         el in np.swapaxes(rad.v_eq[700:-1, :], 0, 1)]

base_n = 0
high_n = 135

print(f'The low level is at {rad.z_eq[base_n]/1e8} Mm')
print(f'The high level is at {rad.z_eq[high_n]/1e8} Mm')

plt.plot(power_spectrum_height[base_n][0],
         power_spectrum_height[base_n][1], 'b.--', label=f'{rad.z_eq[base_n]/1e8:.2f}')
plt.plot(power_spectrum_height[high_n][0],
         power_spectrum_height[high_n][1], '.--', color='orange',
         label=f'{rad.z_eq[high_n]/1e8:.2f}')

x = np.linspace(1e-4, 5.18e-3, num=1e3)
y0 = 1e-20
y1 = 1e30


plt.ylabel('Power [cm$^2$/s$^2$/Hz]')
plt.xlabel('Frequency [Hz]')
plt.yscale('log')
plt.xscale('log')
plt.fill_between(x, y0, y1, facecolor='red', alpha=0.5)
plt.ylim(0.25*np.min(power_spectrum_height[high_n][1][1:-1].flatten()),
         2*np.max(power_spectrum_height[high_n][1].flatten()))
plt.grid()
plt.legend(title='Height')
plt.savefig("../RADYN_calculations/Power_V_vs_height.png")
plt.show()
