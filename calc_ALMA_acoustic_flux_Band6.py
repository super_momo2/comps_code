#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 15:07:41 2020

Calculate the acoustic flux from the ALMA observations Band 6

@author: molnarad
"""

import numpy as np
import scipy as sp
from scipy.ndimage import gaussian_filter
import matplotlib.pyplot as pl
from astropy.io import fits
from test_color_maps import diverging_colors, qualitative_colors
from matplotlib.colors import ListedColormap
from matplotlib.cm import register_cmap
from scipy import io
from scipy import signal

from PYBIS import construct_name, load_fft_data
# Load data

def calc_median_hist(yedges, n):
    '''
    Calculate the median of a histogram column.
    
    Parameters
    ----------
    yedges : TYPE
        DESCRIPTION.
    n : TYPE
        DESCRIPTION.

    Returns
    -------
    y_edge : TYPE
        DESCRIPTION.

    '''    
    y_edge = 0
    n_count = 0
    n_total = np.sum(n)
    
    while n_count < n_total/2.0:
        n_count = n_count + n[y_edge]
        y_edge = y_edge + 1 
    
    return y_edge



ds = 1
ds3 = ds

max_freq = 60e-3
dfreq   = 7e-3    
num_freqs = int(max_freq / dfreq) + 1


# Load ALMA data
data_dir  = ("/Users/molnarad/CU_Boulder/Work/Chromospheric_business/"
             + "Comps_2/comps_data/ALMA/")
file_name3 = "ALMA_20170423_band6_dc_t2_feathered_snr3.sav"

alma_sav3 = io.readsav(data_dir+file_name3)
alma_data_temp3 = alma_sav3["maps"]


ALMA_num_t_steps3 = len(alma_data_temp3)

FOV2_IBIS_center_x = -76.98
FOV2_IBIS_center_y = 248.99
IBIS_dx = 0.096
IBIS_dy = 0.096
IBIS_num_x = 9e2
IBIS_num_y = 9e2

rIBIS = [FOV2_IBIS_center_x - IBIS_num_x*IBIS_dx/2,
         FOV2_IBIS_center_x + IBIS_num_x*IBIS_dx/2,
         FOV2_IBIS_center_y - IBIS_num_y*IBIS_dy/2,
         FOV2_IBIS_center_y + IBIS_num_y*IBIS_dy/2]

FOV2_ALMA_Band3_center_x = -79.037
FOV2_ALMA_Band3_center_y = 252.007
FOV2_ALMA_Band6_center_x = -68.68
FOV2_ALMA_Band6_center_y = 258.83
ALMA_num_x = 360 - 2*ds
ALMA_cutout_dx3 = ALMA_num_x
ALMA_num_y = 360 - 2*ds
ALMA_Band3_dx = 0.33
ALMA_Band6_dx = 0.14


rALMA3 = [FOV2_ALMA_Band3_center_x - ALMA_Band3_dx*ALMA_num_x/2,
          FOV2_ALMA_Band3_center_x + ALMA_Band3_dx*ALMA_num_x/2,
          FOV2_ALMA_Band3_center_y - ALMA_Band3_dx*ALMA_num_x/2,
          FOV2_ALMA_Band3_center_y + ALMA_Band3_dx*ALMA_num_x/2]

#rIBIS1 = rALMA3
#rIBIS1  = rIBIS

rALMA6 = [FOV2_ALMA_Band6_center_x - ALMA_Band6_dx*ALMA_num_x/2,
          FOV2_ALMA_Band6_center_x + ALMA_Band6_dx*ALMA_num_x/2,
          FOV2_ALMA_Band6_center_y - ALMA_Band6_dx*ALMA_num_x/2,
          FOV2_ALMA_Band6_center_y + ALMA_Band6_dx*ALMA_num_x/2]

alma_data3 = np.zeros((ALMA_cutout_dx3, ALMA_cutout_dx3, 
                       ALMA_num_t_steps3))

for el in range(ALMA_num_t_steps3):
    alma_data3[:, :, el] = alma_data_temp3[el][0][ds3:-1*ds3, ds3:-1*ds3]

# Load the T coefficient

with np.load("T_ALMA_coeffs.npz") as hdu:

    T_ALMA3    = hdu["T_ALMA3"]
    T_ALMA6    = hdu["T_ALMA6"]
    freq_range = hdu["freq_range"]


# Compute the PSD on the spacing of the freq_range

fft_data3 = np.zeros((ALMA_cutout_dx3, ALMA_cutout_dx3,
                       ALMA_num_t_steps3//2 + 1))

for ii in range(ALMA_cutout_dx3):
    for jj in range(ALMA_cutout_dx3):
         freq_ALMA3, fft_data3[ii, jj, :] = signal.periodogram(alma_data3[ii, jj, :],
                                                        fs = 1/2.0)

dfreq_array = freq_ALMA3
ddfreq = int(dfreq / dfreq_array[1])

ALMA_3_PSD_mean = np.zeros((ALMA_cutout_dx3, ALMA_cutout_dx3,
                            num_freqs))

for ii in range(ALMA_cutout_dx3):
    for jj in range(ALMA_cutout_dx3):
        ALMA_3_PSD_mean[ii, jj, :] = [np.mean(fft_data3[ii, jj, i*ddfreq:(i*ddfreq 
                                                                  + ddfreq)]) 
                                      for i in range(num_freqs)]

# Compute the flux


Acoustic_flux_Band3 = np.zeros((ALMA_cutout_dx3, ALMA_cutout_dx3))


for ii in range(ALMA_cutout_dx3):
    for jj in range(ALMA_cutout_dx3):
        Acoustic_flux_Band3[ii, jj] = (T_ALMA6[-2, 1:] 
                                       * ALMA_3_PSD_mean[ii, jj, 1:]
                                       * dfreq).sum()          
# Plot the flux 
 cutout = 1 

fig, ax = pl.subplots(1, 2, dpi=300)
im1 = ax[0].imshow(np.log10(Acoustic_flux_Band3[cutout:-1*cutout,
                                                cutout:-1*cutout]), 
                   origin=0, cmap="bwr", vmin=2, vmax=4,
                   extent=[rALMA6[0], rALMA6[1], rALMA6[2], rALMA6[3]])
fig.colorbar(im1, label="Log$_{10}$[Acoustic Flux [W/m$^2$]] ", ax=ax[0],
             shrink=0.6)
ax[0].set_ylabel("Solar Y [arcsec]")
ax[0].set_xlabel("Solar X [arcsec]")

im2 = ax[1].imshow(np.median(alma_data3[cutout:-1*cutout, 
                                        cutout:-1*cutout, :], axis=2), 
                   cmap="plasma", origin=0,
                   extent=[rALMA6[0], rALMA6[1], rALMA6[2], rALMA6[3]], 
                   vmin=5000, vmax=9000)
fig.colorbar(im2, label="Temperature [K]", ax=ax[1], 
             shrink=0.6)
ax[1].set_xlabel("Solar X [arcsec]")
fig.suptitle("Band 6")
pl.tight_layout()
pl.show()


pl.figure(dpi=250, figsize=(7.1, 4.4))
h1, xedges1, yedges1, im = pl.hist2d(np.mean(alma_data3[cutout:-1*cutout, 
                                                       cutout:-1*cutout, :], 
                                     axis=2).flatten(), 
                                     np.log10(Acoustic_flux_Band3[cutout:-1*cutout,
                                              cutout:-1*cutout]).flatten(), 
                                     bins=(50, 50),
                                     range=[[5500, 8500], [1.8, 3.5]])
mean_P1 = np.array([calc_median_hist(yedges1, el) for el in h1])
mean_P1 = yedges1[mean_P1]
pl.plot(xedges1[1:], mean_P1, 'r--', 
        linewidth=2.5)

pl.xlabel("Brightness Temperature [K]")
pl.ylabel("Log$_{10}$[Acoustic Flux [W/m$^2$]]")
pl.title("Band 6")

pl.savefig("/Users/molnarad/CU_Boulder/Work/Manuscripts/"
           + "2020b_Molnar_et_al_High_freq/git/git/Figures/"
           + "ALMA_AC_flux_vs_Tb.png")