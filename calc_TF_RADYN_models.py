#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 11:34:38 2020

Calc RADYN TF from models

@author: momo
"""


import numpy as np
import matplotlib.pyplot as plt
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog
from scipy import signal

RADYN_models = ["radyn_ulmschneider_250_low_dt.cdf",
                "radyn_ulmschneider_625_low_dt.cdf",
                "radyn_ulmschneider_1200_low_dt.cdf",
                "radyn_ulmschneider_3000_low_dt.cdf",
                "radyn_ulmschneider_7500_low_dt.cdf", 
                "radyn_ulmschneider_19000_low_dt.cdf"]

data_dir = "/mnt/Data/Comps_2/comps_data/RADYN_runs/"

labels = ["model_250", "model_625", "model_1200",
          "model_3000", "model_7500", "model_19000"]

sp_line_index = 20 # 20 - Ca II 8542 Å; 2 - Ha 6563 Å

start_index_all = [600, 600, 700, 750, 500, 500]
end_index_all   = [-10, -10, -100, -150, -660, -660]
dt_sim      = 3
count_every = 1

height_v = 1.15e3 


if sp_line_index == 2:
    vel_coefficient = 3e5/6563
if sp_line_index == 20:
    vel_coefficient = 3e5/8542
    


figure, ax1 = plt.subplots(2, 1, dpi=250, figsize=(5, 7))


for el in [0, 1, 2, 3, 4, 5]:
    start_index = start_index_all[el]
    end_index   = end_index_all[el] 
    
    rad = RADYN_atmos(data_dir + RADYN_models[el])
    
    rad.calc_sp_line(sp_line_index)

    int_grid_scale = np.linspace(rad.sp_line_wave[28], rad.sp_line_wave[-28],
                             num=100)
    line_intensity_int = [np.interp(int_grid_scale, rad.sp_line_wave, I) for I in 
                      rad.sp_line_I]
    line_intensity_int = np.array(line_intensity_int)

    Ha_velocity = [calc_v_lc(int_grid_scale, I, 10, -10, 13)[0] for I
               in line_intensity_int[start_index:end_index:count_every, 1:-1]]

    Ha_velocity_metric = vel_coefficient * (np.array(Ha_velocity)
                                            - np.mean(Ha_velocity))
    
    for el1 in range(len(Ha_velocity_metric)):
        if (Ha_velocity_metric[el1]) > 10:
            Ha_velocity_metric[el1] = 0
        elif (Ha_velocity_metric[el1]) < -10:
            Ha_velocity_metric[el1] = 0

    Ha_v_freq, Ha_v_PSD = signal.periodogram(Ha_velocity_metric,
                                         fs=1/dt_sim/count_every) 

    
    rad.calc_Flux_PSD(int((start_index + end_index)/2), 
                      int((end_index - start_index)/2), 
                      height_v, dt_sim)
    
    
    ax1[0].plot(rad.Flux_eq_PSD_freq[1:]*1e3, rad.Flux_eq_PSD[1:], '.--', 
             label=labels[el])
    
    ax1[1].plot(Ha_v_freq[1:]*1e3, Ha_v_PSD[1:]/1e3, '.--', 
                label=labels[el])
    
    

ax1[0].grid()
ax1[0].set_yscale("log")
#ax1[0].set_ylim(1e-4, 1e-3)
ax1[0].set_xlim(2, 60)
ax1[0].set_ylabel(f"Acoustic flux [W$^2$/m$^4$/mHz]")
ax1[0].set_xlabel("Frequency [mHz]")
ax1[0].legend(fontsize=6)

ax1[1].grid()
ax1[1].set_yscale("log")
#ax1.set_ylim(1e1, 1e4)
ax1[1].set_xlim(.5, 60)
ax1[1].set_ylabel("Observed v$_{rms}$ in 854.2 $\AA$ [km$^2$/s$^2$/mHz]")
ax1[1].set_xlabel("Frequency [mHz]")
ax1[1].legend(fontsize=6)

#plt.tight_layout()
#plt.savefig("")
plt.show()

     
