#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 00:11:19 2020

Plot the RF_vel from the RF tests for RADYN atmospheres  

@author: molnarad
"""

import matplotlib.pyplot as pl
from astropy.io import fits
from RHlib import calc_v_lc, RADYN_atmos
import numpy as np

RF_test_names = ["radyn_ulmschneider_100_rf_test.fits",
                 "radyn_ulmschneider_250_rf_test.fits",
                 "radyn_ulmschneider_625_rf_test.fits",
                 "radyn_ulmschneider_1200_rf_test.fits",
                 "radyn_ulmschneider_3000_rf_test.fits",
                 "radyn_ulmschneider_7500_rf_test.fits",
                 "radyn_ulmschneider_19000_rf_test.fits",
                 "radyn_ulmschneider_47000_rf_test.fits"]

RADYN_models = ["radyn_ulmschneider_100.cdf",
                "radyn_ulmschneider_250.cdf",
                "radyn_ulmschneider_625.cdf",
                "radyn_ulmschneider_1200.cdf",
                "radyn_ulmschneider_3000.cdf",
                "radyn_ulmschneider_7500.cdf",
                "radyn_ulmschneider_19000.cdf",
                "radyn_ulmschneider_47000.cdf"]

labels = ["model_100", "model_250", "model_625", "model_1200",
          "model_3000", "model_7500", "model_19000", 
          "model_47000"]

data_dir = "/Users/molnarad/Desktop/rh/results/"
model_data = "/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_data/RADYN_models/"

pl.figure(dpi=200)
i = 0
vel_coefficient = 3e5/854.2

for el in range(len(RF_test_names)):
    rad = RADYN_atmos(model_data + RADYN_models[el])
    with fits.open(data_dir + RF_test_names[el]) as hdu:
        data = hdu[0].data 
    wave_table   = data[0, 250:-85, 0, 0]
    I            = np.swapaxes(data[1, 250:-85, 0, :], 0, 1)
    vel_response = [calc_v_lc(wave_table, el, 10, -10, 9)[0] for
                    el in I]
    vel_response = np.array(vel_response)
    vel_response -= np.mean(vel_response[0:10])
    vel_response *= vel_coefficient 
    pl.plot(rad.z[1000, :]/1e5, vel_response/np.amax(vel_response),
            label=labels[i])
    i+=1

pl.xlim(550, 1600)
pl.xlabel("Height [km]")
pl.ylabel("8542 ${\AA}$ RF for v$_{z}$ from lc-fit method")
pl.grid()
pl.legend()
pl.show()
    