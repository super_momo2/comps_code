# Comps_code

This is repository for the project **'High frequency observations of the chromopshere with IBIS, ALMA and IRIS'** which is supposed to fulfill
the requirement for the second Comprehensive exam of *Momchil Molnar, CU Boulder*.

Below is a short description of the programs in this repo and their purpose. 


1. **Powerspec_GUI.py**   - GUI for visualizing the Power Spectra from IBIS 
2. **power_uncertain.py** - Calculate the uncertainties for a Power Spectrum
3. **TVS.py**             - Calculate the TVS of a spectral line (Fullerton, et al. 1996, ApJS, 103, 475)
4. **mfgs.py**            - MFGS technique for evaluating seeing conditions (Deng,H. et al., 2015, Sol. Phys., 290:1479-1489)

The contents for the paper are in the directory *comps_paper/*.