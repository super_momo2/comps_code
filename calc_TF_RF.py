#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 09:51:31 2019

Create test Atmospheres for TF and RF after N. Bellot Gonzalez

@author: molnarad
"""

from RHlib import Atmos_height, RF_vel_calibration
from matplotlib import pyplot as plt

case = 'RF'
atmosphere = 'FALC'

# First compute the RF

data_dir = '/Users/molnarad/Desktop/rh/results/'

Ha_RF = RF_vel_calibration(data_dir + 'rh_spect.fits',
                           1, 854.2)
Ha_RF.calculate_RF(True)
plt.plot(Ha_RF.RF)
plt.show()
