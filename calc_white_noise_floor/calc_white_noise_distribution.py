#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 22:56:46 2020

@author: molnarad
"""


import numpy as np 
import scipy as sp
import matplotlib.pyplot as pl

from RHlib import *
from scipy import signal, io, interpolate
from PYBIS import count_zeroes_in_array
from scipy.interpolate import interp1d
#TURBO CHARGE YOUR PYTHON -- Make it parallel 
import multiprocessing 
from joblib import Parallel, delayed 
import time 

ADU     = 2.5 # e-/DN
cs      = 3e5 # km/s
waveHa  = 6562.8 #Å
waveCa  = 8542.0 #Å

class spectral_data():
    '''Class for computing the rms velocity noise from the
      spectral data
    '''
    
    def __init__(self, wave, I, ADU, cadence, dl):
        '''Initiate the class
            Inputs:
            -- wave: ndarray[N_waves], wavelenght grid;
            -- I: ndarray[N_waves], intensity profile;
            -- ADU: float, Analog-Digital-unit conversion of the
            camera used;
            -- cadence: float, cadence of the observation;
            -- dl: float, regular wavelength grid we're remapping 
            the data on; 
            '''
        
        self.waves = wave
        self.I    = I
        self.c    = 3e5
        self.ADU  = ADU
        self.cadence = cadence
        self.dl   = dl
        
    def interp_waves_reg_grid(self):
        self.waves_interp = np.linspace(self.waves[0], self.waves[-1], 
                                        num=int((self.waves[-1] 
                                             - self.waves[0])/self.dl))
        
    
    def interp_I_reg_grid(self):
        self.interp_waves_reg_grid()
        self.noisyI_interp = np.zeros((self.noisyI.shape[0], 
                                        self.waves_interp.shape[0]))
        for i in range(self.noisyI.shape[0]):
            f_I = interp1d(self.waves, 
                           self.noisyI[i, :], kind='cubic')
            self.noisyI_interp[i, :] = f_I(self.waves_interp) 
    
    def calc_noise(self):
        #Calculate the noise floor for each intensity point
        self.noise = np.array([np.sqrt(el/ADU) for el in self.I])
    
    
    def calc_velocity(self):
        mean_wave = np.mean(self.measured_prop)
        self.measured_prop -= mean_wave
        self.velocity = self.c * self.measured_prop / mean_wave
        
        
    def calc_rms_vel(self, N_samples, function, *args):
        '''
        Produce N_sample random samples and calc the v_rms
        Input: 
            -- N_samples -- number of random samples
            -- function pointer to the method for measuring 
               velocity (v_cog or v_lc);
            -- 

        return: 
           None
        ''' 
        self.N_samples = N_samples
        self.calc_noise()
        
        noisyI = [(self.I + np.random.poisson(lam=self.noise))
                  for el in range(N_samples)]
        self.noisyI = np.array(noisyI)
        
        self.interp_I_reg_grid()
        
        self.measured_prop = [function(self.waves_interp, el, 
                                        *args) for 
                              el in self.noisyI_interp]
        
        self.measured_prop = np.array(self.measured_prop)
        self.calc_velocity()
        
        self.ff, self.Pxx = signal.periodogram(self.velocity,
                                               self.cadence) 
    
    def calc_rms(self, N_samples, function, *args):
        # Produce N_sample random samples and calc the v_rms
        
        self.N_samples = N_samples
        self.calc_noise()
        
        noisyI = [(self.I + np.random.poisson(lam=self.noise))
                  for el in range(N_samples)]
        self.noisyI = np.array(noisyI)
        self.measured_prop = [ function(self.waves, el, *args) for 
                              el in self.noisyI]
        
        self.measured_prop = np.array(self.measured_prop)        
        self.ff, self.Pxx = signal.periodogram(self.measured_prop,
                                               self.cadence) 
    
    
    def plot_results_vel(self):
        # Plot both velocity and PSD of the parameter measured
        
        fig, ax = plt.subplots(2,1)
        fig.dpi = 250
        fig.figsize=(10,10)
        ax[0].plot(np.linspace(0, (self.N_samples-1)*self.cadence,
                               num=(self.N_samples)),
                   self.velocity)
        
        ax[0].set_xlabel('Time [s]')
        ax[0].set_ylabel('Measured quantity [km/s]')
        
        ax[1].loglog(self.ff[1:], self.Pxx[1:]/1000) #.mHz-1
        ax[1].set_xlabel('Frequency [mHz]')
        ax[1].set_ylabel('Power [km$^2$.s$^{-2}$.mHz$^{-1}}$]')
        
        for el in ax:
            el.grid(alpha=0.5)
        
        plt.show()
        
def create_interp_data(intensityFile, waveFile, reg_grid_waves):
    
    intensity   = np.loadtxt(intensityFile).transpose()
    waves       = np.loadtxt(waveFile).transpose() + waveHa

    noised = [np.sqrt(el/ADU) for el in intensity]
    noise = [np.random.normal(0,scale=el) for el in noised]
    noisyI = noise + intensity
    reg_grid_I     = np.interp(reg_grid_waves, waves, noisyI)
    return reg_grid_I

def compute_noise():
    ROS = ["plage", "network", "fibrils", "internetwork", "penumbra"]
    data_dir = ("/home/momo/Comps_2/") 
    N_samples = 150
    dx = 50
    cadence   = 1/3.5
    dl        = 0.05 # Å is the spacing of the 
    freq      = np.linspace(0, cadence/2, num = int(N_samples/2 + 1))
    filename  = "Average_ROS_profiles.npz"
    Pxx       = np.zeros((len(ROS), int(N_samples/2 + 1)))
    # masks = np.load(data_dir + 'Masks_FOV2.npz')

    aa = np.load(data_dir+filename)
    
        
    waves = aa["waves"]
    I     = aa["spectral_profiles"]

    for el in range(0, len(ROS)):


        
       # print(I.shape, waves.shape)
       #  waves_1 = np.append(waves, waves[-1]+waves[1]-waves[0])
       #  
       #  mask_zeroes = count_zeroes_in_array(mask)
       #  coeff_zeroes = mask.shape[0]**2 /(mask.shape[0]**2 
       #                                    - mask_zeroes)
       #  
        #I_masked = np.array([el * mask for el in I])
        
        # ip = 0
        # while(ip < 1):
        #     rand_ind_x = np.random.randint(899)
        #     rand_ind_y = np.random.randint(899)
        #     if I_masked[0, rand_ind_x, rand_ind_y] != 0.0:
        #         pl.plot(I[:, rand_ind_x, rand_ind_y], 'r.--')
        #         pl.title(f"{rand_ind_x}, {rand_ind_y}, {ROS[el]}")
        #         pl.show()
        #         ip = 2
        #     else: 
        #         print(f"{rand_ind_x} and {rand_ind_y} are not in"
        #               + f"{ROS[el]}") 
        # #if I_masked[rand_ind_x, rand_ind 
        # x_ind = [658, 830, 131, 82, 612]
        # y_ind = [792, 518, 145, 846, 55]
        I_mean = I[el, :]
        #I_mean  = I[:, x_ind[el], y_ind[el]]
        #I_mean = [np.mean(ell) * coeff_zeroes for ell in I_masked]
    
        #plt.plot(waves_1, I_mean, '.--', label=ROS[el])
    
        spec_profile1 = spectral_data(waves+8542.1, I_mean, 
                                      ADU, cadence, dl)
    
        spec_profile1.calc_rms_vel(N_samples, calc_v_lc, 2, -2, 7)
        #spec_profile1.plot_results_vel()
        
        Pxx[el, :]      = spec_profile1.Pxx / 1000
    
    return Pxx

def choose_spec_profile(mask):
    """
    Choose a random index positive set from a mask 

    Input:
    -- mask -- [Nx, Nx] -- mask
   
    Output:
    -- index -- [2] -- x,y coordinates of the index chosen
    """

    Nx = mask.shape[0]
    
    isInArray = False;

    while (isInArray != True):
        index = np.random.randint(0, Nx, size=2)
        if mask[index[0], index[1]] == 1.0:
            isInArray = True
    
    return index
 
    

def compute_noise_1(mask_el, N_tries):
    """
    Compute the PSD of a random spectral profile 

    Input:  
    -- mask_el -- int, index of which element of the mask to be used
    -- N_tries -- int,  # of realizations of the noise 
   
    Output: 
    -- Pxx  -- [freq_num,  N_tries] -- PSD of the resulting noise
    """    
    
    ROS = ["plage", "network", "fibrils", "internetwork", "penumbra"]
    data_dir = ("/home/momo/Comps_2/") 
    N_samples = 150
    dx = 50
    cadence   = 1/3.5
    dl        = 0.05 # Å is the spacing of the 
    freq      = np.linspace(0, cadence/2, num = int(N_samples/2 + 1))
    # filename  = "Average_ROS_profiles.npz"
    filename = "CaII8542_profiles_fast.sav" 
    Pxx       = np.zeros((N_tries, int(N_samples/2 + 1)))
    masks = np.load(data_dir + 'Masks_FOV2.npz')
    mask  = masks[ROS[mask_el]]
    aa = io.readsav(data_dir+filename)
     
        
    waves = aa["waves"]
    waves = np.append(waves, (waves[-1] + .19))
    I     = aa["profiles"]

    index = choose_spec_profile(mask)
    I_mean = np.squeeze(I[:, index[0], index[1]])
    
    
    spec_profile1 = spectral_data(waves+8542.1, I_mean, 
                                  ADU, cadence, dl)
    
    for el in range(0, N_tries):


        
       # print(I.shape, waves.shape)
       #  waves_1 = np.append(waves, waves[-1]+waves[1]-waves[0])
       #  
       #  mask_zeroes = count_zeroes_in_array(mask)
       #  coeff_zeroes = mask.shape[0]**2 /(mask.shape[0]**2 
       #                                    - mask_zeroes)
       #  
        #I_masked = np.array([el * mask for el in I])
        
        # ip = 0
        # while(ip < 1):
        #     rand_ind_x = np.random.randint(899)
        #     rand_ind_y = np.random.randint(899)
        #     if I_masked[0, rand_ind_x, rand_ind_y] != 0.0:
        #         pl.plot(I[:, rand_ind_x, rand_ind_y], 'r.--')
        #         pl.title(f"{rand_ind_x}, {rand_ind_y}, {ROS[el]}")
        #         pl.show()
        #         ip = 2
        #     else: 
        #         print(f"{rand_ind_x} and {rand_ind_y} are not in"
        #               + f"{ROS[el]}") 
        # #if I_masked[rand_ind_x, rand_ind 
        # x_ind = [658, 830, 131, 82, 612]
        # y_ind = [792, 518, 145, 846, 55]
        #I_mean  = I[:, x_ind[el], y_ind[el]]
        #I_mean = [np.mean(ell) * coeff_zeroes for ell in I_masked]
    
        #plt.plot(waves_1, I_mean, '.--', label=ROS[el])
    
    
        spec_profile1.calc_rms_vel(N_samples, calc_v_lc, 2, -2, 7)
        #spec_profile1.plot_results_vel()
        
        Pxx[el, :]      = spec_profile1.Pxx / 1000 #make it in mHz
    
    return Pxx
    
N_profiles = 300
N_trials   = 1500
ROS = ["plage", "network", "fibrils", "internetwork", "penumbra"]
for el in range(len(ROS)):
    name_ROS = ROS[el]
    #Pxx = compute_noise()
    #print(Pxx)
    t0 = time.time()
    Pxx = Parallel(n_jobs=16)(delayed(compute_noise_1)(el, N_trials) 
                              for el1 in range(N_profiles))
    Pxx = np.array(Pxx)
    t1 = time.time()
    np.savez("Pxx_ROS_"+name_ROS+".npz", Pxx=Pxx)    
    print(f"Time for {N_trials*N_profiles} jobs is: {t1-t0:.2f}")

print(f"Time for {N_trials*N_profiles*len(ROS)} jobs is: {t1-t0:.2f}")

