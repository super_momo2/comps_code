#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 17:08:17 2019

@author: momo
"""

import numpy as np

from RHlib import *


IBIS = Instrument_Profile(3.5,[0])
Ca = Spectral_Line('/Users/molnarad/Desktop/rh/results/rh_spect_Ca_nu_30_phi_50.fits',
                   2e-3, 8e-2, 30, 50, 1, 854.2)
Ca.Instrument_degrade(IBIS)
Ca.find_lc_min1(False)
for i in range(Ca.N_nu):
    print("For frequency %f"%Ca.nu[i], np.amax(np.abs(Ca.vel_lc[i, :]) / 3))