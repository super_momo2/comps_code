'''
Compare v_rms <-> rho with ZEPHYR code
'''

import numpy as np
from RHlib import *
import matplotlib.pyplot as plt
import radynpy as rp

rad1 = rp.cdf.LazyRadynData('radyn_out.cdf')

rho  = rad1.d1
vz   = rad1.vz1
Temp = rad1.tg1

timestep = 1800
rad_Atmos = RADYN_atmos('radyn_out.cdf')
rad_Atmos.calc_cs(timestep)
cs = rad_Atmos.cs

fig, ax1 = plt.subplots()

fig.figsize=(6,4)
fig.dpi=250

ax1.plot(rho[timestep, :], [el / 1e5 for el in cs],
        'r.', label='Sound speed')
ax1.plot(rho[timestep, :], np.std(vz[(timestep-1000):-1, :], axis=0)
        / 1e3, 'g.', label='100 $\\times$  V$_z$rms')

ax2 = ax1.twinx()
ax2.plot(rho[timestep, :], Temp[1800, :], label='Temperature, K')


ax1.set_xscale('log')
ax1.set_yscale('log')
ax2.set_yscale('log')

ax1.set_xlabel('Density [g/cm$^3$]')
ax1.set_ylabel('Velocity [km/s]')
ax2.set_ylabel('Temperature [K]')

fig.legend()
ax1.grid(alpha=0.2)
ax2.grid(alpha=0.2)
plt.tight_layout()
plt.show()
