#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 11:48:35 2020

Separate the different regions of the solar surface by their observable
properties and save masks [0 or 1 valued arrays] to be applied for further
study of the data.

criteria for separating the areas of interest:
    -- Plage -- line core intensity of 8542 Ã is above the roof
    -- Network -- line core of intensity lower than Plage, but higher than
    than internetwork; High 5 minute power; 
    -- Internetwork -- dark in all + high 3 min power; 3 min power is 
    in the FFT file ~ mean of frequency array [2:4] indices
    -- Fibrils -- intermediate between Network and Internetwork; Low 3 min
    power;
    -- Penumbra -- Dark + I guess high 5 min power?


    Ca_int file:   Fig_1_Ca_int.fits
    Ha width file: FOV2_widths_Ha.sav ["widths" dic entry]
    FFT 8542 vel.lc: fft.el.lc.23Apr2017.target2.all.8542.ser_172509.sav
        Dic objects ["Freq1", "FFT_Powermap"]

@author: molnarad
"""

import numpy as np
import scipy as sp
from scipy.ndimage import gaussian_filter
from skimage import transform 
import matplotlib.pyplot as pl
from astropy.io import fits
from test_color_maps import diverging_colors, qualitative_colors
from matplotlib.colors import ListedColormap
from matplotlib.cm import register_cmap

from PYBIS import construct_name, load_fft_data
# Load data

class masks():
    def __init__(self, Ca_int, Ha_width, FFT_Ca_vel_lc, hmi_mag, 
                 kernel_size=15, threshold=0.4):
        '''
        Class for computing the masks for different regions of the solar 
        surface, described in the functions below.

        Parameters
        ----------
        Ca_int : ndarray (Nx, Ny)
            Ca II 8542 line core intensity data
        Ha_width : ndarray (Nx, Ny)
            H-alpha line core width array
        FFT_Ca_vel_lc : ndarray (Nx, Ny)
            FFT of the Ca 8542 vel.lc
        hmi_mag: ndarray (Nx, Ny)
            Magnetic field estimate from HMI 
        kernel_size: int, optional
            Kernel size of smoothing. Default is 15 pixels
        threshold: int, optional
            Threshold for the decision of the type solar surface.

        Returns
        -------
        None.
        '''
        
        self.Ca_int   = Ca_int
        self.Ha_width = Ha_width
        self.fft_data = FFT_Ca_vel_lc
        self.fft_mean = np.mean(self.fft_data)
        self.nx       = Ca_int.shape[0]
        self.ny       = Ca_int.shape[1]
        self.plage    = np.zeros((self.nx, self.ny))
        self.penumbra = np.zeros((self.nx, self.ny))
        self.internetwork = np.zeros((self.nx, self.ny))
        self.network  = np.zeros((self.nx, self.ny))
        self.fibrils  = np.zeros((self.nx, self.ny))
        self.Ca_int   = Ca_int
        self.Ha_width = Ha_width
        self.B_field  = np.abs(hmi_mag)
        self.kernel_size = kernel_size
        self.threshold = threshold
        
    def find_plage(self):
        self.plage = self.Ca_int > 850
        self.plage = self.plage * (self.B_field > 1000) 
        self.plage = gaussian_filter(self.plage * 1.0, 
                                     sigma=self.kernel_size)
        self.plage = self.plage > self.threshold
        #self.plage[:500, :] = 0
        
    def find_network(self):
        # self.network = (self.Ca_int < 900)   
        self.network =  (self.Ha_width > 1.00)
        self.network = self.network * (self.Ca_int < 900)
        self.network = self.network * (self.B_field > 300)
        self.network = self.network * (self.Ca_int > 600)   
        self.network = gaussian_filter(self.network * 1.0, 
                                       sigma=self.kernel_size)
        self.network = self.network > self.threshold
    
    def find_internetwork(self):
        self.internetwork = self.Ca_int < 500
        self.internetwork = gaussian_filter(self.internetwork *1.0, 
                                            sigma=self.kernel_size)
        self.internetwork = self.internetwork > self.threshold
    
    
    def find_fibrils(self):
        fibrils_fft_mask = self.fft_data < self.fft_mean
        self.fibrils = self.internetwork * fibrils_fft_mask
        self.fibrils = gaussian_filter(self.fibrils *1.0,  
                                       sigma=self.kernel_size)
        self.fibrils = self.fibrils > self.threshold
        return 0
    
    def find_penumbra(self):
        self.penumbra[400:, 0:200] = self.Ha_width[400:, 0:200] < 1.0
        self.penumbra = gaussian_filter(self.penumbra *1.0,  
                                        sigma=self.kernel_size) 
        self.penumbra = self.penumbra > self.threshold

    def determine_areas(self):
        self.find_plage()
        self.find_network() 
        self.find_penumbra()
        self.find_internetwork()
        self.find_fibrils()
        self.network      = self.network  * (1 - self.plage)
        self.internetwork = self.internetwork * (1 - self.penumbra)
        self.internetwork = self.internetwork * (1 - self.fibrils)
        self.fibrils      = self.fibrils * (1 - self.penumbra)
        
    def plot_masks(self):
        '''
        Plot the different masks with different colors using a discreet 
        colormap. 
        
        Returns
        -------
        None.

        '''
        
        newcmp = ListedColormap(qualitative_colors(5))
        labels = ['Penumbra', 'InterN', 'Fibrils', 'Network',
                  'Plage']
        
        total_mask = (self.penumbra * 1.1 + 2.1 * self.internetwork
                      + 3.1 * self.fibrils 
                      + 5 * self.network + 5.1 * self.plage)
        
        alpha_param = 0.4
        
        fig1 = pl.figure(dpi=250, figsize=(4, 4))
        pl.imshow(self.Ca_int, cmap='gray', origin='lower', 
                  extent=[-127.98, -31.98, 306.99, 210.99])
        
        im1 = pl.contourf(total_mask, cmap=newcmp, levels=[1, 2, 3, 4, 5, 6],
                          alpha=alpha_param, origin='lower',
                          extent=[-127.98, -31.98, 306.99, 210.99])
        # print(f"Mask is {total_mask}")
        m = pl.cm.ScalarMappable(cmap=newcmp)
        m.set_array(total_mask)
        m.set_clim(0.5, 5.5)
        pl.xlabel('Solar X [arcsec]')
        pl.ylabel('Solar Y [arcsec]')
        cbaxes = fig1.add_axes([0.02, -0.015, 0.95, 0.05]) 
        cbar = pl.colorbar(m, cax = cbaxes, boundaries=np.linspace(.5, 5.5, 6),
                           ticks=[1, 2, 3, 4, 5], fraction=0.046, pad=0.14,
                           orientation='horizontal',  anchor = (1.5, 1.0))
        #cbar.set_ticks([])
        cbar.ax.set_xticklabels(labels) 
        
        # pl.contourf(self.plage, alpha=alpha_param, cmap='plasma')
        # pl.contourf(self.network, alpha=alpha_param, cmap='viridis')
        # pl.contourf(self.internetwork, alpha=alpha_param, cmap='jet')
        # pl.contourf(self.penumbra, alpha=alpha_param, cmap='gnuplot')
        
        #pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
        #           +'2021a_Molnar_et_al_High_freq/git/git/Figures/Masks.png', 
        #           transparent=True)
        pl.tight_layout()
        #pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
        #c          +'2020b_Molnar_et_al_High_freq/git/git/Figures/Masks.png')
        
        pl.show()


data_dir = ('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
            + 'Comps_2/comps_data/IBIS/')

pl.style.use('science')


with fits.open(data_dir + 'Fig_1_Ca_int.fits') as temp:
    ca_int_data = np.array(temp[0].data)

ab = sp.io.readsav(data_dir + 'FOV2_widths_Ha.sav')
ha_width_data = np.array(ab['widths'])


aa = sp.io.readsav(data_dir + 'fft.el.lc.23Apr2017.target2.all.8542'
                   + '.ser_172509.sav') 
fft_vel_data = np.log10(np.mean(aa['fft_data'][13:20, :, :], axis=0))# / 
                        # np.mean(aa['fft_data'][4:9, :, :], axis=0))

x_len = ca_int_data.shape[0]
y_len = ca_int_data.shape[1]

with fits.open(data_dir + "HMI_mag_04_23_2017_17_25_UT.fits") as temp:
    hmi_data = np.array(temp[0].data)

hmi_ibis_scale = transform.resize(hmi_data[52:212, 61:221], 
                                  [1000, 1000], preserve_range=True)

# Save the masks

m = masks(ca_int_data, ha_width_data, fft_vel_data, hmi_ibis_scale,
          kernel_size=20, threshold=0.3)
m.determine_areas()


'''
im2 = pl.imshow(m.plage, cmap='plasma', origin='lower')
pl.title('Plage regions')
pl.show()
im2 = pl.imshow(m.network, cmap='plasma', origin='lower')
pl.title('Network regions')
pl.show()
im3 = pl.imshow(m.penumbra, cmap='plasma', origin='lower')
pl.title('Penumbra regions')
pl.show()
'''

pl.imshow(m.fibrils)
pl.show()

m.plot_masks()

#np.savez('Masks_FOV2.npz', plage=m.plage, network=m.network, fibrils=m.fibrils,
#         internetwork=m.internetwork, penumbra=m.penumbra)
    