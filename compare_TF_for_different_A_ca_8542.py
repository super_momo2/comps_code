#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 22:20:35 2020

Compare the different transmission coefficients for different amplitudes of
the source flux for Ca II 8542 line. 

@author: molnarad
"""

import numpy as np
import scipy as sp
from scipy import io
import matplotlib.pyplot as plt
# from power_uncertain import power_uncertain

from RHlib import Spectral_Line, Instrument_Profile

atm_files = ["FALC11_nu_20_phi_num_50_A_1.fits",
             "FALC11_nu_20_phi_num_50_A_2.64.fits",
             "FALC11_nu_20_phi_num_30_A_3.fits",
             "FALC11_nu_20_phi_num_50_A_8.36.fits",
             "FALC11_nu_20_phi_num_50_A_10.fits",
             "FALC11_nu_20_phi_num_50_A_20.0.fits",
             "FALC11_nu_20_phi_num_30_A_30.fits",
             ]

spec_files = [
              "spect_falc11_nu_20_phi_num_50_a_1_ca.fits",
              "spect_falc11_nu_20_phi_num_50_a_2.64_ca.fits",
              "spect_falc11_nu_20_phi_num_30_a_3_ca.fits",
              "spect_falc11_nu_20_phi_num_50_a_8.36_ca.fits",
              "spect_falc11_nu_20_phi_num_50_a_10_ca.fits",
              "spect_falc11_nu_20_phi_num_50_a_20_ca.fits",
              "spect_falc11_nu_20_phi_num_30_a_30_ca.fits",
              ]

amplitudes = [1, 2.64, 3, 8.36, 10, 20, 30]

IBIS = Instrument_Profile(3.5, [0])

plt.figure(dpi=125, figsize=(5, 5))

num_files = 7
num_freqs = 20

G       = np.zeros((num_files, num_freqs))
var_vel = np.zeros((num_files, num_freqs)) 

for ii in range(num_files):

    Ca   = Spectral_Line('/Users/molnarad/Desktop/rh/',
                         atm_files[ii],
                         spec_files[ii],
                         854.2)

    Ca.Instrument_degrade(IBIS)
    Ca.find_lc_min1(False)


    Ca.calc_G()
    
    G[ii, :]       = Ca.G
    var_vel[ii, :] = Ca.var_vel
    
    plt.plot(Ca.nu*1e3, Ca.G, '.--', label=amplitudes[ii])

plt.yscale("log")
plt.title("Ca 8542 TF")
plt.xlabel("Frequency [mHz]")
plt.ylabel("Transmission Coefficient [W.m$^{-2}$"
           + ".s$^2$.km$^{-2}$.mHz]")
plt.grid()
plt.legend(title="Amp [km/s] @ 10$^{12}$ cm$^{-3}$")
plt.show()

np.savez("Ca_8542_vel_lc_G_vel_var.npz", Ca_G = G, Ca_var_vel = var_vel,
         nu=Ca.nu)