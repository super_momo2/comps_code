#Trace magnetic field lines in a Hinode B-field extrapolation
#done with the lff_extrap.pro
#Retain properties of the loops that you can estimate properties of the propagating waves

import numpy as np
import matplotlib as mp
import matplotlib.pyplot as plt
from mayavi.mlab import *
from astropy.io import fits
from scipy.interpolate import *


class loop():
    'Class for the magnetic field lines in the Hinode extrapolation

    Caveat - the coodinates will be appended as 3-numbers and read as 3 numbers

    Return coordinates of the loop as a (3,X) array - use method read_coordinates

    '

    def __init__(self):
        self.data = []

    def read_coordinates(self):
        length_coord_array = (len(data))/3
        coordinates        = np.zeros((3,length_coord_array))
        for i in range(0,length_coord_array):
            coordinates[0,i] = self.data[i*3]
            coordinates[1,i] = self.data[i*3+1]
            coordinates[2,i] = self.data[i*3+2]



def integrate_loop(x,y,z,Bx,By,Bz,inter_parm):
    #x,y,z    - Initial position of the field line;
    #Bx,By,Bz - Field line
    #intet_parm- parameter how sub_resolution to go

    field_shape = Bx.shape
    Bx_extent   = field_shape[2]
    By_extent   = field_shape[1]
    Bz_extent   = field_shape[0]

    grid_x, grid_y, grid_z = np.mgrid[0:Bx_extent:Bx_extent, 0:Bx_extent:Bx_extent,0:Bx_extent:Bx_extent]
    grid_x_i, grid_y_i, grid_z_i = np.mgrid[0:Bx_extent:((Bx_extent)/inter_parm), 0:Bx_extent:((Bx_extent)/inter_parm),0:Bx_extent:((Bx_extent)/inter_parm)]

    Bx_i        = griddata(Bx,

    loop        = []
    dz          = interpolation_fine
    dx          = 0
    dy          = 0
    while (z>0) and (z<field_shape[0]):
        dx = Bx_i( ... ) *dz / Bz_i( ... )
        dy = By_i( ... ) *dz / Bz_i( ... )
        loop.append([x,y,z])

    return loop

hdul  = fits.open('B_target2.fits')
data = hdul[0].data
Bx   = (data[:,:,:,2])
Bz   = (data[:,:,:,0])
By   = (data[:,:,:,1])


