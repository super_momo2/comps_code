#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 21:18:30 2020

Fit all the data with a straight line and a power law to estimate the noise
and the signal

@author: molnarad
"""

import numpy as np
from PYBIS import construct_name, load_fft_data_simple
from scipy.optimize import curve_fit
import matplotlib.pyplot as pl
import time

from tqdm import tqdm 
from skimage.transform import downscale_local_mean

def f(x, a, b, c):
    '''
    

    Parameters
    ----------
    f(x) = a * x^b + c
    
    we can estimate a, b and c from the data itself! 
    Returns
    -------
    y = f(x):

    '''

    return a * (x**b) + c

def f_inv(x, y, b, c):
    return (y - c)/(x**b)


# data loading here

data_dir = ('/Users/molnarad/CU_Boulder/Work/'
                +'Chromospheric_business/Comps_2/comps_data/')
spec_property = 'vel.lc'
time_stamp    = 170444
dsample_f = 6

freq, fft_data = load_fft_data_simple(data_dir, spec_property, time_stamp)

time0 = time.time()
#some fitting here 

popt, pcov = curve_fit(f, freq[5:], np.mean(np.mean(fft_data[5:, 500:504, 500:504], 
                                                    axis=1),axis=1),
                       p0=[1e-1, -4, 1e-3])

fft_data_downsampled = downscale_local_mean(fft_data, (1, dsample_f, 
                                                       dsample_f))
popt_1 = np.zeros((fft_data_downsampled.shape[1],
                   fft_data_downsampled.shape[2], popt.size))

failed_fits = 0
freq_ind    = 6
dfreq_ind   = 8
freq_ratio  = freq[freq_ind]/freq[freq_ind+dfreq_ind]
lg_freq_ratio = np.log10(freq_ratio)


for ii in tqdm(range(fft_data_downsampled.shape[1])):

    for jj in range(fft_data_downsampled.shape[2]):
        
        try:
            y0 = fft_data_downsampled[freq_ind, ii, jj]
            y1 = fft_data_downsampled[freq_ind + dfreq_ind, ii, jj]
            
            est_p2 = np.amin(fft_data_downsampled[:, ii, jj])
            
            dly0 = np.log10(y0 - est_p2)
            dly1 = np.log10(y1 - est_p2)
            
            est_p1 = (dly0 - dly1)/lg_freq_ratio
            
            est_p0 = 0.5*(f_inv(freq[freq_ind], y0, est_p1, est_p2) + 
                          f_inv(freq[freq_ind+dfreq_ind], y1, est_p1, est_p2))
            
            # if est_p1 == np.nan or est_p2 == np.nan:
            # print(f"The fitting coefficients are {est_p0} and {est_p1}")
            
            est    = [est_p0, est_p1, est_p2]
            data_loc = fft_data_downsampled[4:, ii, jj]
            popt_1[ii, jj, :], pcov = curve_fit(f, freq[4:], 
                                                data_loc,
                                                #p0=est, maxfev = 10000,
                                                p0=[1, -3, 1e-4], maxfev=800,
                                                sigma = 0.4*data_loc)
            if popt_1[ii, jj, 2] < 0:
                popt_1[ii, jj, 2] = 1e-8
                
        except RuntimeError:
            failed_fits = failed_fits + 1
            popt_1[ii, jj, :] = [1e-8, 1e-8, 1e-8]
            
            
time1 = time.time()
print(f"Time for fitting is {(time1-time0):.3e}")
print(f"Failed fits during the fitting routine: {failed_fits}")

# Preview a single spectrum
x_loc = 50
y_loc = 50

pl.loglog(freq[2:], fft_data_downsampled[2:, x_loc, y_loc], 'r.')
pl.loglog(freq[5:], f(freq[5:], popt_1[x_loc, y_loc, 0], 
                      popt_1[x_loc, y_loc, 1], popt_1[x_loc, y_loc, 2]))

pl.show()

im = pl.imshow(np.log10(popt_1[:, :, 2]), vmin=-5, vmax=0,
               cmap='gray',
               origin='lower')
pl.colorbar(im)
pl.tight_layout()
pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
           +'2020_Molnar_et_al_High_freq/git/git/Figures/noise_floor_FOV2.png')
pl.show()