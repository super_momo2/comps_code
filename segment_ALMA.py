import numpy as np
import scipy as sp
from scipy.ndimage import gaussian_filter
import matplotlib.pyplot as pl
from astropy.io import fits
from test_color_maps import diverging_colors, qualitative_colors
from matplotlib.colors import ListedColormap
from matplotlib.cm import register_cmap
from scipy import io
from scipy import signal

from PYBIS import construct_name, load_fft_data
# Load data


ds = 160

class masks():
    def __init__(self, ALMA_int):
        '''
        Class for computing the masks for different regions of the solar
        surface, described in the functions below.

        Parameters
        ----------
        Ca_int : ndarray (Nx, Ny)
            Ca II 8542 line core intensity data
        Ha_width : ndarray (Nx, Ny)
            H-alpha line core width array
        FFT_Ca_vel_lc : ndarray (Nx, Ny)
            FFT of the Ca 8542 vel.lc

        Returns
        -------
        None.
        '''
        self.ALMA_int = ALMA_int
        self.ALMA_int_average = np.mean(ALMA_int, axis=2)
        self.nx       = ALMA_int.shape[0]
        self.ny       = ALMA_int.shape[1]
        self.internetwork = np.zeros((self.nx, self.ny))
        self.network  = np.zeros((self.nx, self.ny))
        self.fibrils  = np.zeros((self.nx, self.ny))

    def find_network(self):
        self.network = (self.ALMA_int_average > 10000)
        mask_zeroes_int = count_zeroes_in_array(m.network)
        self.network_coeff = (self.nx**2)/(self.nx**2 - mask_zeroes_int)
        #self.network = gaussian_filter(self.network *1.0, sigma=10)
        #self.network = self.network > 0.4

    def find_internetwork(self):
        self.internetwork = self.ALMA_int_average < 7500
        mask_zeroes_int = count_zeroes_in_array(m.internetwork)
        self.internetwork_coeff = (self.nx**2)/(self.nx**2 - mask_zeroes_int)
        #self.internetwork = gaussian_filter(self.internetwork *1.0,
        #                                    sigma=20)
        #self.internetwork = self.internetwork > 0.45


    def find_fibrils(self):
        self.fibrils = ((self.ALMA_int_average < 10000)
                         * (self.ALMA_int_average > 7500))
        mask_zeroes_int = count_zeroes_in_array(m.fibrils)
        self.fibrils_coeff = (self.nx**2)/(self.nx**2 - mask_zeroes_int)
        #self.fibrils = gaussian_filter(self.fibrils *1.0,
        #                                    sigma=10)
        #self.fibrils = self.fibrils > .4

    def determine_areas(self):
        self.find_network()
        self.find_internetwork()
        self.find_fibrils()

    def plot_masks(self):
        '''
        Plot the different masks with different colors using a discreet
        colormap.

        Returns
        -------
        None.

        '''

        newcmp = ListedColormap(qualitative_colors(5))
        labels = ['InterN', 'Fibrils', 'Network']

        total_mask = (2.1 * self.internetwork
                      + 3.1 * self.fibrils
                      + 5 * self.network )

        alpha_param = 0.4

        fig1 = pl.figure(dpi=125, figsize=(4, 4))
        pl.imshow(self.ALMA_int_average, cmap='gray', origin='lower',
                  extent=[-127.98, -31.98, 306.99, 210.99])

        im1 = pl.contourf(total_mask, cmap=newcmp, levels=[1, 2, 3, 4, 5, 6],
                          alpha=alpha_param, origin='upper',
                          extent=[-127.98, -31.98, 210.99,306.99,])
        print(f"Mask is {total_mask}")
        m = pl.cm.ScalarMappable(cmap=newcmp)
        m.set_array(total_mask)
        m.set_clim(0.5, 5.5)
        pl.xlabel('Solar X [arcsec]')
        pl.ylabel('Solar Y [arcsec]')
        cbaxes = fig1.add_axes([0.02, -0.015, 0.95, 0.05])
        cbar = pl.colorbar(m, cax = cbaxes, boundaries=np.linspace(.5, 5.5, 6),
                           ticks=[1, 2, 3, 4, 5], fraction=0.046, pad=0.14,
                           orientation='horizontal',  anchor = (1.5, 1.0))
        #cbar.set_ticks([])
        cbar.ax.set_xticklabels(labels)

        # pl.contourf(self.plage, alpha=alpha_param, cmap='plasma')
        # pl.contourf(self.network, alpha=alpha_param, cmap='viridis')
        # pl.contourf(self.internetwork, alpha=alpha_param, cmap='jet')
        # pl.contourf(self.penumbra, alpha=alpha_param, cmap='gnuplot')


        pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
                   +'2020b_Molnar_et_al_High_freq/git/git/Figures/ALMA_Mask.png')

        pl.show()


def count_zeroes_in_array(array):
    counter = 0
    for el in array.ravel():
        if el == False:
            counter += 1

    return counter


data_dir  = ("/Users/molnarad/CU_Boulder/Work/Chromospheric_business/"
             + "Comps_2/comps_data/ALMA/")
file_name = "ALMA_band3_t2_csclean_feathered.sav"

alma_sav = io.readsav(data_dir+file_name)
alma_data_temp = alma_sav["maps"]

len_alma_data = len(alma_data_temp)

alma_data = np.zeros((ds, ds, len_alma_data))

for el in range(len_alma_data):
    alma_data[:, :, el] = alma_data_temp[el][0][100:-100, 100:-100]

m = masks(alma_data)
m.determine_areas()
m.plot_masks()

fft_data = np.zeros((ds, ds, int(len_alma_data/2)+1))

for ii in range(ds):
    for jj in range(ds):
         freq, fft_data[ii, jj, :] = signal.periodogram(alma_data[ii, jj, :],
                                                        fs = 1/2.0)
fft_int = np.zeros((ds, ds, len_alma_data//2 + 1))
fft_net = np.zeros((ds, ds, len_alma_data//2 + 1))
fft_fib = np.zeros((ds, ds, len_alma_data//2 + 1))

labels = ["network", "fibrils", "internetwork"]

for el in range(int(len_alma_data/2)+1):
    fft_int[:, :, el] = fft_data[:, :, el] * m.internetwork
    fft_net[:, :, el] = fft_data[:, :, el] * m.network
    fft_fib[:, :, el] = fft_data[:, :, el] * m.fibrils

fft_int = np.mean(fft_int, axis=(0,1)) * m.internetwork_coeff
fft_net = np.mean(fft_net, axis=(0,1)) * m.network_coeff
fft_fib = np.mean(fft_fib, axis=(0,1)) * m.fibrils_coeff

pl.figure(dpi=250, figsize=(3, 3))
pl.plot(freq, fft_net, 'r.--', label='Network')
pl.plot(freq, fft_fib, 'g.--', label='Fibrils')
pl.plot(freq, fft_int, 'b.--', label='Internetwork')
pl.yscale("log")
pl.xscale("log")
pl.grid()
pl.ylim(1e4, 1e8)
pl.xlim(1e-3, 70e-3)
pl.ylabel("Power [$K^2/s^2/Hz$]")
pl.xlabel("Frequency [Hz]")
pl.legend()
pl.tight_layout()
pl.show()
