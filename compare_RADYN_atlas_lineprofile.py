#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 00:13:47 2020

Compare the Ca II 8542 line and the atlas

@author: momo
"""

import numpy as np
import matplotlib.pyplot as plt
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog
from scipy.io import readsav


simulation_name = "radyn_ulmschneider_3000"
atlas_line_profile = "CaII_8542_atlas.sav"
data_dir = "/mnt/Data/Comps_2/comps_data/RADYN_runs/"


rad = RADYN_atmos(data_dir + simulation_name + '.cdf')
    
sp_line_index = 20 # 20 - Ca II 8542 Å; 2 - Ha 6563 Å

rad.calc_sp_line(sp_line_index)
rad.apply_sp_PSF_IBIS(800)


atlas = readsav("/mnt/Data/Comps_2/comps_code/"+atlas_line_profile)
atlas_waves = atlas["wave"]
atlas_spect = atlas["spec"]

time_step = [1000, 1200]
sp_line_av_profile = np.mean(rad.IBIS_int[1000:1200, :], axis=0)

#conv_factor = min(atlas_spect) / min(sp_line_av_profile[time_step, :])

plt.figure(dpi=250)
plt.plot(atlas_waves, atlas_spect, "r.--", label="Atlas")
plt.plot(rad.IBIS_waves-2.295,   
         sp_line_av_profile/max(sp_line_av_profile)/1.7,
         ".--", label="Simulation")
plt.grid()
plt.legend()
plt.title("Ca II 8542 ${\AA}$ RADYN-atlas comparison [timestep " 
          + str(time_step) + "]")
plt.xlabel("Wavelength [$\AA$]")
plt.ylabel("Intensity")
plt.xlim(8540.25, 8544)
plt.show()