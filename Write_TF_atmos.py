#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 12:40:47 2019

Create model Atmospheres for the TF tests

@author: molnarad
"""

from RHlib import Atmos_height

atmos_dir  = '/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_data/Model_Atmospheres/'
model_name = 'model1005'

FALC = Atmos_height(atmos_dir, model_name, 1000)
FALC.write_TF_test(2e-3, 8e-2, 20, 50, 1, 656.3, True)
FALC.write_TF_test(2e-3, 8e-2, 20, 50, 4.64, 656.3, True)
FALC.write_TF_test(2e-3, 8e-2, 20, 50, 8.36, 656.3, True)
FALC.write_TF_test(2e-3, 8e-2, 20, 50, 10.36, 656.3, True)
FALC.write_TF_test(2e-3, 8e-2, 20, 50, 20.0, 656.3, True)
FALC.write_TF_test(2e-3, 8e-2, 20, 50, 30.0, 656.3, True)
