"""

Calculates the derived H-alpha velocities from RADYN
simulations for different acoustic 
flux in the RADYN Atmosphere

"""

import numpy as np
import matplotlib.pyplot as plt
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog
from scipy import signal


simulation_name = "radyn_ulmschneider_1200_low_dt"
data_dir = "/mnt/Data/Comps_2/comps_data/RADYN_runs/"

rad = RADYN_atmos(data_dir + simulation_name + '.cdf')
    
sp_line_index = 20 # 20 - Ca II 8542 Å; 2 - Ha 6563 Å

start_index = 4500
end_index   = -1
dt_sim      = .5
count_every = 1

height_v = 1.13e3 

#rad.calc_Flux(1200, 400)
rad.calc_sp_line(sp_line_index)
rad.calc_Flux_equidistant(start_index+250, 250)


if sp_line_index == 2:
    vel_coefficient = 3e5/6563
    int_grid_scale = np.linspace(rad.sp_line_wave[10], rad.sp_line_wave[-10],
                                 num=100)

if sp_line_index == 20:
    vel_coefficient = 3e5/8542
    int_grid_scale = np.linspace(rad.sp_line_wave[28], rad.sp_line_wave[-28],
                                 num=500)

line_intensity_int = [np.interp(int_grid_scale, rad.sp_line_wave, I) for I in 
                      rad.sp_line_I]
line_intensity_int = np.array(line_intensity_int)


rad.apply_sp_PSF_IBIS(500)

IBIS_I_mean_time = [np.mean(rad.IBIS_int[i*7:(i*7+7), :], axis=0) for i in 
                    range(int(rad.t_steps/7)-3)]
IBIS_I_mean_time = np.array(IBIS_I_mean_time)

Ha_velocity = [calc_v_lc(rad.IBIS_waves, I, 5, -5, 7)[0] for I
               in IBIS_I_mean_time[start_index//7:end_index//7:count_every,:]]

#Ha_velocity = [calc_v_cog(int_grid_scale, I) for
#               I in line_intensity_int[start_index:end_index:count_every, :]]


Ha_velocity_metric = vel_coefficient * (np.array(Ha_velocity)
                                        - np.mean(Ha_velocity))

for el in range(len(Ha_velocity_metric)):
    if (Ha_velocity_metric[el]) > 7:
        Ha_velocity_metric[el] = 0
    elif (Ha_velocity_metric[el]) < -7:
        Ha_velocity_metric[el] = -0.03
Ha_v_freq, Ha_v_PSD = signal.periodogram(Ha_velocity_metric,
                                         fs=1/dt_sim/count_every/7) 

#mean_Ha_v_PSD = np.array([np.mean(Ha_v_PSD[(i*9):(i*9+9)]) 
#                          for i in range(167)])

sp_line_label = np.median(rad.sp_line_wave)

x = np.linspace(1e-1, 5.18, num=1000)
y0 = 1e-20
y1 = 1e30

num_t_steps = len(Ha_velocity_metric)
time_xscale = np.linspace(0, num_t_steps*count_every*dt_sim*7,
                          num=num_t_steps)
markersize_vel = 3

plt.figure(dpi=250)
plt.plot(time_xscale, Ha_velocity_metric,
         'r.--', label='observed', markersize=markersize_vel, 
         alpha=1.0)
plt.ylabel("Velocity [km/s]")
plt.xlabel("Time [s]")
plt.grid()


vz = np.zeros(rad.t_steps)
idx  = np.abs(rad.z_eq/1e5 - height_v).argmin()

rho_eq_mean = np.mean(rad.rho_eq[:, idx])
print(f"Rho_mean is {rho_eq_mean}")

for t in range(rad.t_steps):
    vz[t] = rad.v_eq[t, idx]/1e5

vz -= np.mean(vz[-500:])
num_t_steps = len(vz[start_index:end_index:count_every])
time_xscale = np.linspace(0, num_t_steps*count_every*dt_sim,
                          num=num_t_steps)
    
plt.plot(time_xscale, vz[start_index:end_index:count_every]*-1,
         'b.--', label="Tau=1 Vz", markersize=markersize_vel,
         alpha=0.25)
#plt.xlim(0, 2500)
#plt.ylim(-1, 1)
plt.title(f"Velocities for model f{simulation_name}"
          + f"")
plt.legend()
plt.savefig("vel_example.png", transparent=True)
plt.show()


Ha_v_freq_a, Ha_v_PSD_a = signal.periodogram(vz[start_index:end_index:count_every],
                                             fs=1/dt_sim/count_every) 

plt.figure(dpi=250)
plt.plot(Ha_v_freq*1e3, Ha_v_PSD/1e3, 'r.--', 
         label=f"{sp_line_label:4.0f} Å vel.lc")
plt.plot(Ha_v_freq_a*1e3, Ha_v_PSD_a/1e3, 'b.--', 
         label=f"{sp_line_label:4.0f} Å RADYN tau=1")
#plt.plot(Ha_v_freq[::7], mean_Ha_v_PSD, 'b--', label="PSD average")
plt.fill_between(x, y0*1e3, y1*1e3, facecolor='red', alpha=0.15)
plt.xlim(1, 100)
plt.ylim(1e-10, 1e1)
#plt.ylim(np.min(Ha_v_PSD[1:-1])/2, np.amax(Ha_v_PSD)*2)
plt.yscale('log')
plt.xscale('log')
plt.xlabel("Frequency [mHz]")
plt.ylabel("Spectral power [km$^2$/s$^2$/mHz]")
plt.title(f"PSD of vel_lc for {simulation_name}" 
          + f"")
plt.text(1.1, 1.5e-8, "Evanescent wave\nfrequency regime", 
         color="red", fontsize=12)
plt.legend()
plt.grid()
plt.savefig("PSD_example.png", transparent=True)
plt.show()


max_freq = 60e-3
dfreq   = 5e-3
dfreq_array = Ha_v_freq[1]
ddfreq = int(dfreq / dfreq_array) 
num_freqs = int(max_freq / dfreq) + 1

Ha_v_PSD_mean = [np.mean(Ha_v_PSD[i*ddfreq:(i*ddfreq + ddfreq)]) for i 
                 in range(num_freqs)]
Ha_v_PSD_a_mean = [np.mean(Ha_v_PSD_a[i*ddfreq:(i*ddfreq + ddfreq)]) 
                   for i in range(num_freqs)]
freq_range = np.linspace(0, (num_freqs-1)*dfreq, num=num_freqs) + dfreq/2
T_coeff = np.sqrt(np.array(Ha_v_PSD_mean) 
                  / np.array(Ha_v_PSD_a_mean))


plt.figure(dpi=250)
plt.plot(freq_range*1e3, T_coeff, "bo--")
plt.xlabel("Frequency [mHz]")
plt.ylabel("T coefficient")
plt.xlim(3, 63)
plt.ylim(0, 1)
plt.grid()
plt.savefig("T_coeff.png", transparent=True)
plt.show()


np.savez("T" + simulation_name + ".npz", T_coeff, freq_range)
# 
# 
# figure, ax1 = plt.subplots(dpi=250)
# ax1.plot(rad.z_eq/1e5, rad.Flux_eq, 'b.--')
# ax1.grid()
# ax1.set_yscale("log")
# ax1.set_ylim(1e-4, 1e7)
# ax1.set_xlim(-100, 1800)
# ax1.set_title(f"Acoustic flux {sp_line_label:4.0f} for {simulation_name}")
# ax1.set_xlabel("Height [km]")
# ax1.set_ylabel("Acoustic Flux [W / m^2]")
# 
# ax2 = ax1.twinx()
# 
# color = 'tab:red'
# ax2.set_ylabel('Log$_{10}$[Temperature [K]] ', color=color)
# ax2.plot(rad.z[1000, :]/1e5, 
#          np.log10(rad.temperature[1000, :]), color=color)
# plt.show()

