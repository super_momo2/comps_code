#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 16:27:25 2019

Calculate the fluxes from RADYN simulation

@author: molnarad
"""

import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib.animation as animation
from RHlib import RADYN_atmos


def animate(i):
    vz.set_ydata([el / 1e5 for el in rad.vz[i, :]])
    vz.set_xdata([el / 1e5 for el in rad.z[i, :]])
    Ly.set_ydata(I_Ly[i, :] / np.amax(I_Ly[0, :]))
    Ha.set_ydata(I_Ha[i, :] *.2 / np.amin(I_Ha[0, :]))
    Ca.set_ydata(I_Ca[i, :] / np.amin(I_Ca[0, :]) / 6)
    fig.suptitle(f'Time = {rad.time[i]:5.2f} [seconds]')
    plt.tight_layout()


simulation_name = "radyn_ulmschneider_4500_low_dt.cdf"

rad = RADYN_atmos("/mnt/Data/Comps_2/"   
                  + "comps_data/RADYN_runs/"+ simulation_name) 

fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(10, 6), dpi=250)

mu_Idx = -1# for straight up

wl_Ly, I_Ly = rad.line_intensity_with_cont(0, mu_Idx)
wl_Ha, I_Ha = rad.line_intensity_with_cont(2, mu_Idx)
wl_Ca, I_Ca = rad.line_intensity_with_cont(20, mu_Idx)

vz = ax[0, 0].plot([ el / 1e5 for el in rad.z[0, :]],
               [el / 1e5 for el in rad.vz[0, :]], 'r.--', lw=2)[0]

x = np.linspace(1.05e3, 1.2e3, num=100)
y1 = 0*x - 10
y2 = 0*x + 10
ax[0, 0].fill_between(x, y1, y2, color="red", alpha=0.1)

fontsize_labels = 9
ax[0, 0].text(20, -3.9, "Photosphere", fontsize=fontsize_labels)
ax[0, 0].text(700, -3.9, "Chromosphere", fontsize=fontsize_labels)
#ax[0, 0].text(1300, -4.2, "TR")
ax[0, 0].text(1600, -3.9, "Corona", fontsize=fontsize_labels)
ax[0, 0].text(1000, 3.2, "Ca II 854.2 nm\nFormation region",
              color="red")

# Calculate the total radiative losses

Ly = ax[0, 1].plot(wl_Ly, I_Ly[0, :] / np.amax(I_Ly[0, :]),
                   'r.--', lw=2)[0]
Ha = ax[1, 0].plot(wl_Ha, I_Ha[0, :] * .2 / np.amin(I_Ha[0, :]),
                   'r.--', lw=2)[0]
Ca = ax[1, 1].plot(wl_Ca, I_Ca[0, :] / np.amin(I_Ha[0, :]) / 6,
                   'r.--', lw=2)[0]
plt.tight_layout()
# ax.set_xlim(6562,6568)
vz.axes.set_ylim(-1.5, 1.5)


vz.axes.set_title('V$_z$')
Ly.axes.set_title('H I Ly-$\\alpha$ 1215.7 $\\AA$')
Ha.axes.set_title('H I Balmer $\\alpha$ 6562.8 $\\AA$')
Ca.axes.set_title('Ca II 8542 $\\AA$')

ax[0, 0].set_xlabel('Height [km]')
ax[0, 1].set_xlabel('Wavelength [$\AA$]')
ax[1, 0].set_xlabel('Wavelength [$\AA$]')
ax[1, 1].set_xlabel('Wavelength [$\AA$]')
ax[0, 1].set_ylabel('Intensity')
ax[1, 0].set_ylabel('Intensity')
ax[1, 1].set_ylabel('Intensity')
ax[0, 0].set_xlim(0, 2000)
ax[0, 0].set_ylim(-5, 5)


ax[1, 0].set_xlim(6564.0, 6565.5)
ax[1, 1].set_xlim(8544.1, 8544.7)
ax[1, 1].set_ylim(0 , 1)
for el in ax.flatten():
    el.grid()

# anim = FuncAnimation(fig, animate, interval=45, frames=30)
anim = FuncAnimation(fig, animate, interval=45, frames=len(rad.time))

# Set up formatting for the movie files
Writer = animation.writers['ffmpeg']
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)


#anim.save(str("1.mp4"))
anim.save(simulation_name + "_vz+Ly+Ha+Ca.mp4", writer=writer)

plt.draw()
plt.show()
