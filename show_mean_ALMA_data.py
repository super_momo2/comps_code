import numpy as np
import scipy as sp
from scipy.ndimage import gaussian_filter
import matplotlib.pyplot as pl
from astropy.io import fits
from test_color_maps import diverging_colors, qualitative_colors
from matplotlib.colors import ListedColormap
from matplotlib.cm import register_cmap
from scipy import io
from scipy import signal

from PYBIS import construct_name, load_fft_data

data_dir  = ("/Users/molnarad/CU_Boulder/Work/Chromospheric_business/"
             + "Comps_2/comps_data/alma/")
file_name_Band3 = "alma_band3_t2_csclean_feathered.sav"
file_name_Band6 = "alma_20170423_band6_dc_t2_feathered_snr3.sav"

alma_sav_Band3 = io.readsav(data_dir+file_name_Band3)
alma_data_temp_Band3 = alma_sav_Band3["maps"]
alma_Band3_XC = alma_sav_Band3["maps"]["XC"][0]
alma_Band3_YC = alma_sav_Band3["maps"]["YC"][0]
alma_Band3_DX = alma_sav_Band3["maps"]["DX"][0]
alma_Band3_DY = alma_sav_Band3["maps"]["DY"][0]

range_Band_3 = [alma_Band3_XC-180*alma_Band3_DX-80,
                alma_Band3_XC+180*alma_Band3_DX-80,
                alma_Band3_YC-180*alma_Band3_DY,
                alma_Band3_YC+180*alma_Band3_DY ]


len_alma_data_Band3 = len(alma_data_temp_Band3)


alma_data_Band3 = np.zeros((360, 360, len_alma_data_Band3))

for el in range(len_alma_data_Band3):
    alma_data_Band3[:, :, el] = alma_data_temp_Band3[el][0][:, :]

alma_sav_Band6 = io.readsav(data_dir+file_name_Band6)
alma_data_temp_Band6 = alma_sav_Band6["maps"]


alma_Band6_XC = alma_sav_Band6["maps"]["XC"][0]
alma_Band6_YC = alma_sav_Band6["maps"]["YC"][0]
alma_Band6_DX = alma_sav_Band6["maps"]["DX"][0]
alma_Band6_DY = alma_sav_Band6["maps"]["DY"][0]
print(f"ALMA Band 6 dx is {alma_Band6_DY}")
print(f"ALMA Band 3 XC and YC is {alma_Band3_XC, alma_Band3_YC}")
print(f"ALMA Band 6 XC and YC is {alma_Band6_XC, alma_Band6_YC}")

len_alma_data_Band6 = len(alma_data_temp_Band6)

alma_data_Band6 = np.zeros((360, 360, len_alma_data_Band6))

range_Band_6 = [alma_Band6_XC-180*alma_Band6_DX,
                alma_Band6_XC+180*alma_Band6_DX,
                alma_Band6_YC-180*alma_Band6_DY,
                alma_Band6_YC+180*alma_Band6_DY ]
for el in range(len_alma_data_Band6):
    alma_data_Band6[:, :, el] = alma_data_temp_Band6[el][0][:, :]

fig, ax = pl.subplots(1, 2, dpi=250, figsize=(7, 4.5))

im1 = ax[0].imshow(np.mean(alma_data_Band3, axis=2),
                   extent=[range_Band_3[0], range_Band_3[1],
                           range_Band_3[2], range_Band_3[3]],
                   cmap="plasma", origin=0)

im2 = ax[1].imshow(np.mean(alma_data_Band6, axis=2),
                   extent=[range_Band_6[0], range_Band_6[1],
                           range_Band_6[2], range_Band_6[3]],
                   cmap="plasma", origin=0)

fig.colorbar(im1, ax=ax[0], label="Temperature [K]",
             shrink=0.48)
fig.colorbar(im2, ax=ax[1], label="Temperature [K]", shrink=0.48)

for el in ax:
    el.set_xlabel("Solar X [arcsec]")
    el.set_ylabel("Solar Y [arcsec]")
    
ax[0].set_xticks([-100, -75, -50, -25])
ax[0].set_xticklabels([-100, -75, -50, -25])
ax[1].set_xticks([-90, -75, -60, -45])
ax[1].set_xticklabels([-90, -75, -60, -45])


ax[0].set_title("ALMA Band 3")
ax[1].set_title("ALMA Band 6")
pl.tight_layout()
pl.savefig("/Users/molnarad/CU_Boulder/Work/Manuscripts/"
           + "2021a_Molnar_et_al_High_freq/git/git"
           + "ALMA_FOV_preview.png", transparent=True)
pl.show()
