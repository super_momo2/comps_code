from six.moves import tkinter as Tk

from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg)#, NavigationToolbar2TkAgg)
# Implement the default Matplotlib key bindings.
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
from six.moves import tkinter as Tk
import numpy.random as rand
import matplotlib.pyplot as plt
from tkinter import filedialog
import numpy as np
import scipy as sp
from scipy import io
from copy import deepcopy
import time

root = Tk.Tk()
root.wm_title('Power Spec analysis tool')

file_path = filedialog.askopenfile(parent=root,mode='rb',title='Choose a file:')
#print(file_path.name)
file_path_tk = Tk.StringVar(root,value=file_path.name[-50:])

a = sp.io.readsav(file_path.name,verbose=False)

x1 = Tk.DoubleVar(root)
x2 = Tk.DoubleVar(root)
y1 = Tk.DoubleVar(root)
y2 = Tk.DoubleVar(root)

vmax1 = Tk.DoubleVar(root)
vmin1 = Tk.DoubleVar(root)

fft_data  = np.flip(a.fft_data,axis=1)
frequency = a.freq1*1e3
fft_power = a.fft_powermap

def save_figs(fig1,fig2,path,name):
    fig1.savefig(path+name+'_image_'+str(time.time())+'.png')
    fig2.savefig(path+name+'_plot_'+str(time.time())+'.png')
    print('Figures saved!')
    return 0

def plot_power_image(figure_label,freq_index1):
    vmax1.set(max(fft_data[freq_index1,100:900,100:900].flatten()/2))
    vmin1.set(min(fft_data[freq_index1,100:900,100:900].flatten()))
    ax = fig.add_subplot(111)
    ax.imshow(figure_label[freq_index1,:,:],vmin=vmin1.get(),vmax=vmax1.get(),cmap='gist_gray',extent=(0,1000,0,1000))
    ax.set_xlabel('Solar X, arcsec')
    ax.set_ylabel('Solar y, arcsec')

def plot_power_image1(figure_label,freq_index1,vmin11,vmax11):
    fig.add_subplot(111).imshow(figure_label[freq_index1,:,:],vmin=vmin11,vmax=vmax11,cmap='gist_gray',extent=(0,1000,0,1000))

Label_Input = Tk.Label(root,text='Input file:')
Label_Input.grid(row=1,column=0,sticky='W')

Entry_Load_file = Tk.Entry(root,textvariable=file_path_tk,width=40)
Entry_Load_file.grid(row=1,column=0)

Entry_x_1 = Tk.Entry(root,width=10,textvariable=x1)
Entry_x_1.grid(row=1,column=1,sticky='W')

Entry_x_2 = Tk.Entry(root,width=10,textvariable=x2)
Entry_x_2.grid(row=1,column=1)

Entry_y_1 = Tk.Entry(root,width=10,textvariable=y1)
Entry_y_1.grid(row=1,column=1,sticky='E')

Entry_y_2 = Tk.Entry(root,width=10,textvariable=y2)
Entry_y_2.grid(row=1,column=2,sticky='W')

fig = Figure(figsize=(10, 10), dpi=100)
#fig.add_subplot(111).imshow(fft_power,cmap='gist_gray')
plot_power_image(fft_data,0)

canvas = FigureCanvasTkAgg(fig, master=root)  # A tk.DrawingArea.
canvas.draw()
#canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)
canvas.get_tk_widget().grid(column=0,row=2,sticky='W')

fig2 = Figure(figsize=(10, 10), dpi=100)
mean_power = np.mean(fft_data[:,400:500,400:500],axis=(1,2))
ax2 = fig2.add_subplot(111)
ax2.plot(frequency,mean_power,'r--')
ax2.set_xlabel('Frequency, mHz')
ax2.set_ylabel('Power, ')
ax2.set_yscale('log')
ax2.set_xscale('log')
#ax2[0].set_ylabel('Frequency, mHz')


canvas2 = FigureCanvasTkAgg(fig2, master=root)  # A tk.DrawingArea.
canvas2.draw()
#canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)
canvas2.get_tk_widget().grid(column=1,row=2,sticky='W')


Label_scale = Tk.Label(root,text='Frequency, mHz:')
Label_scale.grid(row=3,column=0,sticky='W')

Scale_freq  = Tk.Scale(root,from_=frequency[0],to=frequency[-1],resolution=(frequency[1]-frequency[0]),orient='horizontal')
Scale_freq.grid(row=3,column=0)


def refresh_plot():
    freq_index = int(Scale_freq.get()/(frequency[1]-frequency[0]))
    #print(freq_index)
    #fig.add_subplot(111).imshow(fft_data[freq_index,:,:],vmin=min(fft_data[freq_index,100:900,100:900].flatten()),vmax=max(fft_data[freq_index,100:900,100:900].flatten()/2),cmap='gist_gray')
    plot_power_image(fft_data,freq_index)
    canvas.draw()

def refresh_plot1():
    freq_index = int(Scale_freq.get()/(frequency[1]-frequency[0]))
    #print(freq_index)
    #fig.add_subplot(111).imshow(fft_data[freq_index,:,:],vmin=min(fft_data[freq_index,100:900,100:900].flatten()),vmax=max(fft_data[freq_index,100:900,100:900].flatten()/2),cmap='gist_gray')
    plot_power_image1(fft_data,freq_index,vmin1.get(),vmax1.get())
    canvas.draw()

button1 = Tk.Button(root,text='plot',command=refresh_plot)
#button1.pack(side=Tk.TOP)
button1.grid(row=4,column=0,sticky='W')


button11 = Tk.Button(root,text='Scale Image',command=refresh_plot1)
#button1.pack(side=Tk.TOP)
button11.grid(row=5,column=0)

Entry_vmin = Tk.Entry(root,width=10,textvariable=vmin1)
Entry_vmin.grid(row=4,column=0,sticky='E')

Entry_vmax = Tk.Entry(root,width=10,textvariable=vmax1)
Entry_vmax.grid(row=4,column=0)

def callback1(event):
    x1.set(int(event.xdata))
    y1.set(int(event.ydata))

def callback2(event):
    x2.set(int(event.xdata))
    y2.set(int(event.ydata))
    #print(x1.get(),x2.get(),y1.get(),y2.get())
    freq_index = int(Scale_freq.get()/(frequency[1]-frequency[0]))
    fft_data1 = deepcopy(fft_data)

    fft_data1[freq_index,(int(1000-y1.get())):(int(1000-y1.get())+1),int(x1.get()):int(x2.get())] = 1e3     #bottom horizontal line
    fft_data1[freq_index,(int(1000-y2.get())):(int(1000-y2.get())+1),(int(x1.get())):(int(x2.get()))] = 1e3 #top horizontal line
    fft_data1[freq_index,(int(1000-y2.get())):(int(1000-y1.get())),int(x1.get()):int(x1.get()+2)] = 1e3    #left vertical line
    fft_data1[freq_index,(int(1000-y2.get())):(int(1000-y1.get())),int(x2.get()):int(x2.get()+2)] = 1e3    #right vertical line
    plot_power_image(fft_data1,freq_index)
    #fig.add_subplot(111).imshow(fft_data1,vmin=min(fft_data[freq_index,100:900,100:900].flatten()),vmax=max(fft_data[freq_index,100:900,100:900].flatten()/2,),cmap='gist_gray')
    canvas.draw()


canvas.mpl_connect('button_press_event',callback1)
canvas.mpl_connect('button_release_event',callback2)




"""
def on_key_press(event):
    print("you pressed {}".format(event.key))
    key_press_handler(event, canvas, toolbar)

canvas.mpl_connect("key_press_event", on_key_press)
"""



def plot_frequencies():
    #plot averaged power spectrum over a region
    mean_power = np.mean(fft_data[:,int(1000-y2.get()):int(1000-y1.get()),int(x1.get()):int(x2.get())],axis=(1,2))
    fig2.add_subplot(111).plot(frequency,mean_power,'*--')
    canvas2.draw()

def clear_plot():
    #plot averaged power spectrum over a region
    mean_power = np.mean(fft_data[:,int(1000-y2.get()):int(1000-y1.get()),int(x1.get()):int(x2.get())],axis=(1,2))
    fig2.clf()
    ax2 = fig2.add_subplot(111)
    ax2.plot(frequency,mean_power,'r*--')
    ax2.set_xlabel('Frequency, mHz')
    ax2.set_yscale('log')
    ax2.set_xscale('log')
    canvas2.draw()


button2 = Tk.Button(root,text='Plot power spectrum',command=plot_frequencies)
button2.grid(row=4,column=1)

button3 = Tk.Button(root,text='Erase plot',command=clear_plot)
button3.grid(row=4,column=1,sticky='W')


'''
def on_key_press(event):
    print("you pressed {}".format(event.key))
    key_press_handler(event, canvas2, toolbar)


canvas2.mpl_connect("key_press_event", on_key_press)
'''


def _quit():
    root.quit()     # stops mainloop
    root.destroy()  # this is necessary on Windows to prevent
                    # Fatal Python Error: PyEval_RestoreThread: NULL tstate

button_savefig = Tk.Button(master=root,text='Save Figures',command=save_figs(fig,fig2,'/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_code/','FFT_'))
button_savefig.grid(row=5,column=0,sticky='E')

button = Tk.Button(master=root, text="Quit", command=_quit)
#button.pack(side=Tk.BOTTOM)
button.grid(row=5,column=1)

Tk.mainloop()
# If you put root.destroy() here, it will cause an error if the window is
# closed with the window manager.
