#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 16:27:25 2019

Calculate the fluxes from RADYN simulation

@author: molnarad
"""

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import radynpy as rp
from matplotlib.animation import FuncAnimation
from RHlib import *


def animate(i):
    line.set_ydata([ el / 1e5 for el in rad.vz[i, :]])
    line.set_xdata([ el / 1e5 for el in rad.z[i, :]])

    line.axes.set_title(f'Time = {rad.time[i]:5.2f} [seconds]')
#    line.axes.set_ylim(1.2*min([ el / 1e5 for el in rad.vz[i, :]]),
#                       1.2*max([ el / 1e5 for el in rad.vz[i, :]]))

rad = RADYN_atmos('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
                  + 'Comps_2/comps_code/radyn_vtab.cdf')

fig, ax = plt.subplots(figsize=(6, 3.72), dpi=250)
#rad.line_intensity_with_cont(2, -1)
line = ax.plot([ el / 1e5 for el in rad.z[0, :]],
               [el / 1e5 for el in rad.vz[0, :]], 'r.--', lw=2)[0]
#plt.tight_layout()
#ax.set_xlim(-300,2100)
ax.set_ylim(-.6,.6)
ax.set_xlabel('Height [km]')
ax.set_ylabel('V$_z$ amplitude [km/s]')
plt.grid()
anim = FuncAnimation(
    fig, animate, interval=50, frames=len(rad.time))

anim.save('RADYN_v_f_0.003_A_0.0055.mp4')

plt.draw()
plt.show()

