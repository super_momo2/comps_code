#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 11 15:19:01 2020

Separate the different regions of the solar surface by their observable
properties and save masks [0 or 1 valued arrays] to be applied for further
study of the data.

criteria for separating the areas of interest:
    -- Plage -- line core intensity of 8542 Ã is above the roof
    -- Network -- line core of intensity lower than Plage, but higher than
    than internetwork; High 5 minute power;
    -- Internetwork -- dark in all + high 3 min power; 3 min power is
    in the FFT file ~ mean of frequency array [2:4] indices
    -- Fibrils -- intermediate between Network and Internetwork; Low 3 min
    power;
    -- Penumbra -- Dark + I guess high 5 min power?


    Ca_int file:   Fig_1_Ca_int.fits
    Ha width file: FOV2_widths_Ha.sav ["widths" dic entry]
    FFT 8542 vel.lc: fft.el.lc.23Apr2017.target2.all.8542.ser_172509.sav
        Dic objects ["Freq1", "FFT_Powermap"]

@author: molnarad
"""

import numpy as np
import scipy as sp
from scipy.ndimage import gaussian_filter
import matplotlib.pyplot as pl
from astropy.io import fits
from test_color_maps import diverging_colors, qualitative_colors
from matplotlib.colors import ListedColormap
from matplotlib.cm import register_cmap

from PYBIS import construct_name, load_fft_data
# Load data

class masks():
    def __init__(self, Ca_int, Ha_width, FFT_Ca_vel_lc, mag):
        '''
        Class for computing the masks for different regions of the solar
        surface, described in the functions below.

        Parameters
        ----------
        Ca_int : ndarray (Nx, Ny)
            Ca II 8542 line core intensity data
        Ha_width : ndarray (Nx, Ny)
            H-alpha line core width array
        FFT_Ca_vel_lc : ndarray (Nx, Ny)
            FFT of the Ca 8542 vel.lc

        Returns
        -------
        None.
        '''

        self.Ca_int   = Ca_int
        self.Ha_width = Ha_width
        self.fft_data = FFT_Ca_vel_lc
        self.fft_mean = np.mean(self.fft_data)
        self.nx       = Ca_int.shape[0]
        self.ny       = Ca_int.shape[1]
        self.plage    = np.zeros((self.nx, self.ny))
        self.penumbra = np.zeros((self.nx, self.ny))
        self.internetwork = np.zeros((self.nx, self.ny))
        self.network  = np.zeros((self.nx, self.ny))
        self.fibrils  = np.zeros((self.nx, self.ny))
        self.Ca_int   = Ca_int
        self.Ha_width = Ha_width
        self.mag  = mag

    def find_plage(self):
        self.plage  = np.abs(self.mag) > 400
        self.plage1 = self.Ca_int > 550
        self.plage = (gaussian_filter(self.plage * 1.0, sigma=15)
                      + gaussian_filter(self.plage1 * 1.0, sigma=15))
        self.plage = self.plage > .75
        #self.plage[:500, :] = 0

    def find_network(self):
        self.network = (self.Ca_int <500)
        self.network = self.network * (self.Ha_width > 1.2)
        self.network = self.network * (self.Ca_int > 350)
        self.network = gaussian_filter(self.network * 1.0, sigma=15)
        self.network = self.network > 0.45

    def find_internetwork(self):
        self.internetwork = self.Ca_int < 600
        self.internetwork = gaussian_filter(self.internetwork *1.0,
                                            sigma=15)
        self.internetwork = self.internetwork > 0.55


    def find_fibrils(self):
        self.fibrils = self.Ca_int <400
        self.fibrils = self.fibrils * (self.Ha_width < 1.2)
        self.fibrils = gaussian_filter(self.fibrils *1.0,
                                       sigma=20)
        self.fibrils = self.fibrils > .25
        return 0

    def find_penumbra(self):
        self.penumbra[400:, 0:200] = self.Ha_width[400:, 0:200] < 1.0
        self.penumbra = gaussian_filter(self.penumbra *1.0, sigma=40)
        self.penumbra = self.penumbra > 0.4

    def determine_areas(self):
        self.find_plage()
        self.find_network()
        self.find_fibrils()
        self.network      = self.network  * (1 - self.plage)
        self.fibrils      = self.fibrils * (1 - self.network)

    def plot_masks(self):
        '''
        Plot the different masks with different colors using a discreet
        colormap.

        Returns
        -------
        None.

        '''

        newcmp = ListedColormap(qualitative_colors(3))
        labels = ["Fibrils", "Network", "Plage"]

        total_mask = (1.1 * self.fibrils
                      + 2.1 * self.network + 3.1 * self.plage)

        alpha_param = 0.3

        fig1 = pl.figure(dpi=250, figsize=(4, 4))
        pl.imshow(self.Ha_width, cmap='gray', origin='lower',
                  vmin=.9,
                  vmax=1.4, extent=extent_FOV1_IBIS)

        im1 = pl.contourf(total_mask, cmap=newcmp, levels=[1, 2, 3, 4],
                          alpha=alpha_param, origin='lower',
                          extent=extent_FOV1_IBIS, linewidths=4)
        print(f"Mask is {total_mask}")
        m = pl.cm.ScalarMappable(cmap=newcmp)
        m.set_array(total_mask)
        m.set_clim(0.5, 5.5)
        pl.xlabel('Solar X [arcsec]')
        pl.ylabel('Solar Y [arcsec]')
        cbaxes = fig1.add_axes([0.02, -0.015, 0.95, 0.05])
        cbar = pl.colorbar(m, cax = cbaxes, boundaries=np.linspace(.5, 4.5, 4),
                           ticks=[1.25, 2.5, 3.75], fraction=0.046, pad=0.14,
                           orientation='horizontal',  anchor = (1.5, 1.0))
        #cbar.set_ticks([])
        cbar.ax.set_xticklabels(labels)

        # pl.contourf(self.plage, alpha=alpha_param, cmap='plasma')
        # pl.contourf(self.network, alpha=alpha_param, cmap='viridis')
        # pl.contourf(self.internetwork, alpha=alpha_param, cmap='jet')
        # pl.contourf(self.penumbra, alpha=alpha_param, cmap='gnuplot')

        pl.tight_layout()
        pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
                   +'2021a_Molnar_et_al_High_freq/git/git/Figures/Masks_FOV1.png',
                   transparent=True)
        #pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
        #c          +'2020b_Molnar_et_al_High_freq/git/git/Figures/Masks.png')

        pl.show()

pl.style.use('science')

data_dir = ('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
            + 'Comps_2/comps_data/IBIS/')

data_dir_hmidata = ("/Users/molnarad/CU_Boulder/Work/Manuscripts/"
                    +"2021a_Molnar_et_al_High_freq/Figures/data/")

with fits.open(data_dir_hmidata + "HMI_FOV1_cutout.fits") as hdul:
    hmi_data = hdul[0].data

IBIS_scale = 0.096
IBIS_size  = 1e3
IBIS_dx = IBIS_dy = IBIS_scale * IBIS_size

FOV1_IBIS_center_x = -857
FOV1_IBIS_center_y = -154.3
FOV1_IBIS_label_coords = [-850, -130]

extent_FOV1_IBIS = [FOV1_IBIS_center_x - IBIS_dx/2,
                   FOV1_IBIS_center_x + IBIS_dx/2,
                   FOV1_IBIS_center_y - IBIS_dy/2,
                   FOV1_IBIS_center_y + IBIS_dy/2]

temp = sp.io.readsav("Fig_1_FOV_1.sav")
ca_int_data = np.array(temp["ca"])
ab = sp.io.readsav("FOV1_widths_Ha.sav")
ha_width_data = np.array(ab['widths'])


aa = sp.io.readsav(data_dir + "fft.nb.vel.lc.142503.ca8542.sav")
fft_vel_data_ratio = np.log10(np.mean(aa['fft_data'][14:21, :, :], axis=0))# /
                        # np.mean(aa['fft_data'][4:9, :, :], axis=0))
fft_vel_data_total = np.log10(np.mean(aa['fft_data'][14:55, :, :], axis=0))# /

x_len = ca_int_data.shape[0]
y_len = ca_int_data.shape[1]


# Save the masks

m = masks(ca_int_data, ha_width_data, fft_vel_data_total, hmi_data)
m.determine_areas()


'''
im2 = pl.imshow(m.plage, cmap='plasma', origin='lower')
pl.title('Plage regions')
pl.show()
im2 = pl.imshow(m.network, cmap='plasma', origin='lower')
pl.title('Network regions')
pl.show()
im3 = pl.imshow(m.penumbra, cmap='plasma', origin='lower')
pl.title('Penumbra regions')
pl.show()
'''

pl.imshow(m.fibrils)
pl.show()

m.plot_masks()

np.savez('Masks_FOV1.npz', plage=m.plage, network=m.network, fibrils=m.fibrils)

