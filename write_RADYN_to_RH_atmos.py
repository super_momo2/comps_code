#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 21:26:00 2020

Write RADYN simulations into RH format for calculation of the mm wave radiatio

@author: molnarad
"""


import numpy as np


import matplotlib.pyplot as plt
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog
from scipy import signal


simulation_name = "radyn_ulmschneider_47000"
data_dir = "/mnt/Data/Comps_2/comps_data/RADYN_runs/"

rad = RADYN_atmos(data_dir + simulation_name + '.cdf')
rad.cut_coronal_temps(5e5)
rad.interp_atmos_height(20000)
rad.write_TF_test(True)