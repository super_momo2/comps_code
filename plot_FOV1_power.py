#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
plot FOV 1 velocity oscillation power (RMS)

@author: molnarad
"""

import numpy as np
import scipy as sp
from scipy import io
import matplotlib.pyplot as plt
from skimage.transform import downscale_local_mean
# from power_uncertain import power_uncertain

from RHlib import Spectral_Line, Instrument_Profile


def downscale(data, dims):
    """
    Downscale the dimensions of the data.

    Parameters
    ----------
    data : array
    dims : number of times to shrink the dimensions of the data

    Returns
    -------
    data_new : array


    """
    data_shape = data.shape
    new_shape  = [int(data_shape[i] / dims[i]) for i in range(len(dims))]
    print(new_shape)
    data_new   = np.zeros(new_shape)

    for ii in range(new_shape[0]):
        for jj in range(new_shape[1]):
            data_new[ii, jj] = np.mean(data[ii*dims[0]:ii*dims[1]+dims[0],
                                            jj*dims[1]:jj*dims[1]+dims[1]])
    return data_new

center_x = -857
center_y = -154
IBIS_dx = IBIS_dy = 0.096 * 1e3
size = 1e3
fontsize_axis = 13
fontsize_ticks = 9
FOV1_IBIS_center_x = -857
FOV1_IBIS_center_y = -154.3
extent_FOV1_IBIS = [FOV1_IBIS_center_x -IBIS_dx/2,
                   FOV1_IBIS_center_x + IBIS_dx/2,
                   FOV1_IBIS_center_y - IBIS_dy/2,
                   FOV1_IBIS_center_y + IBIS_dy/2]

freq_ind = [15, 35, 45, 55] #FOV 1


plt.style.use('science')

data_dir = '/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_data/'
#fft_file_ca = data_dir+'IBIS/fft.nb.vel.lc.23Apr2017.target1.all.8542.ser_141344.sav'
fft_file_ca = data_dir = data_dir + "IBIS/fft.nb.vel.lc.142503.ca8542.sav"

a = sp.io.readsav(fft_file_ca, verbose=False)
#fft_data = np.flip(a.fft_data, axis=1)
fft_data = a.fft_data
frequency = a.freq1
fft_power = a.fft_powermap



lim1 = freq_ind[0]
lim2 = freq_ind[-1]
print(f"The frequency limits in this integration are:"
      + f"{1e3*frequency[lim1]:.5f} mHz"
      + f" to {frequency[lim2]*1e3:.5f} mHz")
downscale_factor = 4

# fft_data_noise = np.mean(fft_data[-10:-1, :, :], axis=0)
#
# fft_data_downsampled = np.zeros((frequency.size, int(1000/downscale_factor),
#                                  int(1000/downscale_factor)))
# fft_data_downsampled1 = np.zeros((frequency.size, int(1000/downscale_factor),
#                                   int(1000/downscale_factor)))
#
#
#
# for ii in range(frequency.size):
#     fft_data_downsampled[ii,:,:] = downscale(fft_data[ii,:,:],
#                                              [downscale_factor, downscale_factor])
#
# for ii in range(fft_data_downsampled.shape[1]):
#     for jj in range(fft_data_downsampled.shape[2]):
#         noise_est = np.median(fft_data_downsampled[-20:, ii, jj])
#         fft_data_downsampled1[:, ii, jj] = (fft_data_downsampled[:, ii, jj]
#                                           - noise_est)
#

fft_data_downsampled = downscale_local_mean(fft_data,
                                            (1, downscale_factor,
                                             downscale_factor))
fft_data_noise = np.median(fft_data_downsampled[-25:, :, :],
                           axis=0)

fft_data_downsampled1 = (fft_data_downsampled - fft_data_noise)

df = frequency[2] - frequency[1]

plt.figure(dpi=250)
im1 = plt.imshow(np.log10(np.sum(fft_data_downsampled1[freq_ind[0]:freq_ind[-1],
                                                       :, :], axis=0)*1233),
                 vmin=-2, vmax=-0.5, cmap="inferno", origin="lower",
                 extent=extent_FOV1_IBIS)
plt.colorbar(im1, label="Log$_{10}$($\langle V^2 \\rangle$ [km$^2$/s$^2$])")
plt.xlabel("Solar X [arcsec]")
plt.ylabel("Solar Y [arcsec]")
plt.savefig("/Users/molnarad/CU_Boulder/Work/Manuscripts/"
            + "2021a_Molnar_et_al_High_freq/git/git/Figures/FOV1_v_rms.png",
            transparent=True)
plt.show()

np.savez("FOV1_average_PSD.npz", freq_FOV1 =frequency,
         FOV1_mean_PSD=np.median(fft_data_downsampled[:, 10:-10, 10:-10],
                               axis=(1, 2)))
