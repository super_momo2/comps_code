#Estimation for the uncertainties in a power spectrum based on the method presented in Hoyng,P. 1973 ApJ

import numpy as np
from numpy import convolve
import matplotlib.pyplot as plt

def movingaverage (values, window):
    #calculating the moving average of the values array, over a <window> interval of indices
    weights = np.repeat(1.0, window)/window
    sma = np.convolve(values, weights, 'valid')
    return sma

def epsilon_p(a_p2,sigma_t2,N):
    #a_p2 - coefficients of the power spectrum squared; sigma_t2 - estimated of the sigma squared; N - number of bins
    x = sigma_t2 /(N*a_p2)
    return np.sqrt(2*x-x*x)

def power_uncertain(spectrum):
    spectrum_size = spectrum.size
    N             = spectrum_size*2
    spectrum_running_average_derivative = np.gradient(movingaverage(spectrum,10))
    uncertainties = []

    #calculate the sigma

    num_av = -20 #counting from the back of the array

    aa     = movingaverage(spectrum_running_average_derivative,5)
    mean_d = np.std(aa[-20:])

    for j in range(20,spectrum_size):
        index = spectrum_size-1-j
        if (aa[index] > 0*mean_d):
            num_av = -1-j
            break


    sigma_t2 = N*np.std(spectrum[num_av:])
    median_t = np.median(spectrum[num_av:])


    A = np.std(spectrum[num_av:])/median_t
    sigma_t2 = N*median_t*(1-np.sqrt(1-A*A))
    for i in range(0,spectrum_size):
        uncertainties.append(epsilon_p(spectrum[i],sigma_t2,N))


    return uncertainties,sigma_t2/N,median_t

if __name__ == "__main__":
    from scipy import signal
    import matplotlib.pyplot as plt
    fs = 10e3
    N = 1e5
    amp = 2*np.sqrt(2)
    freq = 1234.0
    noise_power = 0.001 * fs / 2
    time = np.arange(N) / fs
    x = amp*np.sin(2*np.pi*freq*time)
    print('1')
    x += np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
    f, Pxx_den = signal.periodogram(x, fs)
    plt.semilogy(f, Pxx_den)
    plt.ylim([1e-7, 1e2])
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.show()

    print('2')
    uncertainties = power_uncertain(Pxx_den[0:100])
    print(uncertainties)












