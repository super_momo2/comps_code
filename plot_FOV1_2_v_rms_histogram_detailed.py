#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 11:16:10 2020

Compare the distributions of the v_rms of the different solar features in
both FOV 1 and FOV 2

@author: molnarad
"""

import numpy as np
import scipy as sp
from scipy import io
import matplotlib.pyplot as pl
# from power_uncertain import power_uncertain

from RHlib import Spectral_Line, Instrument_Profile
from skimage.transform import downscale_local_mean

def calc_t_vel(v_l, v_i, mu, mask_el="average"):

    v_t = (v_i - v_l*(mu**2))/(1-mu**2)
    print(f"For {mask_el} longitudinal velocity is: {np.sqrt(v_l):.2f} km/s\n"
          +f"and the transverse velocity is: {np.sqrt(v_t):.2f} km/s\n")

    return 0

ca_coeff = 1233
freq_ind_FOV1 = [14, 6, 7, 55] #FOV 1 frequency limits of integration
freq_ind_FOV2 = [12, 47] # FOV 2 frequency limits of integration
dx = 60 #cutout around edges to remove the corner effects
fontsize_axis = 13
fontsize_ticks = 9

downscale_factor = 1

#Load Mask files

mask_file_FOV1 = "Masks_FOV1.npz"
mask_file_FOV2 = "Masks_FOV2.npz"
labels = ['plage', 'network', 'fibrils']

pl.style.use('science')

data_dir = '/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_data/'
fft_file_ca_FOV1 = (data_dir + "IBIS/fft.nb.vel.lc.142503.ca8542.sav")
fft_file_ca_FOV2 = (data_dir + "IBIS/fft.nb.vel.lc.155413.ca8542.sav")

temp = sp.io.readsav(fft_file_ca_FOV1, verbose=False)
fft_data_FOV1  = np.flip(temp.fft_data, axis=1)
frequency_FOV1 = temp.freq1


lim1_FOV1 = freq_ind_FOV1[0]
lim2_FOV1 = freq_ind_FOV1[-1]

lim1_FOV2 = freq_ind_FOV1[0]
lim2_FOV2 = freq_ind_FOV1[-1]

print("The frequency limits in this integration for FOV 1 are:"
      + f"{1e3*frequency_FOV1[lim1_FOV1]:.5f} mHz"
      + f" to {frequency_FOV1[lim2_FOV1]*1e3:.5f} mHz")


fft_data_downsampled_FOV1 = downscale_local_mean(fft_data_FOV1,
                                                (1, downscale_factor,
                                                 downscale_factor))
fft_data_noise_FOV1 = np.median(fft_data_downsampled_FOV1[-25:, :, :],
                                axis=0)

fft_data_downsampled_FOV1 -= fft_data_noise_FOV1

v_rms_FOV1 = np.log10(np.sum(fft_data_downsampled_FOV1[lim1_FOV1:lim2_FOV1, :, :],
                             axis=0)*ca_coeff)


print("The frequency limits in this integration for FOV 2 are:"
      + f"{1e3*frequency_FOV1[lim1_FOV2]:.5f} mHz"
      + f" to {frequency_FOV1[lim2_FOV2]*1e3:.5f} mHz")

temp = sp.io.readsav(fft_file_ca_FOV2, verbose=False)
fft_data_FOV2  = np.flip(temp.fft_data, axis=1)
frequency_FOV2 = temp.freq1

fft_data_noise_FOV2 = np.median(fft_data_FOV2[-25:, :, :], axis=0)

fft_data_downsampled_FOV2 = downscale_local_mean(fft_data_FOV2,
                                                 (1, downscale_factor,
                                                  downscale_factor))
fft_data_noise_FOV2 = np.median(fft_data_downsampled_FOV2[-25:, :, :],
                                axis=0)
fft_data_downsampled_FOV2 -= fft_data_noise_FOV2

v_rms_FOV2 = np.log10(np.sum(fft_data_downsampled_FOV2[lim1_FOV2:lim2_FOV2, :, :],
                             axis=0)*ca_coeff)

colors = np.flip(['#332288', '#88CCEE', '#117733', '#DDCC77', '#CC6677'])

fig, ax = pl.subplots(3, 1, figsize=(7.3/1.8, 14/2), dpi=125)

for el in range(len(labels)):

    mask_el = labels[el]

    with np.load(mask_file_FOV1) as file:
        mask = np.array(file[mask_el])
    with np.load(mask_file_FOV2) as file:
        mask = np.array(file[mask_el])

    v_rms_FOV1_m = (mask * v_rms_FOV1)[dx:-dx, dx:-dx]
    X = np.ma.masked_equal(v_rms_FOV1_m, 0.0)
    X = X.compressed()
    X = X[~np.isnan(X)]
    v_rms_FOV1_region = X
    median_distr_FOV1 = np.median(v_rms_FOV1_region)

    # print(f"The median for {mask_el} for FOV1 is {median_distr_FOV1}")
    v_rms_FOV2_m = (mask * v_rms_FOV2)[dx:-dx, dx:-dx]

    X = np.ma.masked_equal(v_rms_FOV2_m, 0.0)
    X = X.compressed()
    X = X[~np.isnan(X)]
    v_rms_FOV2_region = X
    median_distr_FOV2 = np.median(v_rms_FOV2_region)
    # print(f"The median for {mask_el} for FOV2 is {median_distr_FOV2}")

    n, bins = np.histogram(v_rms_FOV1_region.flatten(), bins=200,
                           density=True,
                           range=[-2.5, 0.1])
    ax[el].plot(bins[1:], n, label="FOV 1", color=colors[0])
    f1 = np.interp(median_distr_FOV1, bins[1:], n)
    ax[el].plot([median_distr_FOV1, median_distr_FOV1], [0, f1],
                '--', color=colors[0])

    n, bins = np.histogram(v_rms_FOV2_region.flatten(), bins=200,
                           density=True,
                           range=[-2.5, 0.1])
    ax[el].plot(bins[1:], n, label="FOV 2", color=colors[1])
    f2 = np.interp(median_distr_FOV2, bins[1:], n)
    ax[el].plot([median_distr_FOV2, median_distr_FOV2], [0, f2],
                '--', color=colors[1])

    n, bins = np.histogram(v_rms_FOV2_region.flatten() + np.log10(0.4**2),
                           bins=200, density=True,
                           range=[-2.5, 0.1])

    ax[el].plot(bins[1:], n, label="FOV 2 \n$@ \mu$=0.4", color=colors[2])
    median_distr_FOV3 = np.log10(0.4**2) + median_distr_FOV2
    # print(f"The median for {mask_el} for FOV2 @ mu=.41 is {median_distr_FOV2}")

    f3 = np.interp(median_distr_FOV3, bins[1:], n)
    ax[el].plot([median_distr_FOV3, median_distr_FOV3], [0, f3],
                '--', color=colors[2])
    ax[el].set_ylabel("Occurence")

    ax[el].legend(fontsize=8, loc="upper left")
    # ax[el].set_title("Ca II IR $\langle$V$^2_{rms}\\rangle$ vel.lc for " + mask_el)

    calc_t_vel(10**median_distr_FOV2, 10**median_distr_FOV1,
               0.4, mask_el=mask_el)

ax[0].text(-0.325, 1.85, "Plage")
ax[1].text(-0.35, 1.78, "Network")
ax[2].text(-0.35, 1.45, "Fibrils")
ax[-1].set_xlabel("Log10[$\langle$V$^2\\rangle$ [(km/s)$^2$]]")
pl.tight_layout()
pl.savefig("/Users/molnarad/CU_Boulder/Work/Manuscripts/"
           +"2021a_Molnar_et_al_High_freq/git/git/Figures/"
           + "V_rms_comparison_FOV12.png", transparent=True)
pl.show()
