#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 15:03:27 2020

Plot the distribution of the HF white noise in the data from the
simulation

@author: molnarad
"""

import numpy as np
import matplotlib.pyplot as pl

data_dir = ("/Users/molnarad/CU_Boulder/Work/Chromospheric_business/"
            + "Comps_2/comps_code/")

with np.load(data_dir + "Pxx_average_profile_FOV2.npz") as a:
    Pxx = a["Pxx"]

freq = np.linspace(0, 1/7., num =Pxx.shape[2])

ROS = ["plage", "network", "fibrils", "internetwork", "penumbra"]
log_mean_all = np.zeros((len(ROS), Pxx.shape[2]-1))

for i in range(len(ROS)):
    mask = ROS[i]

    data = np.log10(Pxx[:, i, 1:].flatten())
    index = [freq[el//1000] for el in range(1000*75)]



    Nbins_x = 75
    Nbins_y = 100

    pl.figure(dpi=250)
    h, xedges, yedges, im = pl.hist2d(index, data, bins=(Nbins_x, Nbins_y),
                                      cmap='gray',
                                      range=[[freq[0], freq[-1]], [-6, -1]])

    mean     = [np.mean(el) for el in np.swapaxes(Pxx[:, i, 1:], 0, 1)]
    median   = [np.median(el) for el in np.swapaxes(Pxx[:, i, 1:], 0, 1)]
    log_mean = [np.sum(yedges[1:]*el)/np.sum(el) for el in h]
    log_mean_all[i, :] = log_mean

    pl.plot(xedges[1:], np.log10(mean), 'b.--', label='Mean[Data]')
    pl.plot(xedges[1:], log_mean, 'g.--', label='Mean[Log$_{10}[Data]]}$')
    pl.plot(xedges[1:], np.log10(median), 'r.--', label='Median[Data]')

    #pl.ylim(-3.5, -1.0)
    pl.title(mask + " average HF white noise distribution")
    pl.xlabel("Frequency [Hz]")
    pl.ylabel("Power [km$^2$.s$^{-2}$/mHz]")
    pl.legend()
    pl.savefig("/Users/molnarad/Desktop/" + mask + "average_HF_noise.png")
    pl.show()

np.savez("log_mean_HF_noise_av_prof_FOV2.npz", log_mean_all = log_mean_all)
