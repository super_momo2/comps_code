#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 22:25:36 2020

Plot the acoustic flux in all the models + and where is their TR

@author: molnarad
"""

import numpy as np
import matplotlib.pyplot as plt
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog
from scipy import signal

data_dir = ("/Users/molnarad/CU_Boulder/Work/Chromospheric_business"
            + "/Comps_2/comps_data/RADYN_models/")
radyn_model = "radyn_ulmschneider_3000.cdf"

rad = RADYN_atmos(data_dir + radyn_model)

fig, ax = plt.subplots(1, 3, figsize=(9, 3), dpi=250)

time_steps = [0, 710, 1000]

for el in time_steps:
    ax[0].plot(rad.z[el, :]/1e5, rad.temperature[el, :], '.--',
               label=f"Time = {int(el*3)} s")
    ax[1].plot(rad.z[el, :]/1e5, rad.rho[el, :], '.--',
               label=f"Time = {int(el*3)} s")
    ax[2].plot(rad.z[el, :]/1e5, rad.Ne[el, :], '.--',
               label=f"Time = {int(el*3)} s")
    
for el in ax: 
    el.legend()
    
ax[0].set_yscale("log")
ax[0].set_xlabel("Height [km]")
ax[0].set_ylabel("Temperature [K]")
ax[0].set_xlim(-50, 2000)
ax[0].grid()


ax[1].set_yscale("log")
ax[1].set_xlabel("Height [km]")
ax[1].set_ylabel("Density [g.cm$^{-3}$]")
ax[1].grid()
ax[1].set_xlim(-50, 2000)

ax[2].set_yscale("log")
ax[2].set_xlim(-50, 2000)
ax[2].set_xlabel("Height [km]")
ax[2].set_ylabel("N$_e$ density [cm$^{-3}$]")
ax[2].grid()

plt.tight_layout()
plt.savefig("RADYN_atmos.png", transparent=True)
plt.show()