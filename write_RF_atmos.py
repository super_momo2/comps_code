#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 14:38:23 2019

@author: molnarad
"""

from RHlib import Atmos_cmass, Atmos_height

atmos_dir  = '/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_data/Model_Atmospheres/'
model_name = 'FALD_11'

FALC = Atmos_height(atmos_dir+model_name, 83)
FALC.write_response_test(0, True)
