#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 22:56:46 2020

@author: molnarad
"""


import numpy as np 
import scipy as sp
from RHlib import *
from scipy import signal, io, interpolate
from PYBIS import count_zeroes_in_array
from scipy.interpolate import interp1d
#TURBO CHARGE YOUR PYTHON -- Make it parallel 
import multiprocessing 
from joblib import Parallel, delayed 
import time 

ADU     = 2.5 # e-/DN
cs      = 3e5 # km/s
waveHa  = 6562.8 #Å
waveCa  = 8542.0 #Å


plt.style.use('science')

class spectral_data():
    '''Class for computing the rms velocity noise from the
      spectral data
    '''
    
    def __init__(self, wave, I, ADU, cadence, dl):
        '''Initiate the class
            Inputs:
            -- wave: ndarray[N_waves], wavelenght grid;
            -- I: ndarray[N_waves], intensity profile;
            -- ADU: float, Analog-Digital-unit conversion of the
            camera used;
            -- cadence: float, cadence of the observation;
            -- dl: float, regular wavelength grid we're remapping 
            the data on; 
            '''
        
        self.waves = wave
        self.I    = I
        self.c    = 3e5
        self.ADU  = ADU
        self.cadence = cadence
        self.dl   = dl
        
    def interp_waves_reg_grid(self):
        self.waves_interp = np.linspace(self.waves[0], self.waves[-1], 
                                        num=int((self.waves[-1] 
                                             - self.waves[0])/self.dl))
        
    
    def interp_I_reg_grid(self):
        self.interp_waves_reg_grid()
        self.noisyI_interp = np.zeros((self.noisyI.shape[0], 
                                        self.waves_interp.shape[0]))
        for i in range(self.noisyI.shape[0]):
            f_I = interp1d(self.waves, 
                           self.noisyI[i, :], kind='cubic')
            self.noisyI_interp[i, :] = f_I(self.waves_interp) 
    
    def calc_noise(self):
        #Calculate the noise floor for each intensity point
        self.noise = np.array([np.sqrt(el/ADU) for el in self.I])
    
    
    def calc_velocity(self):
        mean_wave = np.mean(self.measured_prop)
        self.measured_prop -= mean_wave
        self.velocity = self.c * self.measured_prop / mean_wave
        
        
    def calc_rms_vel(self, N_samples, function, *args):
        # Produce N_sample random samples and calc the v_rms
        
        self.N_samples = N_samples
        self.calc_noise()
        
        noisyI = [(self.I + np.random.poisson(lam=self.noise))
                  for el in range(N_samples)]
        self.noisyI = np.array(noisyI)
        
        self.interp_I_reg_grid()
        
        self.measured_prop = [function(self.waves_interp, el, 
                                        *args) for 
                              el in self.noisyI_interp]
        
        self.measured_prop = np.array(self.measured_prop)
        self.calc_velocity()
        
        self.ff, self.Pxx = signal.periodogram(self.velocity,
                                               self.cadence) 
    
    def calc_rms(self, N_samples, function, *args):
        # Produce N_sample random samples and calc the v_rms
        
        self.N_samples = N_samples
        self.calc_noise()
        
        noisyI = [(self.I + np.random.poisson(lam=self.noise))
                  for el in range(N_samples)]
        self.noisyI = np.array(noisyI)
        self.measured_prop = [ function(self.waves, el, *args) for 
                              el in self.noisyI]
        
        self.measured_prop = np.array(self.measured_prop)        
        self.ff, self.Pxx = signal.periodogram(self.measured_prop,
                                               self.cadence) 
    
    
    def plot_results_vel(self):
        # Plot both velocity and PSD of the parameter measured
        
        fig, ax = plt.subplots(2,1)
        fig.dpi = 250
        fig.figsize=(10,10)
        ax[0].plot(np.linspace(0, (self.N_samples-1)*self.cadence,
                               num=(self.N_samples)),
                   self.velocity)
        
        ax[0].set_xlabel('Time [s]')
        ax[0].set_ylabel('Measured quantity [km/s]')
        
        ax[1].loglog(self.ff[1:], self.Pxx[1:]/1000) #.mHz-1
        ax[1].set_xlabel('Frequency [mHz]')
        ax[1].set_ylabel('Power [km$^2$.s$^{-2}$.mHz$^{-1}}$]')
        
        for el in ax:
            el.grid(alpha=0.5)
        
        plt.show()
        
def create_interp_data(intensityFile, waveFile, reg_grid_waves):
    
    intensity   = np.loadtxt(intensityFile).transpose()
    waves       = np.loadtxt(waveFile).transpose() + waveHa

    noised = [np.sqrt(el/ADU) for el in intensity]
    noise = [np.random.normal(0,scale=el) for el in noised]
    noisyI = noise + intensity
    reg_grid_I     = np.interp(reg_grid_waves, waves, noisyI)
    return reg_grid_I

def compute_noise():
    ROS = ["plage", "network", "fibrils", "internetwork", "penumbra"]
    data_dir = ('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
            + 'Comps_2/comps_data/IBIS/')
    N_samples = 150
    dx = 50
    cadence   = 1/3.5
    dl        = 0.05 # Å is the spacing of the 
    freq      = np.linspace(0, cadence/2, num = int(N_samples/2 + 1))
    filename  = 'CaII8542_profiles_fast.sav'
    Pxx       = np.zeros((len(ROS), int(N_samples/2 + 1)))
    masks     = np.load(data_dir + 'Masks_FOV2.npz')


    for el in range(len(ROS)):

        mask = masks[ROS[el]][dx:-dx, dx:-dx]
        
        aa = io.readsav(data_dir+filename)
        I = aa["profiles"][:, dx:-dx, dx:-dx]
        
        waves = aa['waves']
        waves_1 = np.append(waves, waves[-1]+waves[1]-waves[0])
        
        mask_zeroes = count_zeroes_in_array(mask)
        coeff_zeroes = mask.shape[0]**2 /(mask.shape[0]**2 - mask_zeroes)
        
        I_masked = [el * mask for el in I]
        I_mean = [np.mean(ell) * coeff_zeroes for ell in I_masked]
    
        #plt.plot(waves_1, I_mean, '.--', label=ROS[el])
    
        spec_profile1 = spectral_data(waves_1+8542.1, I_mean, 
                                  ADU, cadence, dl)
    
        spec_profile1.calc_rms_vel(150, calc_v_lc, 2, -2, 7)
        #spec_profile1.plot_results_vel()
        
        Pxx[el, :]      = spec_profile1.Pxx / 1000
    
    return Pxx

    
N_trials = 10

#Pxx = compute_noise()
#print(Pxx)
t0 = time.time()
Pxx = Parallel(n_jobs=4)(delayed(compute_noise)() 
                         for el in range(N_trials))
t1 = time.time()
print(f"Time for {N_trials} jobs is {(t1-t0):.2f} seconds")
    
