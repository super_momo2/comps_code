#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
plot distribution of the FOV 1/2  v_rms and their mean

@author: molnarad
"""


import numpy as np
import scipy as sp
from scipy import io
import matplotlib.pyplot as plt
# from power_uncertain import power_uncertain

from RHlib import Spectral_Line, Instrument_Profile
from skimage.transform import downscale_local_mean

def downscale(data, dims):
    """
    Downscale the dimensions of the data.

    Parameters
    ----------
    data : array
    dims : number of times to shrink the dimensions of the data

    Returns
    -------
    data_new : array


    """
    data_shape = data.shape
    new_shape  = [int(data_shape[i] / dims[i]) for i in range(len(dims))]
    print(new_shape)
    data_new   = np.zeros(new_shape)

    for ii in range(new_shape[0]):
        for jj in range(new_shape[1]):
            data_new[ii, jj] = np.mean(data[ii*dims[0]:ii*dims[1]+dims[0],
                                            jj*dims[1]:jj*dims[1]+dims[1]])
    return data_new

ca_coeff = 1233
center_x = -857
center_y = -154
IBIS_dx = IBIS_dy = 0.096 * 1e3
size = 1e3
fontsize_axis = 13
fontsize_ticks = 9
FOV1_IBIS_center_x = -857
FOV1_IBIS_center_y = -154.3
extent_FOV1_IBIS = [FOV1_IBIS_center_x -IBIS_dx/2,
                    FOV1_IBIS_center_x + IBIS_dx/2,
                    FOV1_IBIS_center_y - IBIS_dy/2,
                    FOV1_IBIS_center_y + IBIS_dy/2]

freq_ind_FOV1 = [14, 6, 7, 55] #FOV 1 frequency limits of integration
freq_ind_FOV2 = [12, 47] # FOV 2 frequency limits of integration
dx = 60 #cutout around edges to remove the corner effects


plt.style.use('science')

data_dir = '/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_data/'
fft_file_ca_FOV1 = (data_dir + "IBIS/fft.nb.vel.lc.142503.ca8542.sav")
fft_file_ca_FOV2 = (data_dir + "IBIS/fft.nb.vel.lc.155413.ca8542.sav")

a = sp.io.readsav(fft_file_ca_FOV1, verbose=False)
fft_data  = np.flip(a.fft_data, axis=1)
frequency = a.freq1
fft_power = a.fft_powermap


lim1 = freq_ind_FOV1[0]
lim2 = freq_ind_FOV1[-1]

print("The frequency limits in this integration are:"
      + f"{1e3*frequency[lim1]:.5f} mHz"
      + f" to {frequency[lim2]*1e3:.5f} mHz")

downscale_factor = 4

fft_data_noise = np.median(fft_data[-25:, :, :], axis=0)

fft_data_downsampled = downscale_local_mean(fft_data, 
                                            (1, downscale_factor, 
                                             downscale_factor))
fft_data_downsampled -= fft_data_noise

df_FOV1 = frequency[2] - frequency[1]

v_rms_FOV1 = np.log10(np.sum(fft_data_downsampled[lim1:lim2, dx:-dx, dx:-dx], 
                             axis=0)*ca_coeff)
v_rms_FOV1 = v_rms_FOV1.flatten()


# Change region of interest to be inside FOV 
# and use the correct time series
#

a = sp.io.readsav(fft_file_ca_FOV2, verbose=False)
fft_data  = np.flip(a.fft_data, axis=1)
frequency = a.freq1
fft_power = a.fft_powermap

lim1 = freq_ind_FOV2[0]
lim2 = freq_ind_FOV2[-1]
print(f"The frequency limits in this integration are:"
      + f"{1e3*frequency[lim1]:.5f} mHz"
      + f" to {frequency[lim2]*1e3:.5f} mHz")
downscale_factor = 4

fft_data_noise = np.mean(fft_data[-10:-1, :, :], axis=0)

fft_data_downsampled = np.zeros((frequency.size, int(1000/downscale_factor),
                                 int(1000/downscale_factor)))
fft_data_downsampled1 = np.zeros((frequency.size, int(1000/downscale_factor),
                                  int(1000/downscale_factor)))

fft_data_downsampled = downscale_local_mean(fft_data, 
                                            (1, downscale_factor, 
                                             downscale_factor))

for ii in range(fft_data_downsampled.shape[1]):
    for jj in range(fft_data_downsampled.shape[2]):
        noise_est = np.mean(fft_data_downsampled[-25:, ii, jj])
        fft_data_downsampled1[:, ii, jj] = (fft_data_downsampled[:, ii, jj]
                                          - noise_est)

df_FOV2 = frequency[2] - frequency[1]

v_rms_FOV2 = np.sum(fft_data_downsampled1[lim1:lim2, dx:-dx, dx:-dx], axis=0)
v_rms_FOV2 = np.log10(v_rms_FOV2 * ca_coeff).flatten() 

colors = np.flip(['#332288', '#88CCEE', '#117733', '#DDCC77', '#CC6677'])                    
    
plt.figure(dpi=250)

n, bins = np.histogram(v_rms_FOV1, 
                       range=(-3., 1), bins=350, normed=True)
plt.plot(bins[1:], n, label="FOV 1", color=colors[0])
X = np.ma.masked_invalid(v_rms_FOV1)
    
mean_distr = np.percentile(X.compressed(), 50)
f = np.interp(mean_distr, bins[1:], n)
plt.plot([mean_distr, mean_distr], [0, f], '--', color=colors[0])

n, bins = np.histogram(v_rms_FOV2, 
                       range=(-3., 1), bins=350, normed=True)
plt.plot(bins[1:], n, label="FOV 2", color=colors[1])
X = np.ma.masked_invalid(v_rms_FOV2)
    
mean_distr = np.percentile(X.compressed(), 50)
f = np.interp(mean_distr, bins[1:], n)
plt.plot([mean_distr, mean_distr], [0, f], '--', color=colors[1])

n, bins = np.histogram(v_rms_FOV2 + np.log10(.41**2), 
                       range=(-3., 1), bins=350, normed=True)
plt.plot(bins[1:], n, label="FOV 2 $\\times \mu^2 $", color=colors[2])
X = np.ma.masked_invalid(v_rms_FOV2+ np.log10(.41**2))
    
mean_distr = np.percentile(X.compressed(), 50)
f = np.interp(mean_distr, bins[1:], n)
plt.plot([mean_distr, mean_distr], [0, f], '--', color=colors[2])
plt.legend(fontsize=8)
plt.ylabel("Occurence")
plt.xlabel("Log$_{10}$(V$^2_{rms}$ [km$^2$/s$^2$])")
plt.savefig("/Users/molnarad/CU_Boulder/Work/Manuscripts/"
            + "2021a_Molnar_et_al_High_freq/git/git/Figures/Ca_FOV_power_comparison.png",
            transparent=True)

plt.show()