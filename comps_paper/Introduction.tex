\section{Introduction} \label{sec:Intro}

Even though the Sun is one the closest astrophysical objects to Earth it continues to surprise us with new physical phenomena as we explore its mysteries in novel parameter space. Fully understanding our host
star is important beyond astrophysics, as the solar magnetic field is capable of initiating spectacular geomagnetic storms on Earth. Thus, in order to further our knowledge of the Sun 
there are multiple upcoming observatories and space missions (DKIST, PSP, Solar Orbiter) which are going to cover unexplored regions of space, spectral range and polarization of light of our host star.  

The most famous conundrum in solar physics constitutes of the missing energy source to maintain its outer atmosphere (the corona) thousand times hotter than its surface (the photosphere). The scientific community still
does not agree on the physical mechanism responsible of the coronal heating, even almost a century after the temperature of the corona was first inferred. 
 
The photosphere is the visible surface of the Sun in the continuum emitted in the optical part of the spectrum. In this region of the solar atmosphere 
the high density determines the plasma behavior as the fluid flows dominate. The frequent collisions and high opacity keep the plasma close to local thermodynamic equilibrium (LTE) (on the contemporary observable scales). Most of the hydrogen 
in this layer is not ionized and the dominant opacity source is H$^-$, hydrogen 
atoms capturing an additional electron in a free-bound process. The typical temperature of the photosphere is about 5800 K, density of about 10$^{-4}$ kg.m$^{-3}$ and magnetic field 
strengths of tens to thousands of Gauss, resulting in plasma $\beta$\footnote{The plasma beta is defined as the ratio of the gas pressure over magnetic pressure.} parameter far greater than one.
Magnetic field measurements in the photosphere are the input of all space weather forecasts nowadays.    

On the other hand, the outermost layer of the solar atmosphere is the corona, which can be see in its full glory only during a full solar eclipse from the ground. This tenuous layer of the solar atmosphere extends out in the 
heliosphere as it transitions into the solar wind with no distinct boundary. The solar wind permeates the Solar System and is a key player in the space weather on Earth. Thus understanding the origins of the
solar wind is crucial, but
there are still many unknowns in its acceleration mechanism \citep{Solar_wind_review}. The density drops significantly in the corona down to 10$^{-12}$ kg.m$^{-3}$, the temperatures rises to  
several million degrees Kelvin and most atomic populations are ionized to high degree. 
We usually observe the corona in far-ultraviolet and X-ray lines of multiply ionized atomic species, or forbidden lines in the optical and IR. Furthermore, as the density drops and the field expands, dropping its 
strength down to couple of Gauss, the plasma beta falls to significantly less than one; i.e., the magnetic fields determine the structure of the corona. 

There is no consensus what provides the energy required for balancing out the energy losses of the corona, which are a height dependent mixture of conduction, radiation losses and outward enthalpy flux.  
There are multiple theoretical models able to provide the required energy flux, but we still require observational 
evidence to distinguish among them. 
The two prevalent models currently are the nano-flaring model, where magnetic fields constantly braid and reconnect on small scales and MHD wave turbulence, in which waves originating from the 
photosphere propagate upwards to dissipate their energy 
in the corona through turbulent cascade. Models are not exclusive of each other, hence most probably a combination of them is at work in the solar atmosphere~\citep{Coronal_Heating_3}.

The transition layer between the plasma dominated photosphere to the magnetically dominated corona spans about 15 scale heights over a few thousand kilometers 
and is probably the least well understood layer of the solar atmosphere. 
During a total solar eclipse this layer is seen as a ring of reddish glow. This layer is called the chromosphere --- coming
from the Greek ``chromos'' --- colorful, the literal translation meaning a sphere of color. The chromosphere encompasses 
the region where the plasma beta is close to one and complex physical behaviour can occur as 
magnetic effects and hydrodynamic effects are equally important. 
Furthermore, as energy is deposited in this layer of solar atmosphere, hydrogen becomes increasingly ionized with height. This energy sink keeps the chromosphere almost isothermal, until the location 
when all of the hydrogen becomes ionized and the chromosphere cannot cool anymore. At this height the temperature rises sharply, the density drops further, 
and an abrupt change to coronal parameters occurs in the layer of 
the solar atmosphere called the transition region.  

Balancing the radiative losses of the non-magnetic chromosphere requires a similar energy flux to the corona --- about 10$^4$ W.m$^{-2}$~\citep{1977ARA&A..15..363W}. 
Like the corona, there is still no consensus on the mechanism of sustaining the chromosphere in its thermal state. The two most widely accepted theoretical 
frameworks for chromospheric heating are the same as for the corona: wave induced MHD turbulence and small scale ubiquitous reconnection. 
There is evidence for both in the chromosphere, but again the lack of unambiguous observations is the hurdle for 
not answering the chromospheric heating problem definitely.
A major difficulty in observing the chromosphere is that most spectral diagnostics are formed far from LTE (non-LTE from now on). The relaxation of the LTE 
simplification make 
the interpretation of our observations more complex and model dependent. Often, we have to 
rely on nontrivial data-inversions algorithms to interpret the results. 

Waves in the chromosphere are ubiquitous and play a significant role in structuring it. Even though the p-mode oscillations in the photosphere are evanescent, they drive acoustic 
waves with periods 
closer to 3 minutes in the chromosphere \citep{Chromosphere_review}. As the 3-minute acoustic modes propagate to higher layers in the solar atmosphere, where the density decreases and the sound speed stays almost constant,
the wavetrains steepen and drive 
dissipative turbulence leading to heating. Shocks ionize the hydrogen and helium out of equilibrium and maintain the ionization state far away from statistical equilibrium~\citep{Carlsson_and_Stein}. 
However, TRACE observations coupled with simulations have shown that these high-frequency waves (5-28 mHz) do not provide
enough energy to sustain the non-magnetic chromosphere~\citep{Fossum_and_Carlsson}.

A myriad of other theories have been proposed to solve the chromospheric energy conundrum~\citep{Chromospheric_heating_review}. One of the many alternatives that has received a lot of attention
in the recent years is the idea of magnetohydrodynamic (MHD) wave dissipation in the lower solar atmosphere. 
The physical framework for this theory is that the transition layer of $\beta\approx$1, which is a zone of MHD wave mode conversion. Alf\'{e}n waves are non-dissipative, whereas
others, like the fast and the slow modes are more easily damped, so mode conversion can cause non-dissipative modes to be dissipated.
Thus, the power of acoustic and wave oscillations reaching this layer can be converted to other types of dissipative waves which 
deposit their energy there~\citep{Alfven_wave_review}. Observations of such waves across the solar atmosphere are present and there are some hints in recent observational studies that they might provide the missing
energy supply to maintain the quiet chromosphere~\citep{Alfvenic_waves_heat_penumbra}.

A not well explored aspect of the wave theory of chromospheric heating 
is the contribution of high-frequency waves. This is due to the fact that high temporal cadence observations of the chromosphere at the high spatial resolution  
required to resolve the chromospheric structures are challenging. However, previous work~\citep{Hansteen_HF_waves} has shown that wavelet power exists up to 50 mHz in upper chromospheric and transition region lines in data
take with SUMER on the SOHO spacecraft. 
Hence, we will explore the high-frequency regime in the lower and mid-chromosphere with novel observations with IBIS at the Dunn Solar Telescope.

In this paper we present high cadence observations of the solar chromosphere with IBIS at the Dunn Solar Telescope and ALMA in a previously unexplored temporal regime. Our observations were
supported the Interface Region Imaging Spectrograph (IRIS)~\citep{IRIS} and the Hinode satellite~\citep{Hinode_mission_overview}. In Section~\ref{sec:Observations} we describe the data 
acquisition procedure and reduction steps. We present evidence for excess of high-frequency power in the 
velocity measurement of the H$\alpha$ line presented in Section~\ref{sec:High_frequency_power}. The unexpected signal is not detected in any of the other chromospheric diagnostics obtained simultaneously. Hence, we 
performed a thorough analysis of the possible systematic effects which can introduce a periodic disturbance in the data. This procedure for systematics is described in detail in Section~\ref{sec:Systematics}. Since we did not 
find any evidence for instrumental origin of the excess power we performed magnetic field extrapolation of the region with the Hinode SOTSP to look for correlations between the magnetic field properties and the 
detected high-frequency power. We present
this modelling work in Section~\ref{sec:Solar_explanations}. The conclusions from this study and the planned future work are presented in Section~\ref{sec:Conclusions}. 
