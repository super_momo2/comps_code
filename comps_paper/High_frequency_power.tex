\section{High-frequency power} 
\label{sec:High_frequency_power}
\subsection{Spectral line parameters from the IBIS scans}
\label{subsec:spectral_line_properties}

The IBIS observations provided us with spectral scans covering fully the spectral lines from the core out to the wings. The full spectral coverage allowed us to derive
multiple parameters from each profile, including the line core minimum and positions, line bisectors and equivalent width. We calculated three different velocity measures, each with its own benefits and disadvantages.

The first and most commonly used is the Doppler velocity (Vel$_{Int}$ in Table~\ref{table:Detections}) calculated from the shift of the minimum of the spectral line. In order to determine the location of the minimum of the intensity 
in the spectrum, we fitted it around the central region of the line with a parabola. This method for determining the line of sight velocity is imprecise for highly scattering lines with wide flat cores 
(like H$\alpha$) due to the difficulty for the fitting routine to find the minimum precisely especially in the presence of noise.

The center-of-gravity velocity (COG, V$_{COG}$ in Table~\ref{table:Detections}) is the first moment over the whole line with respect to the wavelength. The advantage of this method is that it is independent of the 
spectral resolution of the spectrograph used~\citep{Uitenbroek2003}. However, the COG method suffers from contamination from the wings of the line, which has photospheric contributions
and has to be interpreted with caution.

The third type of velocity measure is the bisector Doppler velocity (Vel$_{Bis}$ in Table~\ref{table:Detections}). We calculated the bisector of the line at 
50\% intensity of the intensity at $\pm$ 1 {\AA} from the line core and the displacement of the center of the bisector gives us an estimate for the Doppler shift of the lines. This method provides a more precise 
estimate of the line-core position due to the steepness of the wings of the line~\citep{Cauzzi_2008}.  

In all three velocity measures' descriptions we alluded to different formation heights for the different parts of the H$\alpha$ line profile. However, the real situation is a bit more complicated - the response functions
of the wing of H$\alpha$ are rather wide, which makes their interpretation not straightforward~\citep{H_alpha_formation}.

We measured an equivalent width of the lines as the negative of the sum of the intensities under in the $\pm$ 1{\AA}. This definition differs from the standard one
of equivalent width, because we cannot measure the continuum while observing chromospheric lines, which are wider than 
the free spectral range of the instrument, which is determined by the prefilter.

\subsection{Fourier power spectra of the H$\alpha$ Dopplergrams}
To explore the high-frequency power in the chromospheric oscillations we calculate the absolute amplitude of the Fourier spectrum, which is called a power spectrum. The power spectrum, unlike the Fourier spectrum contains 
information only about the amplitudes of the constituent periodic components,
but not their relative phases. As with the Fourier spectrum, the Nyquist frequency for a power spectrum is the inverse of twice the interval between measurements.

Previous studies have shown that the power spectra of different chromospheric diagnostics derived from both H$\alpha$ and Ca II 8542 lines 
exhibit a significant high-frequency tail in their power from the 3-minute oscillations peak down to the noise floor in previous studies~\citep{Evans_power}.
This behaviour has been shown to follow a power law down to 50 mHz and has been
interpreted as 
turbulence driven by the intermittent shock waves~\citep{Reardon_Turbulence} and has been modeled numerically for different magnetic field geometries~\citep{2015ApJ...812...71C}. 
Utilizing the extremely high cadence of our data set\footnote{No other study known to the author has measured the line profiles of these spectral 
lines at such cadence.}, we are aiming 
to extend the previous work to higher frequencies, as this could provide an more accurate constraint on the amount of energy being dissipated 
through waves in the quiet chromosphere.

Each data set contains about one hundred scans and lasts for about six to ten minutes.
The achieved Nyquist frequency is five times better than the one in \cite{Reardon_Turbulence}. The power spectrum of each of the spectral properties described in section~\ref{subsec:spectral_line_properties} was calculated in a pixelwise fashion in IDL.  
The average value of each pixel was reduced to zero to reduce the DC component of the power spectrum and then an apodization was applied on the 6 border pixels 
of each time series to reduce the Gibbs phenomenon.

While examining the power spectra of the different spectral diagnostics from the fast scans, we found a surprising high-frequency power excess at some 
frequencies in some of the H$\alpha$ velocity diagnostics. An example is 
shown in Figure~\ref{fig:initial_detection} where the bump at around 30 mHz is present in the H$\alpha$ scan, 
but not in the Ca II 8542 scan. For FOV1 the bump was around
30 mHz and had a width of about 10 mHz; for FOV2 the bump was around 44 mHz and has width of about 10 mHz as well.

Table~\ref{table:Detections} summarizes the detections of the high-frequency bump 
across the various fast scans and the different derived spectral properties. The detection
was based on the averaged power spectrum from the central region of the images, where the surprising peak was 
determined to be statistically significant. The uncertainties estimation method utilized
is described in detail in Appendix~\ref{app:Errors}. 
We used only the central parts of the IBIS image because 
seeing distortions affect the regions farther from the AO lock point of the image to a greater extent.

The unexpected peak is seen only in the H$\alpha$ fast scans, while not present in any of the fast Ca II scans or any of the slow scans. This is not surprising for 
the slow scans, since their cadence of about 15 seconds was too slow to detect the 
high-frequencies. We detect the unexpected signal in the spectral parameters COG velocity, the bisector velocity,  
the line core intensity. It is important to note that these spectral
diagnostics are not completely entirely uncorrelated. Furthermore, we detect the signal only once in an equivalent width measurement in the 14:18 UT time series and
never in the line core position. 
Furthermore, the peak frequency is about 30 mHz for FOV 1 while it is closer to 45 mHz for FOV 2. The different frequencies observed may correlate with the fact that 
FOV1 has stronger magnetic field on average than FOV 2, based on the Hinode SOT SP measurements of the photospheric magnetic field.
This would align well with the hypothesis that the signal could originate from 
magnetic loops acting as resonant cavities for Alfv\'{e}nic waves as described in Section~\ref{subsec:Resonant_Alfvenic_waves}. In this physical model, 
the resonant frequency is inversely proportional to the crossing time, which decreases with increasing Alfv\'{e}nic speed and increasing magnetic field strength accordingly.    

The only other instrument in our observation set with the required temporal resolution to detect these signals is ALMA. 
The power spectrum of the intensity (temperature) variations for FOV 2 with ALMA Band 3 is presented in 
Figure~\ref{fig:IBIS_to_ALMA_power_spectrum} as the blue markers. The ALMA power spectrum was derived following the same preparatory steps as for IBIS. We averaged the power spectra of the central square 40$''$ 
of the FOV, which is the region of highest sensitivity of ALMA. There is no evidence of excess power above the power law slope at the frequency of the bump 
in the ALMA data as seen in Figure~\ref{fig:IBIS_to_ALMA_power_spectrum}. We 
did a numerical experiment to smear down the IBIS spectral profiles to the nominal ALMA 
resolution of 1$''$ for Band 3. The derived COG 
velocity is presented as the yellow markers in Figure~\ref{fig:IBIS_to_ALMA_power_spectrum}. The signal persists in the downgraded IBIS observations, which
means that the coherent scale associated with this signal is of the order of 1$''$ or greater. There are multiple reasons to not expect to see the same
features in H$\alpha$ and ALMA -- ALMA sample a different thermodynamical property at higher region in the atmosphere on average, compared to H$\alpha$~\citep{Molnar_1}; second, the temperature precision of ALMA Band 3 is several hundred degrees Kelvin, which could be the greater than 
the temperature variations in the solar atmosphere associated with this signal, if there are any.

The instrumental systematics are investigated in Section~\ref{sec:Systematics}. The solar origin hypothesis has a starting point that the phenomenon creating the excess signal does not have a thermal imprint in ALMA 
and is not detected in any of the spectral diagnostics derived from Ca II 8542. Furthermore, we detect the signal most prominently in the velocity signal, which makes the idea of Alfv\'{e}n waves plausible. We will investigate the relation between the magnetic field topology and the 
excess high-frequency power in Section~\ref{subsec:Prop_waves}.


\begin{figure}
\includegraphics[width=\columnwidth]{Figures/Fig_initial_detection}
  \caption{Averaged power spectra of the COG velocity of the central regions (100-th to 900-th row, 100-th to 900-th column) of the of Ca II scan at 14:13UT (blue) and H$\alpha$ (yellow) 
scan at 14:18UT. The power of the H$\alpha$ scan is multiplied by 10 for clarity of representation. The power law decrease of the COG velocity power holds until the noise
floor is reached at around 60 mHz. Our measurements are in agreement with previous measurements of chromospheric velocity fields~\citep{Reardon_Turbulence}. The peak is easily noticeable in the H$\alpha$ power spectrum 
at around 30 mHz, which corresponds to a period of about 30 seconds. Surprisingly, we do not see a corresponding peak in the Ca II 8542 power spectra despite the 
lower noise floor. }
\label{fig:initial_detection}
\end{figure}


\begin{table*}
  \begin{center}
\begin{tabular}{ccccccc}
\underline{Time, UT} & \underline{Spectral Line} & \underline{Vel$_{COG}$} & \underline{Vel$_{Int}$} & \underline{Line Core Intensity} & \underline{Width} & \underline{Vel$_{Bis}$}\\
14:18-14:24 & H$\alpha$ 6563 {\AA} & \textcolor{blue}{Yes} & \textcolor{red}{No}&  \textcolor{blue}{Yes}& \textcolor{blue}{Yes} & \textcolor{blue}{Yes} \\
15:18-15:24 & H$\alpha$ 6563 {\AA} & \textcolor{blue}{Yes} & \textcolor{red}{No}& \textcolor{blue}{Yes} & \textcolor{red}{No}& \textcolor{blue}{Yes} \\
15:46-15:54 & H$\alpha$ 6563 {\AA}  & \textcolor{blue}{Yes}& \textcolor{red}{No}& \textcolor{red}{No} & \textcolor{red}{No}& \textcolor{blue}{Yes} \\
17:11-17:21 & H$\alpha$ 6563 {\AA}  & \textcolor{blue}{Yes}& \textcolor{red}{No}& \textcolor{blue}{Yes}& \textcolor{red}{No}& \textcolor{blue}{Yes}\\
18:12-18:19 & H$\alpha$ 6563 {\AA}  & \textcolor{blue}{Yes}& \textcolor{red}{No}& \textcolor{blue}{Yes}& \textcolor{red}{No}&  \textcolor{red}{No} \\ 
14:13-14:18 & Ca II 8542 {\AA} & \textcolor{red}{No} & \textcolor{red}{No}& \textcolor{red}{No}& \textcolor{red}{No}&  \textcolor{red}{No}\\
15:13-15:18 & Ca II 8542 {\AA}  & \textcolor{red}{No}& \textcolor{red}{No}& \textcolor{red}{No}& \textcolor{red}{No}&  \textcolor{red}{No}  \\
15:39-15:46 & Ca II 8542 {\AA}  & \textcolor{red}{No}& \textcolor{red}{No}& \textcolor{red}{No}& \textcolor{red}{No}&   \textcolor{red}{No} \\
17:04-17:11 & Ca II 8542 {\AA} & \textcolor{red}{No}& \textcolor{red}{No}& \textcolor{red}{No}&  \textcolor{red}{No}&  \textcolor{red}{No} 
\end{tabular}
\caption{Summary of detection of the high-frequency excess signal around 40 mHz in the different spectral line properties in the fast 
    scans. Vel$_{COG}$ stands for Doppler velocity derived by the center of gravity of the spectral line. Vel$_{Int}$ is 
    velocity derived from the intensity minimum of the line core. Vel$_{Bis}$ is the velocity 
derived from the bisector at 50\% intensity of the line core. The bisector velocity could provide
information about the line of sight velocity at different (lower) heights in the atmosphere compared to the core. All the detections 
reported in this table are statistically significant, following Appendix~\ref{app:Errors}. Further discussion of the derived spectral 
parameters is included in Section~\ref{subsec:spectral_line_properties}.} 
\label{table:Detections}
  \end{center}
  \end{table*}

\begin{figure}
  \includegraphics[width=\columnwidth]{Figures/ALMA_IBIS_power_spec.png}
  \caption{Power spectrum from ALMA Band 3 of FOV 2 (blue) averaged over the central 40$''$ region of the field of view. The ALMA intensity is converted in units of brightness temperature (Kelvin). The yellow curve is the power spectrum of COG velocity 
  from IBIS H$\alpha$ scan downgraded to the nominal resolution of ALMA of 1$''$. The unexpected peak persists in the IBIS data smeared to the ALMA resolution, whereas it is not detected by ALMA. }
\label{fig:IBIS_to_ALMA_power_spectrum}
\end{figure}

\subsection{Properties of the high-frequency power excess}
The high-frequency signal is detected with the highest contrast in the COG velocity measurement of H$\alpha$. We measured the power of the high-frequency bump 
as the ratio of the sum of the frequency bins 
spanning the peak over the average power in the three frequency bins before the peak. For FOV1 the region of the peak was between frequencies 26 mHz and 33.8  mHz and
the reference points between 20.7 mHz and 23.4 mHz; for FOV2 the region of the peak was between frequencies 40.9  mHz and 47.7 mHz and
the reference points between 34.1 mHz and 36.9 mHz.
The result is shown in Fig~\ref{fig:Where}
for the 14:18 UT time series (top row) and for 17:11 UT (bottom row). The left columns shows the COG velocity, in which the signal is detected, and the right 
columns present the line center velocity (V$_{Int}$ in Table~\ref{table:Detections}), in which the 
signal is not detected. The signal is clearly correlated with
solar structures, as seen in the left column. Furthermore, it is easily observed that the images of excess power resemble the width maps presented in 
Figures~\ref{fig:IBIS_FOV_1} and \ref{fig:IBIS_FOV_2}. 

Figure~\ref{fig:Power_to_Width_to_Int} shows the scatter plot between excess power and intensity (upper panel) and excess power 
with equivalent line width (lower panel). 
In the upper panel we see that the excess power is concentrated of regions of medium intensity of the line core of H$\alpha$, while there is a trend between the 
equivalent width and the excess power. The equivalent width in H$\alpha$ is determined mostly by the opacity~\citep{Molnar_1} 
in the line. Thus we observe the excess power in the regions with less opacity in H$\alpha$ - this suggests that the excess power might be related to 
the regions of the solar surface with fewer magnetic structures with high H$\alpha$ opacity~\citep{H_alpha_formation}.

The excess power corresponds to a velocity amplitude of about 100 m/s, which in
terms of spectral resolution is about 2.2 m{\AA} for H$\alpha$. This is well below the spectral resolution of IBIS of 20 m{\AA}, but this velocity measure is derived 
from hundreds of scans each of which comprising of twenty five spectral
measurements. Averaging in such a fashion over thousands of spectral measurements allows us to detect 
such small velocities in a statistical fashion with IBIS.
This velocity amplitude corresponds to a flux of 
about 10 W/m$^2$ in the middle chromosphere (based on density of 10$^{-5}$ kg.m$^{-3}$) which is 
not energetically significant for maintaining the chromospheric basal state.


\begin{figure*}
  \includegraphics[width=\textwidth]{Figures/Where}
  \caption{Excess power at the bump for different data chromospheric diagnostics from different data series: \emph{upper left}: COG Velocity for the time series at 14:18 UT; \emph{upper right} Line center 
  velocity from the time series at 14:18 UT; \emph{lower left}: COG Velocity for the time series of 17:11 UT; \emph{lower right}: Line center velocity from the time series at 17:11 UT. The excess power was calculated by
  the ratio of the mean power of the 4 frequency bins around the peak over the mean of the power of the three frequency bins before that. For FOV1 the frequency range of the peak was between 26 mHz and 33.8 mHz; for FOV2
  the region of the bump was between 40.9 mHz and 47.7 mHz.}
\label{fig:Where}
\end{figure*}

\begin{figure}[h!]
  \includegraphics[width=\columnwidth]{Figures/Correlation_Widht_I.png}
  \caption{Scatter plots between the excess power measured in the high-frequency peak, measured as described in the text: \emph{upper panel:} excess power with H$\alpha$ line core intensity; \emph{bottom panel:} excess power with H$\alpha$ 
  equivalent width. There is a  
 trend in the lower panel -- the excess power is seen in regions of the solar surface with narrower H$\alpha$ profiles. The regions of narrower line profiles correspond to regions with lower opacity
  in H$\alpha$, which are usually regions with fewer magnetic structures.}
\label{fig:Power_to_Width_to_Int}
\end{figure}


