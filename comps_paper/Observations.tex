\section{Observations and Data Processing} 
\label{sec:Observations}

The observations presented in this paper were obtained on 23 April 2017 as part of the first coordinated solar scientific observations with the Atacama Large (sub)-Millimeter Array (ALMA).
These observations were part of Project ID 2016.1.01129S/Cycle 4 on ALMA, and some of the initial results from these observations are included in \cite{Molnar_1}. 
The author observed at the Dunn Solar Telescope~\citep{Dunn_1} (DST), in Sunspot, New Mexico. 
The ALMA Solar campaign was further supported from the space-borne missions IRIS~\citep{IRIS} and Hinode~\citep{Hinode_mission_overview}, which provided observations with high temporal and spatial resolution in the UV and 
visible, as well as measurements of the photospheric magnetic field. Per usual, the Solar Dynamics Observatory~\citep{SDO} provided context images of the whole solar 
disc along with photospheric magnetograms. 
Two regions of the Sun were observed on 23rd April. We will refer to them in the chronological order they were observed as (field of view) FOV1 and FOV2. The regions were chosen to be at different 
locations on the solar disc such that different angles of inclination on the solar surface are observed. Furthermore, we chose the precise pointing to contain 
a mixture of network, internetwork and plage that we can sample regions of the solar atmospheres with different configurations of the magnetic field. 

FOV1 was observed between 13:50 UT - 15:14 UT at solar coordinates E 66.2$^{\circ}$ S 09.8$^{\circ}$, with inclination of $\mu=0.41$\footnote{$\mu$ is the cosine of the inclination angle of the normal of the solar region which we observe.}. FOV1 was on the leading edge of NOAA Active Region (AR) 12653, coming out from the east
limb. Context images provided from SDO \citep{SDO} and IRIS are provided in Figure~\ref{fig:context_1}. From the context imaging it is clear that the FOV1 contains some plage 
regions as well as some network and internetwork, where the large inclination smears out the granulation in the direction of the solar limb.

FOV2 was observed between 15:15 UT - 18:19 UT at solar coordinates E 04.9 $^{\circ}$ N 10.9$^{\circ}$, with inclination of $\mu=0.98$. FOV2 was centered on the leading edge the active 
region NOAA AR 12651, where we were able to observe a field of view containing a wealth of features - the leading edge of the penumbra of a 
sunspot on the east side of the field of view; quiet network in the south part of the field of view and active network in the center 
of the field of view with some plage in the north-western part of the field of view.


\begin{figure*}
\includegraphics[width=\textwidth]{Figures/Fig_1_2}
  \caption{The Figure shows context images of Field of View 1 (FOV1) from the following instruments described in the text: \textit{Left panel}: AIA 1700 {\AA} image showing chromospheric emission; \textit{Central panel}: HMI Line-of-sight (LOS) magnetogram, where black and white denote high magnetic flux, gray areas 
  depict close to zero magnetic flux; \textit{Right panel}: IRIS slitjaw image at 2796 {\AA}, similar to AIA 1700 {\AA}, but at higher resolution. 
The field of view of IBIS is shown as the green rectangle; the field of view of ALMA Band 6 is drawn as the yellow circle and the Hinode SOTSP is illustrated as the
white rectangle.}
\label{fig:context_1}
\end{figure*}



\begin{figure*}
  \includegraphics[width=\textwidth]{Figures/Fig_1_1}
  \caption{The Figure shows context images of second Field of View (FOV2) from the same facilities and wavelengths as in Figure~\ref{fig:context_1}. The IBIS field of view is represented as the green rectangle; the field of view of ALMA Band 3 is drawn as the yellow circle and the Hinode SOTSP is illustrated as the white rectangle.}
\label{fig:context_2}
\end{figure*}


\subsection{DST observations}

The DST took observations between 14:00 UT - 18:40 UT, under conditions of excellent to good seeing. The instrument setup included IBIS~\citep{Cavallini2006}, FIRS~\citep{Jaeggli2010} and ROSA~\citep{Jess2010}, 
which provided a thorough coverage of key spectral line in the optical and the near-IR parts of the spectrum. All of the aforementioned 
instruments were mounted behind the high-order adaptive optics (AO) system~\citep{AO_Rimmele} which partially compensates for the atmospheric seeing. None of the instruments were
running in polarimetric mode, as high temporal cadence was a priority.

The discussion throughout the paper is centered on the properties of the high frequencies of the observed spectra, thus we will be emphasizing on the data taken with IBIS as it was the 
only instrument which obtained spectral observations at high enough cadence to observe the physical phenomena described in Section~\ref{sec:High_frequency_power}.


\begin{table*}[]
  \begin{center}
\begin{tabular}{c c c c}

\underline{Observing Time Period, UT} & \underline{Observed Spectral Line} & \underline{Cadence, seconds} & \underline{Target}\\
14:13-14:18 & Ca II 8542 \AA  & 3.13 & FOV 1  \\
14:18-14:24 & H$\alpha$ 6563 \AA & 3.66  & FOV 1  \\
  15:13-15:18 & Ca II 8542 \AA  & 3.11 & FOV 2 \\
  15:18-15:24 & H$\alpha$ 6563 \AA & 3.68 & FOV 2 \\
  15:39-15:46 & Ca II 8542 \AA & 3.28 & FOV 2 \\
  15:46-15:54 & H$\alpha$ 6563 \AA & 3.70 & FOV 2 \\
  17:04-17:11 & Ca II 8542 \AA & 3.27 & FOV 2 \\
  17:11-17:21 & H$\alpha$ 6563 \AA  & 3.67 & FOV 2 \\
  18:12-18:19 & H$\alpha$ 6563 \AA & 3.63 & FOV 2 
\end{tabular}
\caption{Summary of the single line ``fast'' scans with IBIS obtained on 23-Apr-2017 with their cadence and the line that was observed. 
    The targets referenced in the table are described in detail in Section~\ref{sec:Observations}. The cadence given in column 3 includes the full duty cycle for one scan.} 
\label{table:Observations}
  \end{center}
  \end{table*}



\subsubsection{IBIS observations}
\label{sec:IBIS_data_reduction}

The Interferometric BIdimensional Spectropolarimeter (IBIS) is a narrowband tunable filter consisting of two Fabry-P\'{e}rot etalons in a collimated mount intended for solar chromospheric observations. 
The instrument operates between 540 nm and 860 nm making it well suited for 
observing some of the most prominent chromospheric lines available from the ground - H$\alpha$ (Balmer $\alpha$), CaII IR 8542 {\AA}, Na D1 5896 {\AA} and He D1.  
The high spectral resolution of R $>$ 200,000 \citep{Cavallini2006} and the quick tuning of the Fabry-P\'erot etalons allow for spectral scans of 
the observed field of view over the whole line profile in seconds. With sampling rate of 7 spectral positions per second, scanning even a broad line like H$\alpha$ at 25 spectral positions can be done in about 3 and a half seconds.
Furthermore, the
instrument has a field of view of 96 $''$ $\times$  96 $''$ in spectroscopic mode and 40 $''$$\times$ 90 $''$ in spectropolarimetric mode, covering the coherent structures in the 
magnetic chromosphere.\footnote{Chromospheric structures are on the order of 15 arcseconds} These properties of the
instrument provide the required spatial, spectral and temporal resolution for studying the highly dynamic chromospheric structures. 


\begin{figure*}
\includegraphics[width=\textwidth]{Figures/IBIS_FOV_2.png}
  \caption{Field of View 1 (FOV1) observed with IBIS in the following spectral line properties: \emph{upper left}: H$\alpha$ line core intensity; \emph{upper right}: H$\alpha$ line width;
  \emph{lower left}: Ca II 8542 line core intensity; \emph{lower right}: Ca II 8542 line width. The width presented in this 
  figure and in Figure~\ref{fig:IBIS_FOV_2} are derived from the sum of the bisector positions of the lines at offset of 1 {\AA}.
  Since this field of view is close to the limb the 
  chromospheric structures seem shortened in the direction of the limb (left) due to the projection effect. This visual perception can be readily 
  dismissed when we compare this figure with Figure~\ref{fig:IBIS_FOV_2} which is observed straight down at disk center.} 
  \label{fig:IBIS_FOV_1}
\end{figure*}

\begin{figure*}
\includegraphics[width=\textwidth]{Figures/IBIS_FOV.png}
  \caption{Field of View 2 (FOV2) observed with IBIS in the following chromospheric diagnostics: \emph{upper left}: H$\alpha$ line core intensity; \emph{upper right}: H$\alpha$ line width;
  \emph{lower left}: Ca II 8542 line core intensity; \emph{lower right}: Ca II 8542 line width. The elongated structures in all panels are called fibrils and are supposed to trace the chromospheric magnetic fields.
  Even though they are seen as dark slender structures, they do correspond to regions with higher line widths (in both right panels). The bright region on the top right is called plag\'{e} and is a concentration of 
  magnetic flux. It is seen as salt-and-pepper like pattern in the Ca II 8542 width as the line goes into emission when the chromospheric temperatures increase, as the line core source function is coupled tightly to 
  the local  plasma temperature.} 
  \label{fig:IBIS_FOV_2}
\end{figure*}



On 23rd April 2017 IBIS observed in the spectral lines of H$\alpha$ 6563 {\AA}, Ca II 8542 {\AA} and Na D$_1$ 5896 {\AA}. Single scan of a spectral line takes between 3 and 4 
seconds and there is an overhead of about 1.5 seconds for changing the prefilter. We utilized two different scanning strategies during the day, depending on the 
seeing conditions. The ``standard'' scan, going through the aforementioned 3 lines, took about 15 seconds, while scanning through the three lines consecutively at 41 total wavelength positions. This 
type of scan
covers thoroughly the solar atmospheric layers from the photosphere through the middle chromosphere, making it well suited
for studying the propagation of energy through the lower solar atmosphere.

During stints of excellent seeing we ran 
spectral line scans (\textbf{``fast''} scans) of individual lines, where each scan 
consisted of 25 and 21 wavelength points in H$\alpha$ and in Ca II 8542 {\AA} respectively. This was done to closely match the ALMA two-second sampling. These scans are described in detail in 
Table~\ref{table:Observations}.
Scanning a single line avoids the overhead for changing prefilters. The achieved cadence was about 3.5 seconds for a single spectral scan. These 
fast scans were intended to capture the fast dynamics of the chromosphere, 
at high temporal frequencies rarely explored before. Since these scans had a single repetition of the unique wavelength points we were not able to use reconstruction
techniques which depend on multiple exposures (like speckle reconstruction \citep{Speckle} or Multi-Object Multi-Frame Blind Deconvolution \citep{MOMFBD}), 
hence these scans were ran only during stints of excellent seeing conditions.

\subsubsection{IBIS data reduction}
The standard initial data reduction procedure for IBIS was followed: removal of the dark current, applying the standard linearity correction, 
flatfielding (for the white-light, the narrowband channel requires additional step), readout correction and fringe removal. 

IBIS takes narrowband images which are affected by the time varying seeing. However, the structures imaged by IBIS also change in wavelength, hence the instrument has a reference 
white-light channel centered around 630 nm, which is fed with the same optical input as the narrowband 
(spectral), but diverted before the Fabry-P\'erot etalons. This broadband 
channel provides a mean to estimate the seeing effects on our observations by comparing the white-light images with a white-light reference not affected by the 
seeing distortions.   

To estimate the optical differences between the two imaging channels an equidistant dot grid was placed in the focal plane of the
telescope. The measured positions in the regularly spaced dots on the images of the dotgrid gave us the mapping between the narrowband channel and the white light 
channel. The static rotation between the channels was measured to be about half a degree and the plate scale differences were calculated to be $\pm$1.5\% of the nominal 
(0.096 $''$/pixel) plate scale. The deviation from the nominal plate scale was the same in both channels in the same direction, meaning that the influence 
of an optical element upstream from the beam splitter for the two channels was likely the reason for this distortion.

Collimated mounts of two Fabry-P\'{e}rot etalons 
introduce a shift of the transmission profile away from the center of field view. 
Since the light rays detected 
by different pixels have different angles of incidence on the glass plates of the etalons, the 
difference in the optical path of the different light rays results in a blueshift of the transmission
profile of the instrument. The blueshift has a quadratic dependence on distance the from the center of the field of view, as concentric circles in the image plane 
of the instrument correspond to equal incidence angles on the glass plates of the etalons.
In order to quantify this systematic blueshift in the transmission profile, 
a scan through the spectral line of interest is performed while the instrument is illuminated with quasi-uniformly illuminated input. This quasi-uniformly
illuminated field
provides that no solar features will introduce inaccuracy in the mean transmission profile. This flatfield feed to the instrument is achieved
by defocusing the telescope and setting the telescope's guiding in a random walk around the disk center, in order to smear out the solar features in the field of view. Furthermore,
random errors are introduced in the AO, leading to further smearing of the image due to the introduced high order aberrations.

The flatfield spectral line profile for each pixel was measured as described in the previous paragraph. Then it is interpolated on a regular wavelength grid and the relative blueshift for each pixel is determined.  
Having the transmission profile for each pixel, we can normalize them all to estimate the gain table (flatfield) for the narrowband channel. 

The last step of the processing of the  IBIS data is reducing the effects of atmospheric seeing. 
As mentioned before, the white-light channel is used to estimate the seeing distortions by comparing the white-light images to a reference 
white-light image not affected by the seeing, such as a time-averaged image or a reference in space such as the HMI on the SDO spacecraft~\citep{HMI}. 
Knowing the seeing distortions from the reference channel we can remove the bulk of them from the narrowband data. 

The excellent seeing conditions on allowed us to do a single exposure per wavelength position in order to achieve higher temporal resolution.
The use of post-processing techniques for removing seeing effects generally require multiple exposures per wavelength position to provide phase information through the multiple realizations of the seeing. However,
this significantly increases the scan time for a line. Our observations were partially corrected in real-time by the AO system and were taken under excellent seeing conditions. However, the residual seeing effects
were on the order of several pixels and aligning our data sets with other observatories 
(like IRIS, SDO or Hinode) was mandatory for further analysis. The procedure for destretching the narrowband images to SDO was the following: 

\begin{enumerate}
  
\item Simultaneous exposures at a single wavelength position are taken in both the white-light and narrowband channels. The simultaneity provides the same seeing distortions effect on both the  
white-light and the narrowband data. 

\item The broadband white-light images are precisely co-aligned to the closest HMI white-light images using a full frame cross correlation. Afterwards, successively smaller boxes of sizes ranging from 
  9$''$ $\times$ 9$''$ to 
1.7$''$ $\times$ 1.7$''$ are cross correlated between the IBIS white-light and the HMI white-light to remove the small scale distortions of the seeing. This method of local correlation tracking is described for example in 
    \cite{LCT} and we used an IDL implementation of it developed in the NSO.
    Even though the HMI resolution is about six times coarser than the IBIS resolution (0.6 $''$) and lower cadence (45 seconds)
the resultant destretched images have stability on the order of a pixel. 
This mapping created from the cross-correlation between the white-light images and HMI gives us an estimate of the seeing aberrations, providing us with a way to map our narrowband data on a regular grid, unaffected by the
atmospheric distortions.  

\item The narrowband images are mapped on a regular grid with scale of 0.096 $''$.pixel$^{-1}$ by applying the static distortion map derived from the dotgrid (which makes them white-light like) and then 
applying the destretch mapping from the corresponding white-light image to HMI to remove the seeing effects and the static aberrations.

\end{enumerate}

Destretching the narrowband images in this way results in a data set mapped onto an
even, fixed scale spatial grid with scale of 0.096$''$.pixel$^{-1}$. The average residual seeing distortions are estimated to be less than a 
pixel, which is sufficient for studying structures with sizes significantly larger than a pixel. Furthermore, this analysis technique does not rely on a comparison with a mean solar image from the data itself, which 
avoids the potential of removal of genuine flows in the solar atmosphere. This approach facilitates the comparison with other data sets from other instruments, like AIA and IRIS as they easily can be mapped to
common grid with HMI as well. 


\subsection{ALMA observations}

The Atacama Large Millimeter Array (ALMA) became available for solar observations in Cycle 4 (2017) after extensive testing \citep{ALMA_1}. The continuum wavelength bands available for solar 
observations are Band 3 (centered around 3 mm/100 GHz) and Band 6 (centered around 1.25 mm/240 GHz). The continuum in these wavelength ranges forms in the chromosphere under LTE conditions, 
as the main source of opacity is from free-free processes. Furthermore, the ALMA intensity can be
interpreted under the Rayleigh-Jeans limit as brightness temperature~\citep{ALMA_ref}.

The solar radiation detected by ALMA does not suffer from scattering, as the source function of the mm-wavelength radiation in the solar atmosphere is coupled to the local plasma black body function. 
Hence, ALMA is a useful tool to study the thermal structure of the chromosphere (for reference \cite{2007A&A...462L..31W}). 
Furthermore, with its improved capabilities to measure circular polarization, ALMA could provide a viable way to measure chromospheric 
magnetic fields in the future~\citep{2000A&AS..144..169G}. 
 
The ALMA observations were reduced by Yi Chai at NJIT with the standard CASA~\citep{CASA} interferometric software.
The basic principle behind it is that every distance between two dishes represents a single
point in the u-v plane (the Fourier transform of the image) of the interferometer. Thus, we have to interpolate between all the measurements to infer the Fourier transform of the 
image and reconstruct its spatial structure. 

Since the Sun is significantly larger than the beam of a single ALMA dish, we require dense coverage around the origin of the U-V plane as otherwise the large gradients of the solar image will not be resolved. This limits 
the solar observations with ALMA to only the most compact dish configurations (like C43-3 or C43-2), as we want the coherent structures of the solar magnetic network (about 15 arcseconds long) to be resolved properly. On 
the other side, choosing an array configuration which
can resolve the smallest scales of the chromosphere (on the order of an arcsecond) make these configurations preferable to ones with even shorter maximal base lengths. Furthermore, a full disk scan was used for monitoring 
the terrestrial atmosphere and to provide a stable absolute for the cross calibration of the antennas.
\bigskip

\subsection{Hinode observations}

The Solar Optical Telescope Spectropolarimeter (SOTSP) \citep{SOT_ref} and the Extreme-Ultraviolet Imaging Spectrometer (EIS) \citep{EIS_ref} on board the Hinode spacecraft observed the 
targets simultaneously as part of program HOP328b, supporting our campaign. The SOTSP is a slit-scanning spectropolarimeter, that observes the magnetically sensitive photospheric Fe I lines at 6301.6 {\AA} 
 and 6302.6 {\AA}. The SOTSP spectrograph has a slit with size of .16 $''$ $\times$ 164$''$. We used a Fast scan mode, which provides the faster option for a smaller 
 raster with higher cadence, where the step size is .32$''$. The magnetic field and atmospheric parameters retrieved from the 
full Stokes profile measurements by the SOTSP are derived with the Milne-Eddington inversion code MERLIN \citep{MERLIN_Code} and are provided online from the Hinode data center. 
The magnetic field inversion provides inferences of the magnetic field strength, inclination angle and stray light (noise measurement) for each pixel. 

The inferred photospheric magnetic field is used for magnetic field extrapolation in the upper parts of the atmosphere in order to determine the magnetic connectivity of the observed region. 
Further discussion of the magnetic field extrapolation is provided in Section~\ref{subsec:PFFFS}. 

The SOT and EIS observed two targets with coordinates [-881$''$, -154$''$] and [-83$''$, 235$''$] with a field of view of 48$''$$\times$81$''$ at the same time as the DST and ALMA. These fields of 
view overlapped well with the other facilities, as
it is shown in Figure~\ref{fig:context_1} and Figure~\ref{fig:context_2} where the SOTSP is depicted with the white rectangles. The difficulty for perfect overlap comes from the fact that the targets for 
space based missions have to picked the previous day, while ground based observatories have to offset a tiny fraction to lock their AO systems and on high contrast regions of the solar surface, such as pores or sunspots. 

A fast scan raster with the SOTSP was obtained in order to provide an estimate of the photospheric magnetic field in the regions of interest. The SOT SP observed FOV1 
between 13:50 UT and 15:14 UT, 
while FOV2 was observed between 15:19 UT and 18:59 UT. A single scan of SOTSP with the aforementioned dimensions took about ten minutes. For the further analysis we used Level 2 data,  
which is already inverted with the MERLIN code and provided on the Hinode SOTSP webpage\footnote{\url{http://sot.lmsal.com/data/sot/level2d/}}.

