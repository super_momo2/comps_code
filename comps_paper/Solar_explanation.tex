\section{Possible Solar explanations?} 
\label{sec:Solar_explanations}

After not finding any evidence of instrumental effects in Section~\ref{sec:Systematics} causing a periodic variation to the image, we will discuss several physical phenomena which could be
responsible for the excess power in the H$\alpha$ observations. 


\subsection{Propagation of waves}
\label{subsec:Prop_waves}
The Fourier transformation provides us with phase information, beside the amplitude information. The phases between the constituent functions provide us with information about the 
propagation characteristics of the waves with these frequencies. Furthermore, if we have two different diagnostics observed simultaneously, we can use their phase difference to estimate the 
propagation properties of the waves. If the two diagnostics whose phases we compare are separated in space we can estimate phase speed of the waves.

Figure~\ref{fig:phase_delay} shows a phase delay diagram between the measured H$\alpha$ COG velocity and the bisector velocity for the 17:11 UT time series. A phase delay diagram shows the phase difference of   
the different Fourier components of the two quantities of interest. Such plots are two dimensional histograms, where we plot along the x-axis frequency and along the y-axis phase difference. Since the
frequency is sampled regularly in a FFT, only the x-axis comprises of multiple 1-D histograms. If there is a phenomenon detected in both diagnostics which
has a stable phase relationship (or offset as a function of the period), then
we see a region of enhancement in the phase delay diagram. If no coherency is detected in the diagnostics, we no region of enhancement is detected in the phase delay diagram and the phases remain widely scattered. 

In this case we are interested to explore if there is any propagation signature between the two velocities (COG and bisector) in which the high-frequency 
excess power was detected. We see in Figure~\ref{fig:phase_delay} that there is significant coherency in the low frequencies around the 3-minute oscillations. An interesting observation is that 
there is no phase delay for the low frequency waves, which sheds doubt on the idea that different parts of the line sample different regions of the atmosphere. The 
coherency between the COG velocity and the bisector velocity is also pretty close to unity, which suggests that the two diagnostics are measuring the same 
velocity. Furthermore, this coherency 
disappears at higher frequencies, as previously 
pointed out by \cite{Fleck1989}. However, at 42 mHz -- the frequency of the excess power in this time series, typical for FOV2 -- we see an enhancement in the coherency of the signal as the phases cluster together. Furthermore, the phase difference in 
the region at 42 mHz is the same as the low frequency region. This might give us a clue about its origin - is it a wave driven from the photospheric convective overshoot or some sort of intensity leakage? 

\begin{figure}[h!]
  \includegraphics[width=0.5\textwidth]{Figures/Phase_delay_171115_COG_BIS.png}
  \caption{Phase delay diagram between H$\alpha$ COG velocity and the bisector velocity for the time series at 17:11 UT. The high values (yellow color) mean higher number of pixels having this phase
  delay between their COG velocity and bisector velocities. We see that at higher frequencies we don't have any coherent signals, as has been shown before. Curiously, we can see the coherent signal around 
  42 mHz which corresponds to the unexplained peak in the H$\alpha$ power spectrum.    
}
    \label{fig:phase_delay}
\end{figure}


\subsection{Resonant Alfv\'{e}n waves in low-lying closed loops}
\label{subsec:Resonant_Alfvenic_waves}

If the high-frequency signal is originating from a MHD wave, it has a signature of incompressible Alfv\'{e}n wave since we detect 
it only in Doppler displacements.
From Figure~\ref{fig:Where} we see that the signal is structured in narrow regions. Hence, we can model it as magnetic loops which act 
like 
resonant cavities. The period in this case is determined by the Alfv\'{e}n crossing time of these loops. In order to investigate more 
this possibility we need to retrieve the magnetic field topology from the SOTSP observations.

\label{subsec:PFFFS}
In order to recover the magnetic field topology in the region of interest we extrapolated the magnetic field from the boundary conditions 
provided from the photospheric observations of SOTSP. Even though the
extrapolations based on photospheric fields are not ideal for chromospheric studies, since the chromosphere is not force-free, 
for the quiet region of interest it is an acceptable approximation. Furthermore, for quiet sun
areas there is almost no twist in the magnetic field, which allows us to use as a suitable approximation a potential field extrapolation which satisfies the equation: 

\begin{equation}
\nabla \times \vec{B} = 0. 
\label{eqn:B_PFE}
\end{equation}

We used a publicly available code from M. K. Georgoulis(JHU/APL), based on \cite{PFFFS} that solves the boundary value problem based on the Fourier transforms. However, spectral methods have 
disadvantages, as the discretization of the domain if not properly expanded in the numerical setup of the problem can cause ringing. We used a single scan for both fields of view as input for the magnetic extrapolation 
as we did not want to average over multiple scans which would have smeared the fine magnetic structures. 

The extrapolated magnetic field model was then interpolated to the plate scale of IBIS and aligned to the far-wing of H$\alpha$. This step is required because the pointing precision of the spacecraft is on the order of 
arcseconds which corresponds to shifts of tens of pixels in terms of IBIS plate scale. The far H$\alpha$ wing is a suitable diagnostic to align magnetograms to, because the regions of high magnetic flux appear bright 
H$\alpha$ wing magnetograms.

After some previous work with RH \citep{RH} we \citep{Molnar_1} estimated that the height of formation of H$\alpha$ is about 1.5 Mm on average. Hence, we will use this value for the following estimates of the 
height of formation of the radiation in which we see the oscillatory power.

If the source of the unexpected peak in the power spectrum is due to Alfv\'{e}nic waves, we will expect to have a correlation between the inclination of the magnetic field and the Doppler velocity power due to projection 
effects. Alfv\'{e}nic waves are transverse in nature, creating velocity oscillations in the direction perpendicular to the direction of propagation. Hence, if we have a vertical flux tube observed from above 
we will not see any Doppler displacements or other typical imprints of Alfv\'{e}nic waves. If we have a horizontal flux tube observed from above, we will see the full amplitude of the propagating Alfv\'{e}nic wave as 
Doppler shift signature.

Figure~\ref{fig:inclination_vs_power} shows that there is no clear correlation between the excess power at 40 mHz and the field inclination derived from our extrapolation. 
This sheds some doubt on the Alfv\'{e}n wave
nature of the waves, but in general it is known that 
photospheric potential field extrapolations underestimate the horizontal magnetic fields in the chromosphere~\citep{Jing_fibrils}. Thus, there is a possibility that the noisy data from the SOTSP 
are not a suitable boundary condition for a meaningful chromospheric magnetic field extrapolation, which might underestimate
the horizontal magnetic fields. 

Figure~\ref{fig:b_strength_vs_power} shows the lack of correlation between the extrapolated magnetic field strength at the formation region of H$\alpha$ and the excess power there. This lack of correlation between the two
might suggest that a process not associated with the magnetic field permeating the solar atmosphere is responsible directly for the excess power.

\begin{figure}[h!]
  \includegraphics[width=0.5\textwidth]{Figures/Inclination_vs_power.png}
  \caption{COG velocity power at 40 mHz vs inclination of the magnetic field extrapolation at height 1.5Mm above the photosphere, where H$\alpha$ is formed. There is no correlation between inclined field lines and 
  excess power.}
    \label{fig:inclination_vs_power}
\end{figure}

\begin{figure}[h!]
  \includegraphics[width=0.5\textwidth]{Figures/B_field_magnitude_vs_power.png}
  \caption{COG velocity power at 40 mHz vs the magnetic field strength at height 1.5 Mm above the photosphere from the extrapolation. There is no correlation between the strength and 
  excess high-frequency power. }
    \label{fig:b_strength_vs_power}
\end{figure}

