\section{Systematic effects, or just the real Sun}
\label{sec:Systematics}


The subtle nature of the detected signal requires us to examine its possible instrumental origin. In the following chapter we present an 
analysis of some possible instrumental sources of periodic disturbances as well as the possibility of periodic disturbances from the data processing.

\subsection{Instrumental effects}

The most probable instrumental disturbances that can introduce such a sharp peak in the power spectra of the data are associated with time varying components of the instrument. 
Since the filter wheel is not changing, the only 
moving components of the instrument are the Fabry P\'{e}rot etalons and the times of the exposures. If the Fabry-P\'{e}rot system is not stable and samples at different wavelength positions, this could introduce a 
possible time dependent wavelength shift in the data. On the other hand, if the exposures are not taken with normally distributed intervals, as assumed in our data reduction, this could also introduce periodic signal in our signal.

\subsubsection{Periodicity of the exposures}

IBIS was operated close to the limit of the memory buffer of the imaging cameras. Hence, there is a possibility that periodic irregular 
sampling was introduced from not having the randomly distributed time intervals
between exposures. Theoretically, this
could influence the power of the data (even though that the solar images will not change on the time scales of milliseconds at these resolutions).

We calculated the power spectrum of the deviation from the mean time interval between exposures, which is a measure of the irregularity of the time sampling. 
This quantity was derived by first calculating the average time step between exposures. Knowing this the expected exposure time was calculated and subtracted from the 
actual exposure realization. The result is presented in Fig.~\ref{fig:Shutter_periodicity} for both a series in which the high-frequency peak was detected    
(series H$\alpha$ 14:18 UT) and one in which there was no detection (Ca II 8542 17:04 UT). There is no characteristic excess power around the frequency region of interest (shaded in red).  
However, there are high-frequency peaks and their harmonics, which have their fundamental frequency around the 0.3 Hz (3 second) mark. These are most 
probably due to the small additional time delay that was introduced by the instrument control software between individual spectral scans. 
Therefore, we dismiss the possibility that the unexpected high-frequency signal was introduced through this 
mechanism.


\begin{figure}[h!]
  \includegraphics[width=0.5\textwidth]{Figures/Shutter_periodicity.png}
  \caption{The power spectrum of the deviations from regular temporal sampling. The green spectrum is from Ca II 17:04 UT time series with no detection of the excess high-frequency power;
  the blue power spectrum is from the H$\alpha$ 14:18 UT time series in which 
  we detected the unexpected high-frequency peak. The shaded in red area is the frequency domain where the excess high-frequency power is detected. There is no excess power in that frequency region in the 
  sampling irregularity, thus we reject the hypothesis that sampling aperiodicity introduces the excess power detected in the H$\alpha$ ``fast'' scans. The two power spectra were shifted along the y-axis with a constant
  for better readability of the graph.}
\label{fig:Shutter_periodicity}
\end{figure}


\subsubsection{Stability of the FP etalons}
Another systematic effect that has to be examined is the temporal stability of the Fabry-P\'{e}rot etalon system. 
If the two Fabry-P\'{e}rot etalons showed slight variations in their spectral tuning positions in successive scans this could result in different parts of the spectral line being imaged at the same nominal wavelength position.
The resulting irregular sampling of 
different intensities could result into a different intensity and introduce a systematic offset in the derived spectral properties. We note that a spectral shift of 
$\sim$2.5 m{\AA} (the spectral shift due to the unexpected high-frequency signal) corresponds to a shift in the cavity in the etalons of $\sim$8 {\AA}. It is important to note that
a shift like this would cause a global shift over the whole field 
of view, which is not what we see in Fig.\ref{fig:Where}. Furthermore, a disturbance of this sort will introduce a spurious signal in every scanned spectral line.

In Figure~\ref{fig:FP-stability} we show the power spectrum at every wavelength location (along the rows) averaged over the whole image. There is no excess power present in any of the 
wavelength positions in the frequency region of interest around 40 mHz in both the H$\alpha$ scan and the Ca II scan. Thus, we can conclude that there is no temporal disturbance in the Fabry-P\'{e}rot etalon system   
that produces such irregular sampling.

\begin{figure*}[ht!]
\subfigure[Scan in H$\alpha$ at 17:11 UT.]{
\label{fig:first}
  \includegraphics[width=\columnwidth]{Figures/fft_FP_stability_Ha.png}}
\qquad
\subfigure[Scan in Ca II 8542 at 17:04 UT.]{
\label{fig:second}
\includegraphics[width=\columnwidth]{Figures/fft_FP_stability_Ca.png}}
\caption{Power in the intensity of single wavelength positions from the spectral line profiles used for calculating the velocities and the other spectral line properties described in the text. In the two panes there are 
  fast scans from two data sets, H$\alpha$ at 17:11 UT on the left, that does exhibit the extra power at around 40 mHz and a Ca II IR scan which does not exhibit excess power at higher frequencies. As we see there is no excess 
  power in the left pane around 40 mHz, which confirms the stability of the Fabry-P\'erot etalon filter system.}
  \label{fig:FP-stability}
\end{figure*}

\subsection{Residual seeing effects}

The other time dependent component of the data acquisition and processing is the seeing reduction. We have destretched IBIS to
HMI continuum images as reference in order to remove the bulk and subfield seeing motions   
in our images. However, this does not perfectly correct all the seeing distortions and might leave some seeing effects with periodic nature. Therefore, 
in the next subsections we will investigate the possible sources of 
seeing induced high-frequency power excess in H$\alpha$. 

\subsubsection{Bulk misalignment of the narrowband images}

The first step of the seeing reduction processing involves alignment of the white-light images to a HMI white-light, which acts like an absolute reference. Hence, a  
test of the quality of the bulk alignment of our data between consecutive images. This misalignment was measured through the minimum of the cross correlation between the two images with an IDL 
routine xyoff.pro, distributed in SolarSoft\footnote{\href{https://sohowww.nascom.nasa.gov/solarsoft/}{SolarSoft} is a free IDL library for solar physics.}. We used the same routine for initial calibration,
and the test of the alignment, but it has been robustly tested.
The images are aligned to
precision of less than a pixel, but still cross talk between pixels might be induced from the seeing, which will smear the signal over adjacent pixels. The power spectrum of the jitter was calculated and is presented in 
Figure~\ref{fig:power_xyoff_center}. There is no signal in the power spectrum of bulk jitter in the frequency region of interest, thus we can discard the hypothesis that the unexpected peak is caused by it.   

\begin{figure}[ht!]
  \includegraphics[width=0.5\textwidth]{Figures/Power_xyoff_center_image.png}
  \caption{The figure represents the power spectrum of the residual bulk displacements of several image sequences, represented with the different colors. The shaded region (in red) represents
  the region of the frequency domain where the excess power is detected. We do not see any signatures of misalignment of the images 
  in the frequency region of interest, hence we can discard the hypothesis that bulk misalignment of images
  introduce the periodic signal observed in H$\alpha$.}
 \label{fig:power_xyoff_center}
\end{figure}

\subsubsection{Residual seeing effects from the small box cross correlations}
\label{sec:Seeing_residuals}
The second step in the destretching procedure was the local correlation tracking~\citep{LCT}. Malfunctioning of this step could have resulted in small scale
uncorrelated image motion that would produce
cross talk between pixels. If this effect was causing the high-frequency excess of power in H$\alpha$ then we would see the most power in pixels close to the locations in the images where the most cross talk
can occur. The locations of the image most prone to cross talk are the sharp boundaries between structures, where the gradient of the measured quantity is greatest. 
If seeing induced high-frequency excess was the case, then the pixels with the highest high-frequency power are going to be associated with regions with the 
highest gradient in the measured intensity.

Figure~\ref{fig:grad_vel_vs_power} shows a 2D histogram of the gradient of the COG velocity in a image vs the average power between 38 mHz and 48 mHz for time series 15:18 UT. These frequencies are the region of the high-frequency power excess in FOV2.
The signal was detected in this time sequence
and we averaged over its physical extent.
The figure shows that the pixels with the highest power in this frequency range are not close to large gradients in the velocity -- even the opposite is seen. This supports the idea that 
seeing reduction procedure is not responsible for excess high-frequency power. Furthermore, even the opposite could be inferred from Figure~\ref{fig:grad_vel_vs_power} -- where there are large gradients, close to the 
boundaries of structures, the seeing induced cross talk between pixels reduces the amplitude of the high-frequency signals, as their coherency time is shorter than the longer time perturbations.

\begin{figure*}[ht!]
\subfigure[Gradient of COG Velocity against power around 44 mHz.]{
\label{fig:first_2}
  \includegraphics[width=\columnwidth]{Figures/Fig_gradient_image.png}}
\qquad
\subfigure[Gradient of line core intensity against power around 44 mHz.]{
\label{fig:second_2}
  \includegraphics[width=\columnwidth]{Figures/Fig_gradient_image_2.png}}
\caption{Histograms of the average power at the unexpected peak against the gradient of the image of the quantity that the power was derived from for the time series of 15:18UT. 
  In figure~\ref{fig:first_2} the COG velocity gradient is plotted against the excess power; in Figure~\ref{fig:second_2} the line core intensity gradient is plotted against the average power at the aforementioned 
  frequency region. We see that the regions with the highest excess of high-frequency power are close to regions with
  no gradients, which refutes the hypothesis of seeing induced cross talk origin of the unexpected signal.}
\label{fig:grad_vel_vs_power}
\end{figure*}

\subsubsection{Estimating seeing conditions and their periodicities}

Recurring deterioration of image quality on a certain timescale could produce excess power at a certain frequency as it will smear out the velocity signal in a periodic pattern. This could be 
introduced by the Adaptive Optics system as well, not only the atmospheric turbulence causing the ``seeing''.

We investigated this possibility by calculating a image quality metric for each image in the time sequence and sought out periodicities in the seeing metric.
The image quality metric of our choice is the Median Filtered 
Gradient Similarity (MFGS) \citep{MFGS_info}. MFGS compares the similarity between the absolute value of a gradient of the original image and value of the gradient 
for the same image
convolved with a median filter.
We chose MFGS over the regular RMS of the image, because MFGS has been proven to be more robust for images with 
high contrast regions like sunspots or pores on the border of the image. MFGS returns a value between 0 and 1, increasing for improving image quality. 

We calculated the MFGS with a simple Python script\footnote{\emph{mfgs.py} in the author's repository; see Page 21 for link.} for the white-light and narrowband images which exhibited the excessive high-frequency power  
and for ones which did not. Then we calculated the power spectrum of the time series of the MFGS values for these two series. The results are presented in Figure~\ref{fig:MFGS_PS}. The shaded in red frequency region is the domain where the excess power is observed - nothing significant is seen in that 
region, so we can discard the hypothesis that AO or some other periodic image deterioration causes the spike.  

\begin{figure}[ht!]
  \includegraphics[width=0.5\textwidth]{Figures/Power_MFGS}
  \caption{Power spectrum of the estimated MFGS for every frame in the different time series. We don't see a significant difference between the ones with or without detected excess power. The power spectra are shifted apart by a constant to improve the clarity of the figure.}
  \label{fig:MFGS_PS}
\end{figure}
