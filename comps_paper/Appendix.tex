\appendix

\section{Estimation of the uncertainties in a power spectrum}
\label{app:Errors}
Estimating the noise levels in a measurement is a crucial step toward claiming a scientific discovery. In this appendix we will introduce a method for estimating the uncertainties in a power spectra introduced by 
\cite{Hoyng1976}. We are utilizing the derivated probability distributions associated with power spectra from \cite{Groth1975}. 

All of the presented power spectra throughout this paper have uncertainties based on the described below approach, as otherwise claiming excess power in power spectra would be meaningless. 

The Fourier transform of of a finite equidistant series $x_0,x_1,\ldots,x_{N-1}$ (which we will refer from now as \textbf{$x_k$}) is:
 
\begin{equation}
a_p = h \sum_{k=0}^{N-1} x_k \exp{(ickp)} \quad p,k = 0,1,\ldots,N-1 
\end{equation}

where h=1/N and c=2$\pi$/N. Furthermore, we will use \emph{p} as a frequency range index and \emph{k,l} as a temporal range index. 

The power spectrum of the is given by the absolute magnitude of the Fourier transform coefficients $a_pa_p^* = \left | a_p \right |^2$. Let us assume that the measurement $x_k$ is composed of the 
genuine signal $\xi_k$ plus a noise component $d_k$: $x_k = \xi_k + d_k$ which has a second moment $\sigma^2$; where $Ed_k = 0$ (the mean of the noise is zero) and that the noise is not correlated $E d_k d_l = 0$.

The uncertainty $\epsilon_p$ in the Fourier components $a_p$ can be derived from the standard formula for standard deviation: 

\begin{equation}
  \epsilon^2_p = E \left [ \left | a_p \right | ^2 - E \left |a_p \right |^2 \right ]^2 = E \left [ \left | a_p \right | ^4 \right ] - \left ( E \left [ \left | a_p \right | ^2 \right ] \right )^2    
\end{equation}

Hence, we need to estimate what are $E\left [ \left | a_p \right | ^4 \right ]$ and $E\left [ \left | a_p \right | ^2 \right ]$. For a normally distributed $\xi_k$ the estimate for the $E \left [ \left | a_p \right | ^2 \right ]$ is:

\begin{equation} 
  \begin{split}
  E \left [ \left | a_p \right | ^2 \right ] =  \frac{1}{N^2} \sum_{kl} E \left [ (\xi_k + d_k)(\xi_l+d_l) \exp{(icp(k-l))}  \right ] = \\
    = \frac{1}{N^2} \left ( \sum_{kl} \xi_k\xi_l \exp{(icp(k-l))} + \sum_k \xi_k^2  \right ) = \left | \alpha_p \right |^2 +\frac{\sigma^2}{N}  
  \end{split}
  \label{eqn:ap2-estimate}
\end{equation}
where $\alpha_p \equiv E \left [ a_p \right ]$ is the expectation value for the $a_p$. In the third step of equation~\ref{eqn:ap2-estimate} we have used the properties of the normally distributed noise that its average 
value is zero, and the noise between different measurements is not correlated. 

Estimating $E\left [ \left | a_p \right | ^4 \right ]$ is very similar to the estimation of $E \left [ \left | a_p \right | ^2 \right ]$, but a bit more tedious. 

\begin{equation}
  \begin{split}
  E \left [ \left | a_p \right | ^4 \right ] = \frac{1}{N^4}\sum_{klmn}E \left [ (\xi_k+d_k)(\xi_l+d_l)(\xi_m+d_m)(\xi_n+d_n)\exp{(icp(k-l+m-n))} \right ] = \\
    = \left | \alpha_p \right |^4 + 2 \left (N h^2 \sigma^2 \right ) + 4 \left | \alpha_p \right |^2 N h^2 \sigma^2 
  \end{split}
  \label{eqn:ap4-estimate}
\end{equation}
where we have again utilized the properties of the noise $d_k$ and the fact that we are working with a normally distributed random variables.

Having the results from equations~\ref{eqn:ap4-estimate} and \ref{eqn:ap2-estimate} we can estimate the uncertainty in power spectrum:

\begin{equation}
  \epsilon_p^2 =  E \left [ \left | a_p \right | ^4 \right ] - \left ( E \left [ \left | a_p \right | ^2 \right ] \right )^2 = \frac{2\sigma^2 \left | \alpha_p \right | ^2 }{N} + \frac{\sigma^4}{N^2}
  \label{eqn:epsilonp-estimate}
\end{equation}

$\epsilon_p$ cannot be estimated directly from equation~\ref{eqn:epsilonp-estimate} as we do not know the exact values of the parameters $\left | \alpha_p \right |^2$ and $\sigma^2$. However, we can 
estimate $\left | \alpha_p \right |^2$ from equation~\ref{eqn:ap2-estimate}. If we lightheartedly substitute $\alpha_p$ for $a_p$ then we will overestimate our high-frequency noise three times. Furthermore, the 
standard deviation of the data can readily substituted with the standard deviation measured from the data ($\sigma \mapsto \tilde{\sigma}$). Thus, the estimate for the uncertainty $\tilde{\epsilon_p}^2$ is: 

\begin{equation}
  \tilde{\epsilon_p}^2 =  \frac{2\sigma^2 \left | \alpha_p \right | ^2 }{N} + \frac{\sigma^4}{N^2} \simeq \frac{2 \tilde{\sigma}^2 \left | a_p \right |^2}{N} - \frac{\tilde{\sigma}^4}{N^2}   
\label{eqn:epsilonp_real}
\end{equation}

where we have utilized equation~\ref{eqn:ap2-estimate} for estimating $\alpha_p$. We can rewrite equation~\ref{eqn:epsilonp_real} for the relative uncertainty of the power spectrum as: 

\begin{equation}
  \frac{\tilde{\epsilon_p}}{\left | \ a_p \right | ^2 } \simeq (2x - x^2)^{1/2}; \quad x = \frac{\tilde{\sigma}^2}{N \left | \ a_p \right | ^2}
    \label{eqn:epsilonp_relative}
\end{equation}

Expression~\ref{eqn:epsilonp_relative} allows us to estimate the uncertainty in the power spectrum inexpensively, once we have estimate for $\tilde{\sigma}^2$. We can estimate this quantity as it is equal to the 
average power in the high frequency regime part of the spectrum ($h^2 \tilde{\sigma} = E[\left | a_p \right |]^2 \quad p\gg 1$). 

Furthermore, for large p, as the average power in the large frequency regime is approximately equal to $\tilde{\sigma}^2$, then   
$x\mapsto 1$. Thus, in the high frequency regime the relative uncertainties of the power spectra are of the order of the signal amplitude ($\frac{\tilde{\epsilon_p}}{\left | \ a_p \right | ^2 } \mapsto 1$).  

%\begin{figure}
%
%\label{fig:power_spectra-uncertainty}
%\end{figure}

We implemented a short script in Python to estimate the uncertainties in the presented power spectra throughout the paper. The script estimates the quantity $\tilde{\sigma}^2$ from the flat part of the power 
spectrum encountered in all of the IBIS data sets. The script determines the running average of the power spectrum and decides on the change of its first derivative where the flat part terminates and what part of the spectrum to be used for the estimate of the high-frequency power. This method gives a slight overestimate
of the average high power and $\tilde{\sigma}^2$. Hence, all of the uncertainties cited above are slightly more conservative systematically. 

