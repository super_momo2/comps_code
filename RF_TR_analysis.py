c = 3e5



class Atmos_height():
    ''' Class to hold all the atmospheric info
    numpy.interp(x, xp, fp,...)'''
    def __init__(self,fileName,Nx_new_num):
        atmos_temp   = (np.loadtxt(fileName)).transpose()
        nH_temp      = (np.loadtxt(fileName+'_pops')).transpose()
        self.cs      = 7 
        
        self.Nx_new_num = Nx_new_num
        self.Nx_size = (atmos_temp.shape)[1] 
        self.Nx      = np.linspace(0,(self.Nx_size-1),num=self.Nx_size)
        self.Nx_new  = np.linspace(0,(self.Nx_size-1),num=self.Nx_new_num)
        self.height  = np.linspace(atmos_temp[0,0],
                                   atmos_temp[0,-1],num=self.Nx_new_num)
        
        h            = atmos_temp[0,:]
        
        self.nH         = 6
        self.model_name = fileName

        self.T       = np.interp(self.height,h,atmos_temp[1,:])
        self.Ne      = np.interp(self.height,h,atmos_temp[2,:])
        self.V       = np.interp(self.height,h,atmos_temp[3,:])
        self.Vturb   = np.interp(self.height,h,atmos_temp[4,:])
        self.B       = np.zeros(self.Nx_new_num)
        self.phi     = np.zeros(self.Nx_new_num)
        self.gamma   = np.zeros(self.Nx_new_num)
        self.nH      = np.zeros((self.nH,Nx_new_num))
        for i in range(6):
            self.nH[i,:]   = np.interp(self.height,h,nH_temp[i,:])
        print('Atmosphere loaded')
    
    def plot_v_field(self):
        plt.plot(self.height,self.V,'r.--')
        plt.ylabel('Vertical Velocity, [km/s]')
        plt.xlabel('Height, [km]')
        plt.grid(alpha=0.5)
        plt.show()
    
    def plot_T(self):
        plt.plot(self.height,self.T,'r.--')
        plt.ylabel('Temperature, [K]')
        plt.xlabel('Height, [km]')
        plt.grid(alpha=0.5)
        plt.yscale('log')
        plt.show()
        
    def include_V_wave(self,nu,phi,Amplitude):
        '''For now with constant amplitude; to implement changing amplitude to conserve flux'''
        self.V = Amplitude*np.sin(2*np.pi*nu*self.height/self.cs+phi)
    
    def include_V_pulse(self,index,Amplitude):
        self.V[:] = 0 
        self.V[index] = Amplitude
    
    def write_TF_test(self,nu_min,nu_max,nu_num,phi_num,Amplitude):
        ''' Write an RH output atmosphere for Ricardos's version 
        of RH
        
        Format has to be the following: 
        Height,Temperature,Ne,V,Vturb,field,gamma,phi,nh(1),nh(2),nh(3),nh(4),nh(5),np
        '''
    
        atmos   = np.zeros((phi_num,nu_num,14,self.Nx_new_num))
        
        nu      = np.logspace(np.log10(nu_min),np.log10(nu_max),num=nu_num)
        
        for jj in range(nu_num):
            for ii in range(phi_num):
                atmos[ii,jj,0,:]    = self.height
                atmos[ii,jj,1,:]    = self.T
                atmos[ii,jj,2,:]    = self.Ne
                atmos[ii,jj,4,:]    = self.Vturb
                atmos[ii,jj,5,:]    = self.B
                atmos[ii,jj,6,:]    = self.phi
                atmos[ii,jj,7,:]    = self.gamma
                atmos[ii,jj,8:13,:] = self.nH[0,:]
                self.include_V_wave(nu[jj],ii*2*np.pi/phi_num,Amplitude)
                atmos[ii,jj,3,:]    = self.V
                
        atmos = np.swapaxes(atmos,1,2)
        atmos = np.swapaxes(atmos,0,3)    
        hdu = fits.PrimaryHDU(atmos)
        hdu.writeto('/Users/molnarad/Desktop/rh/FAL11_nu_'+str(nu_num)+'_phi_num_'+str(phi_num)+'.fits',overwrite=True)
        
        return 'Successfully written out atmosphere'
        
    
    def write_response_test(self,Amplitude):
        ''' Write an RH output atmosphere for Ricardos's version 
        of RH
        
        Format has to be the following: 
        Height,Temperature,Ne,V,Vturb,field,gamma,phi,nh(1),nh(2),nh(3),nh(4),nh(5),np
        '''
    
        atmos   = np.zeros((1,self.Nx_new_num,14,self.Nx_new_num))
        
        for jj in range(self.Nx_new_num):
            atmos[0,jj,0,:]    = self.height
            atmos[0,jj,1,:]    = self.T
            atmos[0,jj,2,:]    = self.Ne
            atmos[0,jj,4,:]    = self.Vturb
            atmos[0,jj,5,:]    = self.B
            atmos[0,jj,6,:]    = self.phi
            atmos[0,jj,7,:]    = self.gamma
            atmos[0,jj,8:13,:] = self.nH[0,:]
            self.include_V_pulse(jj, Amplitude)
            atmos[0,jj,3,:]    = self.V
                
        atmos = np.swapaxes(atmos,1,2)
        atmos = np.swapaxes(atmos,0,3)    
        hdu = fits.PrimaryHDU(atmos)
        hdu.writeto('/Users/molnarad/Desktop/rh/FAL11_RF_vel_'+str(Amplitude)+'_kms.fits',overwrite=True)
        
        return 'Successfully written out atmosphere'

class RF_vel_calibration(): 
    
    def __init__(self,Filename,Amplitude):
        data            = fits.open(Filename)
        data            = data[0].data
        self.wavelength = data[0,:,0,0] 
        self.Amplitude  = Amplitude
        self.spectra    = data[1,:,:,:]
        self.vel_lc     = data[1,0,:,:]
        
        self.Ha_waves_u,self.unique_i = np.unique(self.wavelength[self.Ha_waves[0]:(self.Ha_waves[1])],
                                    return_index=True)
        self.Ha_I       = (self.spectra[self.Ha_waves[0]:(self.Ha_waves[1]),:,:])[self.unique_i,:,:]
        self.N_waves_Ha = len(self.unique_i)
    
    def Instrument_degrade(self,Instrument):
        #First Convolve spectrum with SPSF 
        
        #Second Average the profile in time
        exp_time = Instrument.sampling_rate
        self.Ha_degraded  = np.zeros((self.N_waves_Ha,self.N_nu,self.N_phi))
        
        for ii in range(self.N_nu):
            
            nu1               = self.nu[ii]
            N_average         = int(self.N_phi*nu1*exp_time)

            if N_average == 0:
                N_average = 1 
                
            weights           = np.ones(N_average) / N_average 
            print('For Frequency %f the degradation is over %d frames'%(nu1,N_average))
            for ll in range(self.N_waves_Ha):
                self.Ha_degraded[ll,ii,:] = np.convolve(self.Ha_I[ll,ii,:],
                                                        weights,mode='same')
    
    def calculate_Doppler(self):
        
    
    def calculate_RF(self):
        
        
class Spectral_Line():
    '''Class for the output from the RH code'''
    
    def __init__(self,Filename,nu_min,nu_max,nu_num,phi_num,Amplitude,wave):
        #(2,238,10,20) ,,nu,phi
        data            = fits.open(Filename)
        data            = data[0].data
        self.wavelength = data[0,:,0,0] 
        self.nu         = np.logspace(np.log10(nu_min),np.log10(nu_max),num=nu_num)
        self.phi        = np.linspace(0,2*np.pi,num=phi_num)
        self.Amplitude  = Amplitude
        self.spectra    = data[1,:,:,:]
        self.vel_lc     = data[1,0,:,:]
        self.width      = data[1,0,:,:]
        self.vel_gr     = data[1,0,:,:]
        self.N_nu       = (data.shape)[2]
        self.N_phi      = (data.shape)[3]
        self.Ha_waves   = [193,-5]
        self.wave       = wave
        
        
        
        self.Ha_waves_u,self.unique_i = np.unique(self.wavelength[self.Ha_waves[0]:(self.Ha_waves[1])],
                                    return_index=True)
        self.Ha_I       = (self.spectra[self.Ha_waves[0]:(self.Ha_waves[1]),:,:])[self.unique_i,:,:]
        self.N_waves_Ha = len(self.unique_i)
        
    def Instrument_degrade(self,Instrument):
        #First Convolve spectrum with SPSF 
        
        #Second Average the profile in time
        exp_time = Instrument.sampling_rate
        self.Ha_degraded  = np.zeros((self.N_waves_Ha,self.N_nu,self.N_phi))
        
        for ii in range(self.N_nu):
            
            nu1               = self.nu[ii]
            N_average         = int(self.N_phi*nu1*exp_time)

            if N_average == 0:
                N_average = 1 
                
            weights           = np.ones(N_average) / N_average 
            print('For Frequency %f the degradation is over %d frames'%(nu1,N_average))
            for ll in range(self.N_waves_Ha):
                self.Ha_degraded[ll,ii,:] = np.convolve(self.Ha_I[ll,ii,:],
                                                        weights,mode='same')
            #print('Difference is:',(self.Ha_degraded[:,ii,10]-self.Ha_I[:,ii,10]))
    def find_vel_RF(self,Plot_B):
        ''' Calculate the line center with non-degraded 
        spectral profiles
        '''
        for xx in range(self.Nx_new_num):
            I     = interpolate.CubicSpline(self.Ha_waves_u,self.Ha_I[:,xx,yy])
            self.vel_lc[xx,yy] = (optimize.fmin(I,self.wave,disp=False) 
                                  - self.wave) / self.wave * c
            
            if (Plot_B == True) :
                plt.plot(self.Ha_waves_u,self.Ha_I[:,xx,yy],'b.')
                plt.plot(np.tile(self.vel_lc[xx,yy],len(self.Ha_waves_u)),self.Ha_I[:,xx,yy],'r--')
                plt.xlim(self.wave-.1,self.wave+.1)
                plt.xlabel('Wavelength, [nm]')
                plt.show()
            
            self.vel_lc[xx,:] = self.vel_lc[xx,:] - np.mean(self.vel_lc[xx,:])
        return 0
    
    def find_lc_min(self,Plot_B):
        ''' Calculate the line center with non-degraded 
        spectral profiles
        '''
        for xx in range(self.N_nu):
            for yy in range(self.N_phi):
                I     = interpolate.CubicSpline(self.Ha_waves_u,self.Ha_I[:,xx,yy])
                self.vel_lc[xx,yy] = (optimize.fmin(I,self.wave,disp=False) 
                                      - self.wave) / self.wave * c
                
                if (Plot_B == True) :
                    plt.plot(self.Ha_waves_u,self.Ha_I[:,xx,yy],'b.')
                    plt.plot(np.tile(self.vel_lc[xx,yy],len(self.Ha_waves_u)),self.Ha_I[:,xx,yy],'r--')
                    plt.xlim(656.2,656.4)
                    plt.xlabel('Wavelength, [nm]')
                    plt.show()
            
            self.vel_lc[xx,:] = self.vel_lc[xx,:] - np.mean(self.vel_lc[xx,:])
        return 0
    
    def find_lc_min1(self,Plot_B):
        ''' Calculate the line center with the instrument
        degraded spectral profiles 
        '''
        for xx in range(self.N_nu):
            for yy in range(self.N_phi):
                I     = interpolate.CubicSpline(self.Ha_waves_u,self.Ha_degraded[:,xx,yy])
                self.vel_lc[xx,yy] = (optimize.fmin(I,self.wave,disp=False) 
                                      - self.wave) / self.wave * c
                
                if (Plot_B == True) :
                    plt.plot(self.Ha_waves_u,self.Ha_I[:,xx,yy],'b.')
                    plt.plot(np.tile(self.vel_lc[xx,yy],len(self.unique_i)),self.Ha_degraded[:,xx,yy],'r--')
                    plt.xlim(656.2,656.4)
                    plt.xlabel('Wavelength, [nm]')
                    plt.show()
            
            self.vel_lc[xx,:] = self.vel_lc[xx,:] - np.mean(self.vel_lc[xx,:])   
    
    def calc_T(self):
        self.T = np.zeros(self.N_nu)
        for i in range(self.N_nu):
            self.T[i] = np.amax(np.abs(self.vel_lc[i,:]))/self.Amplitude
    
    def RF(self):
        '''Calculate the velocity Response function
        from the spectral line
        '''
        self.RF = np.zeros(self.Nx_new_num)
        
        
        
    def calc_Acoustic_flux(self,P_0,slope_index):
        ''' Calc the acoustic flux, given the P_0, 
        and the slope of the power law'''
        
        T = sp.interpolate.interp1d(self.nu,self.T)
        self.rho = 5e-8
        cs  = 7e3
        
        def flux_est(freq):
            return P_0*((freq/self.nu[0])**(slope_index))*cs*self.rho/(T(freq)**2)
        
        flux = integrate.quad(flux_est,self.nu[0],self.nu[-1])
        self.flux = flux
        
class Instrument_Profile():    
    def __init__(self,sampling_rate,SPSF):
        self.sampling_rate = sampling_rate
        self.SPSF          = SPSF

def write_rh_output(Atmos_Object,nu_range,Amplitude,N_trials):
    ''' Write an RH output atmosphere for Ricardos's version 
        of RH
        
        Format has to be the following: 
        Height,Temperature,Ne,V,Vturb,field,gamma,phi,nh(1),nh(2),nh(3),nh(4),nh(5),np
        '''
    
    atmos   = np.zeros((N_trials,1,14,Atmos_Object.Nx_new_num))
    
    for i in range(N_trials):
        atmos[i,0,0,:]    = Atmos_Object.height
        atmos[i,0,1,:]    = Atmos_Object.T
        atmos[i,0,2,:]    = Atmos_Object.Ne
        atmos[i,0,4,:]    = Atmos_Object.Vturb
        atmos[i,0,5,:]    = Atmos_Object.B
        atmos[i,0,6,:]    = Atmos_Object.phi
        atmos[i,0,7,:]    = Atmos_Object.gamma
        atmos[i,0,8:13,:] = Atmos_Object.nH[0,:]
        Atmos_Object.include_V_perturb(nu,np.random.random_sample()*2*np.pi,Amplitude)
        atmos[i,0,3,:]    = Atmos_Object.V
        
    atmos = np.swapaxes(atmos,1,2)
    atmos = np.swapaxes(atmos,0,3)    
    hdu = fits.PrimaryHDU(atmos)
    hdu.writeto('/Users/molnarad/Desktop/rh/FAL11_nu_'+str(nu)+'_'+str(N_trials)+'.fits',overwrite=True)
    
    return 'Successfully written out atmosphere'