#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 14:54:06 2020

Calculate the slope histograms of some parameters.

@author: molnarad
"""
import numpy as np
import scipy as sp
import matplotlib as mp
import matplotlib.pyplot as pl
from scipy import io
from scipy import signal

#TURBO CHARGE YOUR PYTHON -- Make it parallel 
import multiprocessing 
from joblib import Parallel, delayed 

import matplotlib.colors as mcolors

from numpy.random import multivariate_normal

from skimage.transform import downscale_local_mean

from PYBIS import count_zeroes_in_array

def load_ALMA_Band3(time_block):
    '''
    Parameters
    ----------
    time_block : int
        Which timeblock of the ALMA dataset to be used for the analysis (2-5);
    Returns
    -------
    freqs: float array
        Frequency grid 
    ALMA6_fft_cut: float array
        PSD of the pixels chosenc
    '''
    
    file_name  = ('ALMA_band3_t' + str(time_block) 
                  +'_csclean_feathered.sav')
    sav_file   = io.readsav(data_dir + 'ALMA/' + file_name)
    ALMA3_data = np.zeros((360, 360, 299))
    
    for ii in range(299):
        ALMA3_data[:, :, ii] = np.rot90(sav_file['maps'][ii][0].transpose())
    
    cut = 100
    dx1 = ALMA3_data.shape[0]//2 - cut
    dx2 = ALMA3_data.shape[0]//2 + cut
    ddx = dx2 - dx1

    ALMA3_fft = [signal.periodogram(el-np.mean(el))[1]
                           for line in ALMA3_data[dx1:dx2, dx1:dx2, :]
                                                   for el in line] 
                          
    ALMA3_fft_cut = np.reshape(ALMA3_fft, (ddx**2, 
                                           ALMA3_data.shape[2]//2 + 1))
    freqs    = signal.periodogram(ALMA3_data[0, 0, :], fs = 1/2.0)[0]
    freqs    = freqs * 1e3
    
    return np.array(freqs), np.array(ALMA3_fft_cut)

def load_ALMA_Band6(time_block):
    '''
    

    Parameters
    ----------

    time_block : int
        Which timeblock of the ALMA dataset to be used for the analysis (2-5);

    Returns
    -------
    freqs: float array
        Frequency grid 
    ALMA6_fft_cut: float array
        PSD of the pixels chosenc
        

    '''
    
    file_name  = ('aligned_ALMA_20170423_band6_dc_t' + str(time_block) 
                  +'_feathered_snr3.sav')
    sav_file   = io.readsav(data_dir + 'ALMA/' + file_name)
    map_ex     = sav_file['maps'][0][0].transpose()
    ALMA6_data = np.zeros((map_ex.shape[0],
                           map_ex.shape[1],
                           238))
    
    for ii in range(238):
        ALMA6_data[:, :, ii] = np.rot90(
            sav_file['maps'][ii][0].transpose())
   
    cut = 90
    
    dx1 = ALMA6_data.shape[0]//2 - cut
    dx2 = ALMA6_data.shape[0]//2 + cut
    
    ddx = dx2 - dx1
    
    ALMA6_fft = [signal.periodogram(el-np.mean(el))[1]
                           for line in ALMA6_data[dx1:dx2, dx1:dx2, :]
                                                   for el in line] 
                          
    ALMA6_fft_cut = np.reshape(ALMA6_fft, (ddx**2, 
                                           ALMA6_data.shape[2]//2 + 1))
    
    freqs    = signal.periodogram(ALMA6_data[0, 0, :], fs = 1/2.0)[0]
    freqs    = freqs * 1e3
    
    return np.array(freqs), np.array(ALMA6_fft_cut)

def construct_name(spec_property, time):
    '''
    Generate names for the save files to be retrieved from the 
    .sav library.

    Parameters
    ----------
    spec_property : string
        Spectral property to be investigated.
        
    time : string
        Time of observations

    Returns
    -------
    name : string
        DESCRIPTION.

    '''
    if int(time) < 150000:
        target = '.23Apr2017.target1.all.'
    else:
        target = '.23Apr2017.target2.all.'

    name = ("fft.nb."+ spec_property + target
            + filter_dic[time] + '.ser_'
            + str(time) + ".sav")
    return name

def load_data(spec_obs, time):
    '''
    Load the data from the .sav files
    Parameters
    ----------
    time: int
        time of the observation to be plot
    spec_obs : string
        one of spec_obs
 
    Returns
    -------
    fft_freq: float array
        frequencies of the PSDs
    fft_data: 2D float array 
        PSDs 
    '''
    data_file     = construct_name(spec_obs, time)
    pix_lim       = 0
    dx            = 1
    
    if filter_dic[time] == "8542":
        corr_coeff = ca_v_coeff
    elif filter_dic[time] == "6563":
        corr_coeff = ha_v_coeff
    
    fft_dict = sp.io.readsav(data_dir + 'IBIS/'
                             + data_file, python_dict=True)
    fft_data = downscale_local_mean(fft_dict['fft_data'], (1, 1, 1))
    fft_freq = fft_dict['freq1'] * 1e3 #make it in mHz
    
    fft_data = fft_data.reshape(fft_data.shape[0],
                                fft_data.shape[1]*fft_data.shape[2])
    fft_data = np.swapaxes(fft_data, 0, 1)
    print('Loaded ' + spec_obs + f' {filter_dic[time]}')
    
    return np.array(fft_freq), np.array(fft_data)
    


pl.style.use('science')

filter_dic = {141344:"8542", 141807:"6563", 151321:"8542", 151857:"6563", 
              153954:"8542", 154624:"6563", 170444:"8542", 171115:"6563", 
              181227:"6563", 2:"", 4:""}

data_dir        = ('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
                   + 'Comps_2/comps_data/')

labels = ['plage', 'network', 'fibrils', 'internetwork', 'penumbra']
masks_file = 'Masks_FOV2.npz'

ALMA3_time_block = 2
ALMA6_time_block = 2


ca_v_coeff = 1233.5
ha_v_coeff = 2089
# fig.subplots_adjust(hspace=.05, wspace= .22)

data_int   = ['vel.cog', 'vel.lc', 'int.lc', 'equiv.width', 'ALMA Band 3', 
              'ALMA Band 6', 'vel.cog', 'vel.lc', 'int.lc', ]
time_stamp = [154624, 154624 ,154624, 154624, 2, 2, 153954, 
              153954, 153954]

for el in range(7, 8):
    if el < 4 or el > 5:
        freqs, data = load_data(data_int[el], time_stamp[el])
    
    elif el == 4: # Plot ALMA Band 3 
        freqs, data = load_ALMA_Band3(time_stamp[el])
    
    else: # Plot ALMA Band 6 
        freqs, data = load_ALMA_Band6(time_stamp[el])
        
    data_len = data.shape[0]
    print(f"Data length is {data.shape}")
    
    if el < 4:
        freq_0 = 3
        freq_1 = 14

    if el == 4:
        freq_0 = 6
        freq_1 = 23
    if el >= 5:
        freq_0 = 4
        freq_1 = 15
    
    print(f"Frequencies range for plot #{el+1} from {freqs[freq_0]} "
          + f"to {freqs[freq_1]}")

    # y = slopes[0] * x + slopes[1]

    slopes = Parallel(n_jobs=4)(delayed(np.polyfit)(np.log10(freqs[freq_0:freq_1]), 
                                                    np.log10(data[ii, freq_0:freq_1]),
                                                    1) for ii
                                in range(data_len))
    slopes = np.array(slopes)
    slopes = slopes[:, 0]

    print(f'Slopes shape is {slopes.shape}')
    slopes_dim_square = int(np.sqrt(slopes.shape))    
    slopes = np.reshape(slopes, (slopes_dim_square, slopes_dim_square))


i = 0
dx = 30

slopes_all = np.zeros((5, slopes.shape[0]-2*dx, slopes.shape[1]-2*dx))

pl.figure(dpi=125, figsize=(5, 3.5))

colors = np.flip(['#332288', '#88CCEE', '#117733', '#DDCC77', '#CC6677'])

i = 0
for mask_el in labels:
    
    with np.load(masks_file) as file:
        mask = (np.array(file[mask_el]))

    slopes_all[i, :, :] = (mask * slopes)[dx:-dx, dx:-dx]
    slopes_non_zero = np.ma.masked_equal(slopes_all[i, :, :], 0.0).compressed()
    
    n, bins = np.histogram(slopes_non_zero, 
                                    bins=70, range=[-6, 2], density=True)
    
    mean_distr = np.percentile(slopes_non_zero, 50)    
    perc10 = np.percentile(slopes_non_zero, 10)
    perc90 = np.percentile(slopes_non_zero, 90)
    
    pl.plot(bins[1:], n, label=mask_el, color=colors[i])
    f = np.interp(mean_distr, bins[1:], n)
    pl.plot([mean_distr, mean_distr], [0, f], '--', color=colors[i])
    
    X = np.ma.masked_equal(slopes_all[i, :, :], 1230.0)
    

    
    print(f"The slope of the {mask_el} region is {mean_distr:.3f}"
          + f" + {perc90 - mean_distr: .3f} - {perc10 - mean_distr:.3f}")
    i = i + 1

#pl.ylim(0, 0.00075)

pl.ylabel('Occurence')
pl.xlabel('PSD Power Law slope')
pl.xlim(-6, 1.5)
#pl.yscale("log")
#pl.ylim(0., .4)
pl.grid(alpha=0.3)
pl.legend(prop={'size': 8})
pl.tight_layout()
pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
           +'2021a_Molnar_et_al_High_freq/git/git/Figures/Slopes_histogram.png')
pl.show()




