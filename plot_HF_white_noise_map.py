#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 15:09:21 2020

Plot the HF whitenoise level at the high frequencies 

@author: molnarad
"""

import numpy as np
from PYBIS import construct_name, load_fft_data_simple
from scipy.optimize import curve_fit
import matplotlib.pyplot as pl
import time

from tqdm import tqdm 
from skimage.transform import downscale_local_mean



# data loading here

data_dir = ('/Users/molnarad/CU_Boulder/Work/'
                +'Chromospheric_business/Comps_2/comps_data/')
spec_property = 'vel.lc'
time_stamp    = 170444
dsample_f = 6

freq, fft_data = load_fft_data_simple(data_dir, spec_property, time_stamp)

time0 = time.time()
#some fitting here 


fft_data_downsampled = downscale_local_mean(fft_data, (1, dsample_f, 
                                                       dsample_f))

failed_fits = 0
freq_ind    = 6
dfreq_ind   = 8
freq_ratio  = freq[freq_ind]/freq[freq_ind+dfreq_ind]
lg_freq_ratio = np.log10(freq_ratio)
            
            
time1 = time.time()
print(f"Time for fitting is {(time1-time0):.3e}")
print(f"Failed fits during the fitting routine: {failed_fits}")


extent_FOV2 = [-127.98, -31.98, 210.99,306.99]

pl.figure(figsize=(5, 5), dpi=250)
im = pl.imshow(np.log10(np.mean(fft_data_downsampled[40:, :, :], axis=0)),
               vmin=-5, vmax=0, cmap='gray', origin='lower',
               extent=extent_FOV2)
pl.xlabel("Solar X [arcsec]")
pl.ylabel("Solar Y [arcsec]")
cbar1 = pl.colorbar(im, fraction=0.0465, pad=0.03)
cbar1.set_label("Log$_{10}$[White noise level]")
pl.xlim(extent_FOV2[0]+3, extent_FOV2[1]-3)
pl.ylim(extent_FOV2[2]+3, extent_FOV2[3]-3)
pl.tight_layout()
pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
           +'2020_Molnar_et_al_High_freq/git/git/Figures/noise_floor_FOV2.png')
pl.show()
