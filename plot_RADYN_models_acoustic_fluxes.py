#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 11:34:38 2020

Calc RADYN TF from models

@author: momo
"""


import numpy as np
import matplotlib.pyplot as plt
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog
from scipy import signal

RADYN_models = ["radyn_ulmschneider_250_low_dt.cdf",
                #"radyn_ulmschneider_625_low_dt.cdf",
                "radyn_ulmschneider_1200_low_dt.cdf",
                "radyn_ulmschneider_3000_low_dt.cdf",
                #"radyn_ulmschneider_4500_low_dt.cdf",
                #"radyn_ulmschneider_7500.cdf",
                "radyn_ulmschneider_19000.cdf"]

data_dir = "/mnt/Data/Comps_2/comps_data/RADYN_runs/"

labels = ["model_250", 
          #"model_625", 
          "model_1200",
          "model_3000", 
          #"model_4500", 
          #"model_7500", 
          "model_19000"]

sp_line_index = 20 # 20 - Ca II 8542 Å; 2 - Ha 6563 Å

start_index = 800 
end_index   = -10
dt_sim      = 3
count_every = 1

height_v = 1.15e3 


if sp_line_index == 2:
    vel_coefficient = 3e5/6563
if sp_line_index == 20:
    vel_coefficient = 3e5/8542

figure, ax1 = plt.subplots(dpi=250)


for el in range(len(RADYN_models)):
    rad = RADYN_atmos(data_dir + RADYN_models[el])
    
    rad.calc_Flux_equidistant(start_index+250, 250)
    ax1.plot(rad.z_eq/1e5, rad.Flux_eq, 'o--', label=labels[el])
    
y_axis_lim_upper = 1e3
y_axis_lim_lower = 1e-2    
x = np.linspace(1.05e3, 1.15e3, num=1000)
y1 = x*0 + y_axis_lim_lower
y2 = x*0 + y_axis_lim_upper
ax1.fill_between(x, y1, y2, color="red", alpha=0.15)

ax1.grid()
ax1.set_yscale("log")
ax1.set_ylim(y_axis_lim_lower, y_axis_lim_upper)
ax1.set_xlim(1000, 1400)
ax1.set_title(f"Acoustic flux in RADYN runs")
ax1.set_xlabel("Height [km]")
ax1.set_ylabel("Acoustic Flux [W / m$^2$]")
ax1.legend(fontsize=8)
ax1.text(1060, .7, "Ca II 854.2 nm\nFormation region", color="red")

plt.savefig("RADYN_runs_Acoustic_flux.png", transparent=True)
plt.show()
