#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  8 14:14:46 2020

Align the Ca II images and Acoustic flux estimate with the ALMA 
images
@author: molnarad
"""


# Load the ALMA images

ALMA_pixel = 0.33 #arcsec

# Load the Ca II images

IBIS_pixel = 0.096 #arcsec

# Shift and rescale 

# Find the shift according to the cross correlation 

# Compare the fluxes