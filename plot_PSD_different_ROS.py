#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 19:50:44 2020

Compute the average PSD for areas of the Sun with similar properties (like
plage, network etc)

Use the mask from split_solar_surface.py

@author: molnarad
"""

import numpy as np
import scipy as sp
import matplotlib.pyplot as pl
from scipy import stats as stats

from PYBIS import construct_name, load_fft_data_simple

fontsize_legend = 7
pl.style.use('science')


def count_zeroes_in_array(array):
    counter = 0
    for el in array.ravel():
        if el == False:
            counter += 1

    return counter

labels = ['plage', 'network', 'fibrils', 'internetwork', 'penumbra']
#labels = ['penumbra']
#labels = ['network']
fft_mean_data = np.zeros((5, 61))

masks_file = 'Masks_FOV2.npz'
i = 0

data_dir = ('/Users/molnarad/CU_Boulder/Work/'
                +'Chromospheric_business/Comps_2/comps_data/')
spec_property = 'vel.lc'
time_stamp    = 153954
freq, fft_data = load_fft_data_simple(data_dir, spec_property, time_stamp)

noise_estimate_file = "log_mean_HF_noise_1.npz"
with np.load(noise_estimate_file) as a:
    noise_estimate = a["log_mean_all"]

fft_data = np.log10(fft_data)
dx = 50
ddfreq = freq[1] - freq[0]

for mask_el in labels:

    with np.load(masks_file) as file:
        mask = np.array(file[mask_el])


    for el in range(fft_data.shape[0]):
        fft_mean_data_temp =(mask  * fft_data[el, :, :])[dx:-dx, dx:-dx]
        X = np.ma.masked_equal(fft_mean_data_temp, 0.0)
        X_compressed = X.compressed()
        fft_mean_data[i, el] = np.mean(X_compressed)
        
    fft_data_masked = fft_data * mask
    fft_data_summed = (np.sum(10**fft_data_masked[3:19, :, :], axis=0)
                       + 10**fft_data_masked[2, :, :] * 0.5) * ddfreq
    X = np.ma.masked_greater(fft_data_summed, stats.mode(fft_data_summed).mode[0]*.98)
    X_compressed = X.compressed()
    median = np.percentile(X_compressed, 50)
    percent_10 = np.percentile(X_compressed, 10)
    percent_90 = np.percentile(X_compressed, 90)
    print(f"For {mask_el} the median is {median:.3f}+{percent_90-median:.3f}"
          +f"-{median-percent_10:.3f} and the 10th percentile is"
          +f" {percent_10:.3f} and the 90th percentile is {percent_90:.3f}.")
    i = i + 1

pl.figure(dpi=250)
# range_colors = ['b', 'c', 'r', 'g', 'm']
range_colors = np.flip(['#4477AA', '#88CCEE', '#117733',
                        '#DDCC77', '#CC6677'])

with np.load("FOV1_average_PSD.npz") as aa:
    freq_FOV1 = aa["freq_FOV1"] * 1e3 #make it in mHz
    PSD_FOV1  = aa["FOV1_mean_PSD"] * 1239 / freq_FOV1[1] # make it in metric
                                                          # (km/s) units
                                                          
pl.plot(freq_FOV1[1:-2], PSD_FOV1[1:-2], ".--", markersize=6,
        # label="FOV 1 PSD",
        color="black", alpha=.2)
pl.plot(1e5,1e5, "k.--", markersize=6,
        label="FOV 1", alpha=.45)

for el in range(len(labels)):
    pl.plot(freq[1:-1], 10**fft_mean_data[el, 1:-1], '.--',
            color=range_colors[el],
            markersize=8, label = labels[el])


pl.legend(fontsize=fontsize_legend)
#pl.title('Average PSD in different regions of the solar surface')
pl.xlabel("Frequency [mHz]")
pl.ylabel("Power [(km/s)$^{2}$/mHz]")
pl.tight_layout()
pl.yscale('log')
pl.xscale('log')
pl.xlim(3,5e1)
pl.ylim(1e-5, 5e-1)
pl.grid()
#pl.title(f'{spec_property} at time {time_stamp}')
pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
           + '2021a_Molnar_et_al_High_freq/git/git/Figures/Average_PSD_ROS.png'
           , transparent=True)
pl.show()

np.savez("PSD_ROS.npz", fft_mean_data = fft_mean_data,
         noise_estimate = noise_estimate, freq=freq)
