"""
Created on Thu Oct 17 08:39:31 2019

Calculate the Acoustic Flux in the H I 6563 data
from the FAL models derived fluxes

@author: molnarad
"""


import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
# from power_uncertain import power_uncertain

from RHlib import Spectral_Line, Instrument_Profile


def downscale(data, dims):
    '''
    Downscale the dimensions of the data

    Parameters
    ----------
    data : array
    dims : number of times to shrink the dimensions of the data

    Returns
    -------
    data_new : array


    '''
    data_shape = data.shape
    new_shape  = [int(data_shape[i] / dims[i]) for i in range(len(dims))]
    print(new_shape)
    data_new   = np.zeros(new_shape)

    for ii in range(new_shape[0]):
        for jj in range(new_shape[1]):
            data_new[ii, jj] = np.mean(data[ii*dims[0]:ii*dims[1]+dims[0],
                                            jj*dims[1]:jj*dims[1]+dims[1]])
    return data_new


# import the data
data_dir = '/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_data/'
fft_file_ca = data_dir+'IBIS/fft.nb.vel.lc.23Apr2017.target2.all.6563.ser_154624.sav'

a = sp.io.readsav(fft_file_ca, verbose=False)
fft_data = np.flip(a.fft_data, axis=1)
frequency = a.freq1
fft_power = a.fft_powermap

# ca_coeff = 1e3*1233.5/(frequency[1]-frequency[0])
ha_coeff = 2089
IBIS = Instrument_Profile(3.5, [0])
Ha   = Spectral_Line('/Users/molnarad/Desktop/rh/',
                     'FALC11_nu_20_phi_num_50_A_10.fits',
                     'spect_falc11_nu_20_phi_num_50_a_10.fits', 656.3)

Ha.Instrument_degrade(IBIS)
Ha.find_lc_min1(False)
# for i in range(Ha.N_nu):
#    print("For frequency %f the amx velocity is %f"
#         % Ha.nu[i], np.amax(np.abs(Ha.vel_lc[i, :]) / 1))

lim1 = 3
lim2 = 22
print(f'The frequency limits in this integration are: {frequency[lim1]:.5f}'
      + f' to {frequency[lim2]:.5f}')

Ha.calc_G()
print('G is: ', Ha.G)

fft_data_noise = np.mean(fft_data[-10:-1, :, :], axis=0)

fft_data_downsampled = np.zeros((frequency.size, 250, 250))

for ii in range(frequency[lim1:lim2].size):
    fft_data_downsampled[ii, :, :] = downscale(fft_data[ii, :, :], [4, 4])
    
for ii in range(fft_data_downsampled.shape[1]):
    for jj in range(fft_data_downsampled.shape[2]):
        noise_est = np.mean(fft_data_downsampled[-25:-1, ii, jj])
        fft_data_downsampled[:, ii, jj] = (fft_data_downsampled[:, ii, jj] 
                                          - noise_est)
        
Ha_flux = Ha.calc_Acoustic_flux_map(frequency[lim1:lim2],
                               fft_data_downsampled[lim1:lim2, :, :] * ha_coeff)

center_x = -79.98
center_y = 258.99
scale = 0.096
size = 1e3
fontsize_axis = 13
fontsize_ticks = 9

x_left_lim = center_x - size * scale / 2
y_left_lim = center_y - size * scale / 2
x_right_lim = center_x + size * scale / 2
y_right_lim = center_y + size * scale / 2


plt.figure(figsize=(10, 6), dpi=250)

aa = plt.imshow(np.log10(Ha_flux), vmin=2, vmax=4,
                extent=[x_left_lim, x_right_lim, y_right_lim, y_left_lim],
                cmap='plasma')
cbar = plt.colorbar(aa)
cbar.ax.get_yaxis().labelpad = 15
plt.xlabel('Solar X [arcsec]')
plt.ylabel('Solar Y [arcsec]')
cbar.ax.set_ylabel('Acoustic power, W/m$^2$', rotation=270)
plt.title('Ha I 6562.8 $\\AA$ acoustic power')
plt.show()

plt.figure(figsize=(5, 3), dpi=125)
plt.plot(frequency[lim1:lim2], Ha.G_c, 'b.--')
plt.xlabel('Frequency [Hz]')
plt.yscale('log')
plt.xscale('log') 
plt.ylabel('T coefficient, [W/$m^2.s^2/km^2$.Hz]')
plt.title("Transmission coefficient Halpha vel.lc")
plt.show()
