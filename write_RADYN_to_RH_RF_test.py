
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 21:26:00 2020

Write RADYN simulations into RH format 
for calculation of the RF of velocity

@author: molnarad
"""


import numpy as np


import matplotlib.pyplot as plt
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog
from scipy import signal


simulation_name = ["radyn_ulmschneider_47000", "radyn_ulmschneider_19000",
                   "radyn_ulmschneider_7500", "radyn_ulmschneider_3000",
                   "radyn_ulmschneider_1200", "radyn_ulmschneider_625",
                   "radyn_ulmschneider_250", "radyn_ulmschneider_100"]

data_dir = "/mnt/Data/Comps_2/comps_data/RADYN_runs/"

for el in simulation_name:
    rad = RADYN_atmos(data_dir + el + '.cdf')
    rad.write_RF_test(1000)