#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 14:56:46 2019

@author: molnarad
"""

from RHlib import Spectral_Line

Ha = Spectral_Line('/Users/molnarad/Desktop/rh/results/rh_spect_RF_Ha.fits',
                   1e-3, 2e-3, 1, 100, 1, 656.3)
Ha.spectra