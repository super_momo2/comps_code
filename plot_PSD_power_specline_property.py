#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 11:25:48 2020

Plot correlation of PSD power vs property -- does higher values of 
H-alpha width/Ca II 8542 Å intensity correlate directly with PSD power  

@author: molnarad
"""


import numpy as np
import scipy as sp
from scipy.ndimage import gaussian_filter
import matplotlib.pyplot as pl
from astropy.io import fits
from test_color_maps import diverging_colors
from matplotlib.colors import ListedColormap
from matplotlib.cm import register_cmap
import matplotlib.colors as mcolors

from PYBIS import construct_name, load_fft_data_simple

data_dir = ('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
            + 'Comps_2/comps_data/')

ab = sp.io.readsav(data_dir + 'IBIS/FOV2_widths_Ha.sav')
ha_width_data = np.array(ab['widths'])

spec_property = 'vel.lc'
time_stamp    = 170444
freq, fft_data = load_fft_data_simple(data_dir, spec_property, time_stamp)

lim_1 = 7
lim_2 = 20
xlim  = 50 # border to be cut around the edges to remove edge garbage


fft_power_tot = np.log10((np.sum(fft_data[lim_1:lim_2, :, :], axis=0)))

with fits.open(data_dir + 'IBIS/Fig_1_Ca_int.fits') as temp:
    ca_int_data = np.array(temp[0].data)

pl.figure(dpi=250, figsize=(4, 2.8))
gamma_1 = .3
pl.hist2d(ha_width_data[xlim:-xlim, xlim:-xlim].ravel(),
          fft_power_tot[xlim:-xlim, xlim:-xlim].ravel(), bins=300,
          norm=mcolors.PowerNorm(gamma_1),
          range=[[.8, 1.3], [0.002,.1]])
#pl.ylim(-3.5, 0)
pl.xlabel("H$\\alpha$ line width [Å]")
pl.ylabel(f"Log10[Acoustic power {freq[lim_1]:.1f} - {freq[lim_2]:.1f} mHz]")
pl.show()

pl.figure(dpi=250, figsize=(4, 2.8))
gamma_2 = .5
pl.hist2d((ca_int_data[xlim:-xlim, xlim:-xlim].ravel()),
          fft_power_tot[xlim:-xlim, xlim:-xlim].ravel(), bins=300,
          norm=mcolors.PowerNorm(gamma_2), cmap='gray',
          range=[[300, 1210], [-3, 0]])# np.mean(fft_power_tot < 10)]])
pl.xlabel("Ca II 8542 Å core intensity")
pl.ylabel(f"Acoustic power {freq[lim_1]:.1f} - {freq[lim_2]:.1f} mHz")
pl.show()

# pl.figure(dpi=250, figsize=(4, 2.8))
# gamma_3 = .7
# pl.hist2d(ca_int_data[xlim:-xlim, xlim:-xlim].ravel(),
#         ha_width_data[xlim:-xlim, xlim:-xlim].ravel(), bins=300,
#          norm=mcolors.PowerNorm(gamma_3))
#pl.ylabel("H$\\alpha$ line width [Å]")
#pl.xlabel("Ca II 8542 Å core intensity")
#pl.show()