#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 02:51:48 2020

Plot the acoustic flux in all the models + and where is their TR

@author: momo
"""

import numpy as np
import matplotlib.pyplot as plt
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog
from scipy import signal

RADYN_models = ["radyn_ulmschneider_250.cdf",
                "radyn_ulmschneider_1200.cdf",
                "radyn_ulmschneider_3000.cdf",
                "radyn_ulmschneider_7500.cdf",
                "radyn_ulmschneider_19000.cdf",
                "radyn_ulmschneider_47000.cdf"]

data_dir = "/mnt/Data/Comps_2/comps_data/RADYN_runs/"

labels = ["model_250", "model_1200",
          "model_3000", "model_7500", "model_19000", 
          "model_47000"]

sp_line_index = 20 # 20 - Ca II 8542 Å; 2 - Ha 6563 Å

start_index = 800 
end_index   = -10
dt_sim      = 3
count_every = 1

height_v = 1.15e3 


if sp_line_index == 2:
    vel_coefficient = 3e5/6563
if sp_line_index == 20:
    vel_coefficient = 3e5/8542

figure, ax1 = plt.subplots(dpi=250)


for el in range(len(RADYN_models)):
    rad = RADYN_atmos(data_dir + RADYN_models[el])
    
    rad.calc_Flux_PSD(start_index+250, 250, height_v)
    ax1.loglog(rad.Flux_eq_PSD_freq*1e3, rad.Flux_eq_PSD/1e3, '.--', label=labels[el])

x = np.linspace(1e-1, 5.18, num=1000)
y0 = 1e-20
y1 = 1e30


ax1.grid()
ax1.set_yscale("log")
ax1.set_ylim(1e-13, 1e0)
ax1.fill_between(x, y0*1e3, y1*1e3, facecolor='red', alpha=0.05)
ax1.set_xlim(1, 120)
ax1.set_ylabel(f"Acoustic flux [W$^2$/m$^2$/Hz]")
ax1.set_xlabel("Frequency [mHz]")
ax1.set_title("Acoustic Flux PSD")
ax1.legend(fontsize=8)

plt.show()

     