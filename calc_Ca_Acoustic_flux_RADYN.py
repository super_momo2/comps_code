#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 09:39:15 2020

Calculate the Acoustic flux based on the RADYN models

@author: molnarad
"""

import numpy as np
import scipy as sp
from scipy import io
import matplotlib.pyplot as plt
# from power_uncertain import power_uncertain

from skimage.transform import downscale_local_mean
from RHlib import Spectral_Line, Instrument_Profile


def downscale(data, dims):
    """
    Downscale the dimensions of the data.

    Parameters
    ----------
    data : array
    dims : number of times to shrink the dimensions of the data

    Returns
    -------
    data_new : array


    """
    data_shape = data.shape
    new_shape  = [int(data_shape[i] / dims[i]) for i in range(len(dims))]
    print(new_shape)
    data_new   = np.zeros(new_shape)

    for ii in range(new_shape[0]):
        for jj in range(new_shape[1]):
            data_new[ii, jj] = np.mean(data[ii*dims[0]:ii*dims[1]+dims[0],
                                            jj*dims[1]:jj*dims[1]+dims[1]])
    return data_new

plt.style.use('science')


T_coeff_file = "T.npz"
hdu = np.load(T_coeff_file)
T_freq  = hdu["arr_1"] * 1e-3
T_coeff = hdu["arr_0"] * 0 + [1, .4, .25, .3, .15, .15,
                              .2, .2, .3, .3, .3, .3, .3]

T = sp.interpolate.interp1d(T_freq, T_coeff, kind="slinear")

freq_ind = [2, 5, 11, 20] #FOV 2
# freq_ind = [1, 6, 7, 8] #FOV 1

# import the data

data_dir = '/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_data/'
fft_file_ca = data_dir+'IBIS/fft.nb.vel.lc.23Apr2017.target2.all.8542.ser_153954.sav'

figs_dir = ("/Users/molnarad/CU_Boulder/Work/Manuscripts/"
            + "2021a_Molnar_et_al_High_freq/git/git/Figures/")

a = sp.io.readsav(fft_file_ca, verbose=False)
fft_data = np.flip(a.fft_data, axis=1)
frequency = a.freq1
fft_power = a.fft_powermap

density = 5.5e-9
cs      = 7e3

coeff = density * cs / T(frequency[freq_ind[0]:freq_ind[-1]])**2

ca_coeff = 1233.5e6
# ha_coeff = 2089

#Ca.Instrument_degrade(IBIS)
#Ca.find_lc_min1(False)
# for i in range(Ha.N_nu):
#    print("For frequency %f the amx velocity is %f"
#          % Ha.nu[i], np.amax(np.abs(Ha.vel_lc[i, :]) / 1))

lim1 = freq_ind[0]
lim2 = freq_ind[-1]
print(f"The frequency limits in this integration are:"
      + f"{1e3*frequency[lim1]:.5f} mHz"
      + f" to {frequency[lim2]*1e3:.5f} mHz")

#Ca.calc_G()
#print('G is: ', Ca.G)

downscale_factor = 4



fft_data_downsampled = downscale_local_mean(fft_data,
                                            (1, downscale_factor,
                                             downscale_factor))
fft_data_noise = np.median(fft_data_downsampled[-25:, :, :],
                           axis=0)

fft_data_downsampled1 = (fft_data_downsampled - fft_data_noise)

# fft_data_noise = np.median(fft_data[-20:-1, :, :], axis=0)
#
# fft_data_downsampled = np.zeros((frequency.size, int(1000/downscale_factor),
#                                  int(1000/downscale_factor)))
# fft_data_downsampled1 = np.zeros((frequency.size, int(1000/downscale_factor),
#                                   int(1000/downscale_factor)))
#
#   for ii in range(frequency.size):
#       fft_data_downsampled[ii,:,:] = downscale(fft_data[ii,:,:],
#                                                [downscale_factor, downscale_factor])
#   for ii in range(fft_data_downsampled.shape[1]):
#       for jj in range(fft_data_downsampled.shape[2]):
#           noise_est = np.mean(fft_data_downsampled[-25:, ii, jj])
#           fft_data_downsampled1[:, ii, jj] = (fft_data_downsampled[:, ii, jj]
#                                             - noise_est)

df = frequency[2] - frequency[1]


Ca_flux_total = np.zeros((1000//downscale_factor, 1000//downscale_factor))
Ca_flux_sub = np.zeros((len(freq_ind)-1,
                       1000//downscale_factor, 1000//downscale_factor))
print(f"Total flux calculated over {frequency[freq_ind[0]]} to" +
      f" {frequency[freq_ind[-1]]}")

for el in range(len(freq_ind)-1):
    print(f"Sub flux {el} calculated over {frequency[freq_ind[el]]} to" +
          f" {frequency[freq_ind[el+1]]}")


for ii in range(int(1000/downscale_factor)):
    for jj in range(int(1000/downscale_factor)):
        Ca_flux_total[ii, jj] = (np.sum(fft_data_downsampled1[freq_ind[0]:freq_ind[-1],
                                                           ii, jj] * coeff)
                                 * ca_coeff)
        for el in range(len(freq_ind)-1):
            Ca_flux_sub[el, ii, jj] = (np.sum(fft_data_downsampled1[freq_ind[el]:freq_ind[el+1],
                                                                       ii, jj]
                                                  * coeff[freq_ind[el]-freq_ind[0]:freq_ind[el+1]-freq_ind[0]])
                                 * ca_coeff)
#Ca_flux = Ca.calc_Acoustic_flux_map(frequency[lim1:lim2],
#                               fft_data_downsampled1[lim1:lim2, :, :] * ca_coeff)

center_x = -79.98
center_y = 258.99
scale = 0.096
size = 1e3
fontsize_axis = 18
fontsize_ticks = 9

x_left_lim = center_x - size * scale / 2
y_lower_lim = center_y - size * scale / 2
x_right_lim = center_x + size * scale / 2
y_upper_lim = center_y + size * scale / 2

plt.figure(figsize=(7, 6), dpi=125)

aa = plt.imshow(np.log10(Ca_flux_total), vmin=2 - np.log10(5),
                vmax= 4 - np.log10(5),
                extent=[x_left_lim, x_right_lim, y_lower_lim, y_upper_lim],
                cmap='RdBu')

cbar = plt.colorbar(aa)
cbar.ax.get_yaxis().labelpad = 15
plt.xlabel('Solar X, [arcsec]', fontsize=fontsize_axis)
plt.ylabel('Solar Y, [arcsec]', fontsize=fontsize_axis)
plt.xlim(x_left_lim, x_right_lim-5.0)
plt.ylim(y_lower_lim+5, y_upper_lim)
cbar.ax.set_ylabel('Log$_{10}$(Acoustic power [W/m$^2$])', rotation=270)
#plt.title('Ca II 8542 $\\AA$ acoustic power (RADYN)')
plt.savefig(figs_dir+"Ca_8542_acoustic_flux.png", transparent=True)
plt.show()

from mpl_toolkits.axes_grid1 import make_axes_locatable

fig, ax = plt.subplots(2, 3, dpi=250, figsize=(10, 6))

for el in range(3):
    im = ax[0, el].imshow(Ca_flux_sub[el, :, :]/Ca_flux_total, vmin=0, vmax=1,
                      extent=[x_left_lim, x_right_lim, y_lower_lim, y_upper_lim],
                      cmap='brg')

    #ax[el].set_xlabel('Solar X, [arcsec]')
    ax[0, el].set_xlim(x_left_lim, x_right_lim-5.0)
    ax[0, el].set_ylim(y_lower_lim+5, y_upper_lim)

ax[0, 0].set_title("5-10 mHz")
ax[0, 1].set_title("10-30 mHz")
ax[0, 2].set_title("30-50 mHz")

divider = make_axes_locatable(ax[0, 2])
cax = divider.append_axes("right", size="5%", pad=0.1)
cbar = fig.colorbar(im, cax=cax, label="Relative flux contribution")
ax[0, 0].set_ylabel('Solar Y, [arcsec]')


for el in range(3):
    im = ax[1, el].imshow(np.log10(Ca_flux_sub[el, :, :]), vmin=1.25, vmax=3.25,
                      extent=[x_left_lim, x_right_lim, y_lower_lim, y_upper_lim],
                      cmap='bwr')


    ax[1, el].set_xlabel('Solar X, [arcsec]')
    ax[1, el].set_xlim(x_left_lim, x_right_lim-5.0)
    ax[1, el].set_ylim(y_lower_lim+5, y_upper_lim)

divider = make_axes_locatable(ax[1, 2])
cax = divider.append_axes("right", size="5%", pad=0.1)
cbar = fig.colorbar(im, cax=cax, label="Log$_{10}$ (Acoustic flux [W/m$^2$])")
ax[0, 0].set_ylabel('Solar Y, [arcsec]')


plt.savefig(figs_dir+"Acoustic_flux_contributions.png", transparent=True,
            bbox_inches='tight')


labels     = ['plage', 'network', 'fibrils', 'internetwork', 'penumbra']
colors = np.flip(['#332288', '#88CCEE', '#117733', '#DDCC77', '#CC6677'])
masks_file = 'Masks_FOV2.npz'
flux_ros   = np.zeros(len(labels))

plt.figure(dpi=250)
for el in range(len(labels)):
    mask_el = labels[el]

    with np.load(masks_file) as file:
        mask = np.array(file[mask_el])
    mask = downscale(mask, [4, 4])
    mask_factor = np.amax(mask)
    flux_region = ((mask * np.flipud(Ca_flux_total))[5:-5, 5:-5])
    #plt.imshow(np.log10(1 + mask * np.flipud(Ca_flux_total)), origin=0)
    #plt.show()
    n, bins = np.histogram((flux_region).flatten(),
                           range=(1, 1e4), bins=4000, normed=True)
    plt.plot(bins[1:], n, label=mask_el, color=colors[el], linewidth=.5,
             alpha=0.5)
    X = np.ma.masked_equal(flux_region,0.0)

    mean_distr = np.percentile(X.compressed(), 50)
    perc_10 = np.percentile(X.compressed(), 10)
    perc_90 = np.percentile(X.compressed(), 90)

    print(f"The mean flux for the {mask_el} region is {mean_distr:.2f} W/m^2 "
          +"and the 10th/90th"
          +f" percentiles are: {mean_distr-perc_10:.2f} W/m^2 and"
          +f" {perc_90-mean_distr:.2f} W/m^2.")
    f = np.interp(mean_distr, bins[1:], n)
    plt.plot([mean_distr, mean_distr], [0, f], '--', color=colors[el],
             linewidth=1)

    flux_ros[el] = np.sum(mask * np.flipud(Ca_flux_total)) / np.sum(mask) / mask_factor
plt.legend(fontsize=7, loc=1)
plt.yscale("log")
plt.xscale("log")
plt.xlabel("Acoustic Flux [W/m$^2$]")
plt.ylabel("Normalized PDF")
plt.grid(alpha=0.5)
plt.xlim(3e0, 2e3)
plt.savefig(figs_dir+"Flux_ROS_distribution.png", transparent=True,
            bbox_inches='tight')
plt.show()

np.savez("Ca_flux.npz", Ca_flux=Ca_flux_total)
