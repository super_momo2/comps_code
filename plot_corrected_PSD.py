#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 22:55:58 2020

Plot corrected PSD's  of different regions of the sun

@author: molnarad
"""

import numpy as np
import matplotlib.pyplot as pl

with np.load("PSD_ROS.npz") as a:
    fft_mean_data = a["fft_mean_data"]
    freq_data     = a["freq"]

with np.load("Ca_8542_vel_lc_G_vel_var.npz") as b:
    freq_Gc = b["nu"]
    Ca_G_c  = b["Ca_G"]

#plt.title("Transmission coefficient Ca II 8542 Å vel.lc")


G_c_data = np.interp(freq_data/1000, freq_Gc, Ca_G_c[5], left=0, right=0)


pl.figure(dpi=250, figsize=(7, 4.5))
range_colors = ['b', 'c', 'r', 'g', 'm']
labels = ['plage', 'network', 'fibrils', 'internetwork', 'penumbra']

noise_levels = np.zeros(5)

for el in range(5):
    noise_levels[el] = np.mean(fft_mean_data[el, -20:])

for el in range(len(labels)):
    pl.plot(freq_data[1:-1], 10**(fft_mean_data[el, 1:-1]
                                  -noise_levels[el])*G_c_data[1:-1],
            range_colors[el]+'.--',
            markersize=8, label = labels[el])
    #pl.plot(freq_data[1:], 10**noise_estimate[el, :len(freq_data[1:])],
    #        range_colors[el]+'--',
    #        linewidth=1)
#pl.legend(fontsize=fontsize_legend)
#pl.title('Average PSD in different regions of the solar surface')


pl.xlabel("Frequency [mHz]")
pl.ylabel("Power [km$^2$.s$^{-2}$/mHz]")
pl.tight_layout()
pl.yscale('log')
pl.xscale('log')
pl.title("Removing the white noise")
pl.grid()
pl.legend()
#pl.title(f'{spec_property} at time {time_stamp}')
#pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
#           + '2020b_Molnar_et_al_High_freq/git/git/Figures/Average_PSD_ROS.png')
pl.show()