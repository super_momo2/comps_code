#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  8 16:58:52 2020

Plot average sp profiles + samplings for fast scans

@author: molnarad
"""

import numpy as np
import matplotlib.pyplot as pl

data_dir = "/Users/molnarad/CU_Boulder/Work/Chromospheric_business/Comps_2/comps_data/IBIS/"
fig_dir = "/Users/molnarad/CU_Boulder/Work/Manuscripts/2021a_Molnar_et_al_High_freq/git/git/Figures/"
ca_file = "Ca_8542_average_spec_profile.csv"
ha_file = "Ha_6563_average_spec_profile.csv"

fontsize_legend = 14
fontsize_labels = 14

ca_data = np.loadtxt(data_dir + ca_file, delimiter=",").transpose()
ha_data = np.loadtxt(data_dir + ha_file, delimiter=",").transpose()

fig, ax = pl.subplots(2, 1, dpi=125, figsize=(7, 8.75))
ax[0].plot(ha_data[0], ha_data[1]/np.amax(ha_data[1]),
           'ro--', label="H I 6563 $\AA$")
ax[1].plot(ca_data[0]+.41, ca_data[1]/np.amax(ca_data[1]),
           'ro--', label="Ca II 8542 $\AA$")
ax[0].legend(fontsize=fontsize_legend, loc=3)
ax[1].legend(fontsize=fontsize_legend)
ax[0].grid()
ax[1].grid()
ax[0].set_ylabel("Relative intensity", fontsize=fontsize_labels)
ax[1].set_ylabel("Relative intensity", fontsize=fontsize_labels)
ax[1].set_xlabel("Wavelength offset [$\AA$]", fontsize=fontsize_labels)
pl.tight_layout()
pl.savefig(fig_dir + "Fast_scan_profiles.png")
pl.show()
