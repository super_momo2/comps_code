import numpy as np
import matplotlib.pyplot as pl

labels = ["100", "250", "625", "1200", "3000", "7500",
          "19000", "47000"]

pl.style.use('science')

for el in range(len(labels)):

    fig, ax = pl.subplots(2, 1, dpi=250)
    file_name = "radyn_ulmschneider_"+labels[el]+".cdf_tau1_props.csv"
    data = np.loadtxt(file_name, delimiter=",").T
    ax[0].plot(data[0, :], data[1, :]/1e5, label="Ha")
    ax[0].plot(data[0, :], data[3, :]/1e5, label="Ca")
    ax[0].legend()
    ax[0].set_ylabel("Height [km]")
    ax[0].grid(alpha=.5)

    ax[1].plot(data[0, :], data[2, :], label="Ha")
    ax[1].plot(data[0, :], data[4, :], label="Ca")
    ax[1].legend()
    ax[1].grid(alpha=.5)
    ax[1].set_ylabel("Density [g.cm$^{-3}$]")
    ax[1].set_xlabel("Time [s]")
    ax[1].set_yscale("Log")
    fig.suptitle("Model " + labels[el])
    pl.savefig(file_name+".png", transparent=True)
    pl.show()
