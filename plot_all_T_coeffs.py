import numpy as np
import matplotlib.pyplot as pl

data = ["Tradyn_ulmschneider_250_low_dt.npz",
       "Tradyn_ulmschneider_625_low_dt.npz",
       "Tradyn_ulmschneider_1200_low_dt.npz", 
       "Tradyn_ulmschneider_3000_low_dt.npz"]

labels=["250", "625", "1200", "3000"]

for el in range(len(data)):
    hdu = np.load(data[el]) 
    freq = hdu["arr_1"]
    T    = hdu["arr_0"]

    pl.plot(freq*1e3, T, label=labels[el], marker='o')

pl.xlabel("Frequency [mHz]")
pl.ylabel("T coefficient")
pl.legend(title="Model")
pl.grid()
pl.xlim(5, 60)
pl.ylim(0, 1.2)
pl.show()
