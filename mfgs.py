#Program snippet for calculating the MFGS of a certain frame
#following Deng et al. 2015, Sol.Phys., 290:1479-1489

import matplotlib as mp
import matplotlib.pyplot as plt
import numpy as np
from numpy import random as rand
import scipy as sp
import scipy.signal as signal
import matplotlib.image as mpimg

def mfgs(image,kernel_size1):

    #median filter original image with <kernel_size>
    image_median = signal.medfilt(image,kernel_size=kernel_size1)


    #calculate gradient of the original and filtered image

    grad10 = np.gradient(image,axis=0)
    grad11 = np.gradient(image,axis=1)


    grad20 = np.gradient(image_median,axis=0)
    grad21 = np.gradient(image_median,axis=1)

    G_p = np.sqrt(np.sum(np.multiply(grad10,grad10)+np.multiply(grad11,grad11)))
    G_r = np.sqrt(np.sum(np.multiply(grad20,grad20)+np.multiply(grad21,grad21)))
    #calculate the similarity of the two
    mfgs_measure = 2*G_p*G_r/(G_p*G_p+G_r*G_r)
    return mfgs_measure

if __name__=="__main__":
    image = mpimg.imread('FFT__image_1536277055.764559.png')
    a = rand.rand((image.shape)[0],(image.shape)[1]);
    for i in range(0,10):
        print(mfgs(image[:,:,1]+100*i*a,7))
        plt.imshow(image[:,:,1]+100*i*a)
        plt.show()
