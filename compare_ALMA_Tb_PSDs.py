#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 16:46:16 2020

@author: momo
"""


import numpy as np
import astropy.io.fits as fits
import matplotlib.pyplot as pl
from scipy import signal

data_dir = "/mnt/Data/RH/rh_parallel/rh2/results/"

RADYN_models = ["radyn_ulmschneider_100.fits",
                "radyn_ulmschneider_250.fits",
                "radyn_ulmschneider_625.fits",
            
                "radyn_ulmschneider_3000.fits",
                "radyn_ulmschneider_7500.fits",
                "radyn_ulmschneider_19000.fits",
                "radyn_ulmschneider_47000.fits"]

labels = ["model_100", "model_250", "model_625", 
          "model_3000", "model_7500", "model_19000", 
          "model_47000"]

dt_sim = 3.0 #seconds per step in the simulation
RJ_coeff_Band3 = 3.26e17
RJ_coeff_Band6 = 5.66e16
acoustic_flux  = [1e-2, 1e-1, 6e-1, 1e0, 7e0, 7e1, 8e2, 4e3]

num_files = len(RADYN_models)
data_temp = fits.open(data_dir + RADYN_models[0] )
d = data_temp[0].data

fig, ax = pl.subplots(2, 1, dpi=200, figsize=(4, 6))

for el2 in range(6):
    el = 6-el2
    data_holder   = fits.open(data_dir + RADYN_models[el])
    alma_intensity = data_holder[0].data
    alma_Tb_Band3 = alma_intensity[1, -5, 0, 300:] * RJ_coeff_Band3
    alma_Tb_Band6 = alma_intensity[1, -15, 0 , 300:] * RJ_coeff_Band6

    freq_Band3, alma_Tb_Pxx_Band3 = signal.periodogram((alma_Tb_Band3
                                                        -np.mean(alma_Tb_Band3)),
                                                       fs=1/dt_sim)
    freq_Band6, alma_Tb_Pxx_Band6 = signal.periodogram((alma_Tb_Band6
                                                        - np.mean(alma_Tb_Band6)),
                                                       fs=1/dt_sim)

    num_tsteps = len(alma_Tb_Band3)
    time = np.linspace(0, dt_sim*(num_tsteps-1), num=num_tsteps)
    #ax[0, 0].plot(time, alma_Tb_Band3, label = labels[el])
    #ax[0, 1].plot(time, alma_Tb_Band6, label = labels[el])

    ax[0].plot(freq_Band3[2:-2], alma_Tb_Pxx_Band3[2:-2], 
                  '.--', label=labels[el], alpha=0.6)
    ax[1].plot(freq_Band6[2:-2],
                  alma_Tb_Pxx_Band6[2:-2], 
                  '.--', label=labels[el], alpha=0.6)

    #ax[2, 0].plot(np.sum(alma_Tb_Pxx_Band3[15:120])*(freq_Band3[2]-freq_Band3[1]),
    #              acoustic_flux[el], 'o',
    #              label=labels[el])
   # ax[2, 1].plot(np.sum(alma_Tb_Pxx_Band6[15:120])*(freq_Band6[2]-freq_Band6[1]),
   #               acoustic_flux[el], 'o',
    #              label=labels[el])

ax[0].set_title("Band 3 T$_b$ PSD")
ax[1].set_title("Band 6 T$_b$ PSD")

for el in ax:

    el.set_ylabel("PSD[Tb] [K^2/Hz]") 
    el.set_xlabel("Frequency [mHz]")
    
    el.set_yscale("log")
    el.set_ylim(1e4, 1e9)
    el.set_xlim(5e-3, 70e-3)
    el.set_xscale("log")


    el.grid()

pl.tight_layout()
pl.show()
