import numpy as np
import astropy.io.fits as fits
import matplotlib.pyplot as pl
from scipy import signal
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog

data_dir = "/mnt/Data/RH/rh_parallel/rh2/results/"

RADYN_models = ["radyn_ulmschneider_1200.fits",
                "radyn_ulmschneider_3000.fits",
                "radyn_ulmschneider_7500.fits",
                "radyn_ulmschneider_19000.fits",]
                #"radyn_ulmschneider_47000.fits"]

labels = [ "model_1200", 
          "model_3000", "model_7500", "model_19000"]

simulation_name = ["radyn_ulmschneider_1200", 
                   "radyn_ulmschneider_3000",
                   "radyn_ulmschneider_7500",
                   "radyn_ulmschneider_19000"]
                 #  "radyn_ulmschneider_47000"]

sim_dir = "/mnt/Data/Comps_2/comps_data/RADYN_runs/"

start_index = 500
end_index   = -150
dt_sim      = 3
count_every = 1

height_Band3 = 1.25e3 
height_Band6 = 1.0e3

dt_sim = 3.0 #seconds per step in the simulation
RJ_coeff_Band3 = 3.26e17
RJ_coeff_Band6 = 5.66e16
acoustic_flux  = [1e-2, 1e-1, 6e-1, 1e0, 7e0, 7e1, 8e2, 4e3]

num_files = len(RADYN_models)
data_temp = fits.open(data_dir + RADYN_models[0] )
d = data_temp[0].data

# fig, ax = pl.subplots(3, 2, dpi=200, figsize=(8, 6))
fig, ax = pl.subplots(4, 2, dpi=200, figsize=(8, 14))

max_freq = 60e-3
dfreq   = 7e-3    
num_freqs = int(max_freq / dfreq) + 1

T_ALMA3 = np.zeros((5, num_freqs))
T_ALMA6 = np.zeros((5, num_freqs))

for el2 in range(1, 5):
    el = len(RADYN_models)-el2
    print(el)
    data_holder   = fits.open(data_dir + RADYN_models[el])
    alma_intensity = data_holder[0].data
    alma_Tb_Band3 = alma_intensity[1, -5, 0,
                                   start_index:end_index] * RJ_coeff_Band3
    alma_Tb_Band6 = alma_intensity[1, -15, 0, 
                                   start_index:end_index] * RJ_coeff_Band6

    freq_Band3, alma_Tb_Pxx_Band3 = signal.periodogram((alma_Tb_Band3
                                                        -np.mean(alma_Tb_Band3)),
                                                       fs=1/dt_sim)
    freq_Band6, alma_Tb_Pxx_Band6 = signal.periodogram((alma_Tb_Band6
                                                        - np.mean(alma_Tb_Band6)),
                                                       fs=1/dt_sim)

    num_tsteps = len(alma_Tb_Band3)
    time = np.linspace(0, dt_sim*(num_tsteps-1), num=num_tsteps)

    rad = RADYN_atmos(sim_dir + simulation_name[el] + '.cdf')

    rad.calc_Flux_PSD(start_index+250, 300, height_Band3, dt_sim)  
    ax[1, 0].plot(rad.Flux_eq_PSD_freq[1:]*1e3, rad.Flux_eq_PSD[1:],
                  label=labels[el])
 
    dfreq_array = rad.Flux_eq_PSD_freq[1]
    ddfreq = int(dfreq / dfreq_array) 
    Flux_PSD_mean_Band3 = [np.mean(rad.Flux_eq_PSD[i*ddfreq:(i*ddfreq 
                                                             + ddfreq)]) 
                           for i in range(num_freqs)]
  
    rad.calc_Flux_PSD(start_index+250, 300, height_Band6, dt_sim)  
    ax[1, 1].plot(rad.Flux_eq_PSD_freq[1:]*1e3, rad.Flux_eq_PSD[1:],
                  label=labels[el])
    Flux_PSD_mean_Band6 = [np.mean(rad.Flux_eq_PSD[i*ddfreq:(i*ddfreq 
                                                             + ddfreq)]) 
                           for i in range(num_freqs)]
    
    alma_Tb_Pxx3_interp = np.interp(rad.Flux_eq_PSD_freq, 
                                         freq_Band3, 
                                         alma_Tb_Pxx_Band3, right=0)
 
    alma_Tb_Pxx6_interp = np.interp(rad.Flux_eq_PSD_freq, 
                                         freq_Band6, 
                                         alma_Tb_Pxx_Band6, right=0)
       
    dfreq_array = rad.Flux_eq_PSD_freq[1]
    ddfreq = int(dfreq / dfreq_array) 

    ALMA_3_PSD_mean = [np.mean(alma_Tb_Pxx3_interp[i*ddfreq:(i*ddfreq + ddfreq)]) 
                       for i in range(num_freqs)]

    ALMA_6_PSD_mean = [np.mean(alma_Tb_Pxx6_interp[i*ddfreq:(i*ddfreq + ddfreq)]) 
                       for i in range(num_freqs)]
    
    freq_range = np.linspace(0, (num_freqs-1)*dfreq, 
                             num=num_freqs) + dfreq/2
    
    T_ALMA3[el, :] = np.sqrt(np.array(Flux_PSD_mean_Band3) 
                             / np.array(ALMA_3_PSD_mean))

    T_ALMA6[el, :] = np.sqrt(np.array(Flux_PSD_mean_Band6) 
                             / np.array(ALMA_6_PSD_mean))
 
    ax[3, 0].plot(freq_range*1e3, T_ALMA3[el, :],
                  label=labels[el], marker='o')
    ax[3, 1].plot(freq_range*1e3, T_ALMA6[el, :], 
                  label=labels[el], marker='o')
    ax[0, 0].plot(time, alma_Tb_Band3, label = labels[el])
    ax[0, 1].plot(time, alma_Tb_Band6, label = labels[el])

    ax[2, 0].plot(freq_range*1e3, ALMA_3_PSD_mean, 
                  '.--', label=labels[el])
    ax[2, 1].plot(freq_range*1e3, ALMA_6_PSD_mean, 
                  '.--', label=labels[el])

    # ax[1, 0].plot(np.sum(alma_Tb_Pxx_Band3[15:120])*(freq_Band3[2]-freq_Band3[1]),
    #               acoustic_flux[el], 'o',
    #               label=labels[el])
    # ax[1, 1].plot(np.sum(alma_Tb_Pxx_Band6[15:120])*(freq_Band6[2]-freq_Band6[1]),
    #               acoustic_flux[el], 'o',
    #               label=labels[el])

# ax[0, 0].set_ylim(4.5e3)
# ax[0, 1].set_ylim(3.e3)
# ax[0, 0].set_title("Band 3 Brightness T")
# ax[0, 1].set_title("Band 6 Brightness T")
# ax[0, 0].legend(fontsize=8)
# ax[0, 1].legend(fontsize=8)
# 
# 

ax[0, 0].set_ylabel("Brightness\nTemperature [K]")

ax[0, 0].set_xlabel("Time [s]")
ax[0, 1].set_xlabel("Time [s]")

ax[2, 0].set_ylabel("PSD[Tb]\n[K$^2$/Hz]") 
# 

for el in ax[1, :]:
     # el.set_xlabel("Frequency [mHz]")
     el.set_yscale("log")
     

for el1 in ax[2, :]:

    el1.set_yscale("log")
    

np.savez("T_ALMA_coeffs.npz", T_ALMA3 = T_ALMA3, T_ALMA6=T_ALMA6, 
         freq_range=freq_range)  
# for el2 in ax[2, :]:
#     el2.set_ylabel("Acoustic flux [W/m$^2$]")
#     el2.set_xlabel("High freq ALMA power [10-40 mHz]")
#     el2.set_yscale("log")
#     el2.set_xscale("log")
#     el2.grid()

  
ax[1, 0].set_yscale("log")
ax[1, 0].set_xlim(5, 60)
# ax[2, 0].set_xlabel("Frequency [mHz]")
# ax[2, 1].set_xlabel("Frequency [mHz]")
ax[0, 0].legend(fontsize=8)

ax[2, 0].set_ylabel(f"PSD[Velocity]\n[cm$^2$/s$^2$]")

ax[3, 0].set_ylabel("T coefficient\n[W/m$^2$ / K$^2$]")
ax[3, 0].set_xlabel("Frequency [mHz]")
ax[3, 1].set_xlabel("Frequency [mHz]")

ax[0, 0].set_title("ALMA Band 3")
ax[0, 1].set_title("ALMA Band 6")


pl.tight_layout()
pl.savefig("ALMA_comparison.png", transparent=True)
pl.show()
