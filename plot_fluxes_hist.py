#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 01:18:57 2020

Plot the acoustic fluxes of the

@author: molnarad
"""

import numpy as np
import matplotlib.pyplot as pl

def create_circular_mask(h, w, center=None, radius=None):

    if center is None: # use the middle of the image
        center = (int(w/2), int(h/2))
    if radius is None: # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

    mask = dist_from_center <= radius
    return mask


def plot_histogram(data, label, range1, nbins, color_el=0, alpha=.75):
    colors = np.flip(['#332288', '#88CCEE', '#117733', '#DDCC77', '#CC6677'])

    n, bins = np.histogram((data.flatten()),
                           range=range1, bins=nbins, normed=True)
    pl.plot(bins[1:], n, label=label, color=colors[color_el],
            alpha=alpha)
    X = np.ma.masked_invalid(data)
    X1 = np.ma.masked_equal(X.compressed(), 0.0)
    median_distr = np.percentile(X1.compressed(), 50)
    f = np.interp(median_distr, bins[1:], n)
    pl.plot([median_distr, median_distr], [0, f], '--', color=colors[color_el])
    print(median_distr)

pl.style.use("science")

figures_dir = ("/Users/molnarad/CU_Boulder/Work/Manuscripts/"
               + "2021a_Molnar_et_al_High_freq/git/git/Figures/")
ALMA_flux_file = "ALMA_AC_flux.npz"

aa= np.load(ALMA_flux_file)

Acoustic_flux_Band3 = aa["Acoustic_flux_Band3"]
Acoustic_flux_Band6 = aa["Acoustic_flux_Band6"]

bb = np.load("Ca_flux.npz")
Ca_flux=bb["Ca_flux"]

mask_Band6 = create_circular_mask(358, 358, radius=100)
mask_Band3 = create_circular_mask(158, 158, radius=40)
mask_Ca2   = create_circular_mask(150, 150, radius=28)


pl.figure(dpi=250)

plot_histogram(Ca_flux[42:192, 54:204]*mask_Ca2, "Ca II 854.2 nm",
               [5, 3e3], 300, 2)
plot_histogram(Acoustic_flux_Band3[104:262, 90:248, 0]*mask_Band3, "ALMA Band 3",
               [5, 3e3], 300, 0)
plot_histogram(Acoustic_flux_Band6[:, :, 0]*mask_Band6, "ALMA Band 6",
               [5, 3e3], 300, 4)

pl.legend(fontsize=6)
pl.xlabel("Acoustic flux [W/m$^2$]")
pl.ylabel("Occurence")
pl.yscale("log")
pl.xscale("log")

#pl.savefig(figures_dir+"hist_fluxes.png", transparent=True)

pl.show()
