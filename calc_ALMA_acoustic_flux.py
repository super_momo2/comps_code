#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 13:29:27 2020

Calculate the acoustic flux from the ALMA observations

@author: molnarad
"""

import numpy as np
import scipy as sp
from scipy.ndimage import gaussian_filter
import matplotlib.pyplot as pl
import matplotlib as mp
from astropy.io import fits
from test_color_maps import diverging_colors, qualitative_colors
from matplotlib.colors import ListedColormap
from matplotlib.cm import register_cmap
from scipy import io
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy import signal

from PYBIS import construct_name, load_fft_data
# Load data

def create_circular_mask(h, w, center=None, radius=None):

    if center is None: # use the middle of the image
        center = (int(w/2), int(h/2))
    if radius is None: # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

    mask = dist_from_center <= radius
    return mask

def plot_coord_obs_circle(ax_object, xc, yc, radius, text, color, fontsize):
    """

    plot a circle of a coordinate of an observation
    Parameters
    ----------
    ax_object : axis object to be plotted on
    xc : x center of circle;
        DESCRIPTION.
    yc : y center of a circle;
        DESCRIPTION.
    radius : radius of the circle
        DESCRIPTION
    text : label of the circle
        DESCRIPTION.
    color : color
        DESCRIPTION.
    fontsize : TYPE
        DESCRIPTION.

    Returns
    -------
    fig : TYPE
        DESCRIPTION.
    ax : TYPE
        DESCRIPTION.

    """

    ax_object.add_patch(mp.patches.Circle((xc, yc), radius=radius,
                                          fill=False, edgecolor=color, linewidth=1.75))
    ax_object.text( xc + 11, (yc-radius - 8), text, color=color, fontsize=fontsize)
    return fig, ax

def calc_median_hist(yedges, n):
    '''
    Calculate the median of a histogram column.

    Parameters
    ----------
    yedges : TYPE
        DESCRIPTION.
    n : TYPE
        DESCRIPTION.

    Returns
    -------
    y_edge : TYPE
        DESCRIPTION.

    '''
    y_edge = 0
    n_count = 0
    n_total = np.sum(n)

    while n_count < n_total/2.0:
        n_count = n_count + n[y_edge]
        y_edge = y_edge + 1

    return y_edge

def calc_mean_hist(yedges, n):
    '''
    Calculate the mean of a histogram column.

    Parameters
    ----------
    yedges : y-values of the pixels from the histogram

    n : number of points in the pixel

    Returns
    -------
    y_edge_mean : the mean of the data along the y_dimension for this column
    of the histogram.

    '''

    n_total = np.sum(n)

    y_edge_mean = yedges*n.sum / n_total

    return y_edge_mean

ds = 1
ds3 = ds
ds6 = 1

max_freq = 60e-3
dfreq   = 7e-3
num_freqs = int(max_freq / dfreq) + 1



# Load ALMA data
data_dir  = ("/Users/molnarad/CU_Boulder/Work/Chromospheric_business/"
             + "Comps_2/comps_data/ALMA/")
file_name3 = "ALMA_20170423_band3_t1.sav"
file_name6 = "ALMA_20170423_band6_dc_t1_feathered_snr3.sav"

figures_dir = ("/Users/molnarad/CU_Boulder/Work/Manuscripts/"
               + "2021a_Molnar_et_al_High_freq/git/git/Figures/")

alma_sav3 = io.readsav(data_dir+file_name3)
alma_data_temp3 = alma_sav3["maps"]


alma_sav6 = io.readsav(data_dir+file_name6)
alma_data_temp6 = alma_sav6["maps"]


ALMA_num_t_steps3 = len(alma_data_temp3)
ALMA_num_t_steps6 = len(alma_data_temp6)

FOV2_IBIS_center_x = -79.
FOV2_IBIS_center_y = 252.
IBIS_dx = 0.096
IBIS_dy = 0.096
IBIS_num_x = 9e2
IBIS_num_y = 9e2

rIBIS = [FOV2_IBIS_center_x - IBIS_num_x*IBIS_dx/2,
         FOV2_IBIS_center_x + IBIS_num_x*IBIS_dx/2,
         FOV2_IBIS_center_y - IBIS_num_y*IBIS_dy/2,
         FOV2_IBIS_center_y + IBIS_num_y*IBIS_dy/2]

FOV2_ALMA_Band3_center_x = -83.537
FOV2_ALMA_Band3_center_y = 255.007
FOV2_ALMA_Band6_center_x = -82.1
FOV2_ALMA_Band6_center_y = 251.83
ALMA_num_x = 360 - 2*ds
ALMA_cutout_dx3 = ALMA_num_x
ALMA_cutout_dx6 = 360 - 2*ds6
ALMA_num_y = 360 - 2*ds
ALMA_Band3_dx = 0.33
ALMA_Band6_dx = 0.14

rALMA3 = [FOV2_ALMA_Band3_center_x - ALMA_Band3_dx*ALMA_num_x/2,
          FOV2_ALMA_Band3_center_x + ALMA_Band3_dx*ALMA_num_x/2,
          FOV2_ALMA_Band3_center_y - ALMA_Band3_dx*ALMA_num_x/2,
          FOV2_ALMA_Band3_center_y + ALMA_Band3_dx*ALMA_num_x/2]

#rIBIS1 = rALMA3
#rIBIS1  = rIBIS

rALMA6 = [FOV2_ALMA_Band6_center_x - ALMA_Band6_dx*ALMA_num_x/2,
          FOV2_ALMA_Band6_center_x + ALMA_Band6_dx*ALMA_num_x/2,
          FOV2_ALMA_Band6_center_y - ALMA_Band6_dx*ALMA_num_x/2,
          FOV2_ALMA_Band6_center_y + ALMA_Band6_dx*ALMA_num_x/2]

alma_data3 = np.zeros((ALMA_cutout_dx3, ALMA_cutout_dx3,
                       ALMA_num_t_steps3))
alma_data6 = np.zeros((ALMA_cutout_dx6, ALMA_cutout_dx6,
                       ALMA_num_t_steps6))


for el in range(ALMA_num_t_steps3):
    alma_data3[:, :, el] = alma_data_temp3[el][0][ds3:-1*ds3, ds3:-1*ds3]

for el in range(ALMA_num_t_steps6):
    alma_data6[:, :, el] = alma_data_temp6[el][0][ds6:-1*ds6, ds6:-1*ds6]

# Load the T coefficient

with np.load("T_ALMA_coeffs.npz") as hdu:

    T_ALMA3    = hdu["T_ALMA3"]
    T_ALMA6    = hdu["T_ALMA6"]
    freq_range = hdu["freq_range"]


# Compute the PSD on the spacing of the freq_range

fft_data3 = np.zeros((ALMA_cutout_dx3, ALMA_cutout_dx3,
                       ALMA_num_t_steps3//2 + 1))
fft_data6 = np.zeros((ALMA_cutout_dx6, ALMA_cutout_dx6,
                       ALMA_num_t_steps6//2 + 1))

for ii in range(ALMA_cutout_dx3):
    for jj in range(ALMA_cutout_dx3):
         freq_ALMA3, fft_data3[ii, jj, :] = signal.periodogram(alma_data3[ii, jj, :],
                                                        fs = 1/2.0)

for ii in range(ALMA_cutout_dx6):
    for jj in range(ALMA_cutout_dx6):
         freq_ALMA6, fft_data6[ii, jj, :] = signal.periodogram(alma_data6[ii, jj, :],
                                                        fs = 1/2.0)

dfreq_array = freq_ALMA3
ddfreq = int(dfreq / dfreq_array[1])

ALMA_3_PSD_mean = np.zeros((ALMA_cutout_dx3, ALMA_cutout_dx3,
                            num_freqs))

ALMA_6_PSD_mean = np.zeros((ALMA_cutout_dx6, ALMA_cutout_dx6,
                            num_freqs))

for ii in range(ALMA_cutout_dx3):
    for jj in range(ALMA_cutout_dx3):
        ALMA_3_PSD_mean[ii, jj, :] = [np.mean(fft_data3[ii, jj, i*ddfreq:(i*ddfreq
                                                                  + ddfreq)])
                                      for i in range(num_freqs)]

for ii in range(ALMA_cutout_dx6):
    for jj in range(ALMA_cutout_dx6):
        ALMA_6_PSD_mean[ii, jj, :] = [np.mean(fft_data6[ii, jj, i*ddfreq:(i*ddfreq
                                                                  + ddfreq)])
                                      for i in range(num_freqs)]


# Compute the flux


Acoustic_flux_Band3 = np.zeros((ALMA_cutout_dx3, ALMA_cutout_dx3, 4))
Acoustic_flux_Band6 = np.zeros((ALMA_cutout_dx6, ALMA_cutout_dx6, 4))

for ii in range(ALMA_cutout_dx3):
    for jj in range(ALMA_cutout_dx3):
        Acoustic_flux_Band3[ii, jj, 0] = (T_ALMA3[-2, 1:]
                                          * ALMA_3_PSD_mean[ii, jj, 1:]
                                          * dfreq).sum()
        Acoustic_flux_Band3[ii, jj, 1] = (T_ALMA3[-2, 1:3]
                                          * ALMA_3_PSD_mean[ii, jj, 1:3]
                                          * dfreq).sum()
        Acoustic_flux_Band3[ii, jj, 2] = (T_ALMA3[-2, 3:6]
                                          * ALMA_3_PSD_mean[ii, jj, 3:6]
                                          * dfreq).sum()
        Acoustic_flux_Band3[ii, jj, 3] = (T_ALMA3[-2, 6:]
                                          * ALMA_3_PSD_mean[ii, jj, 6:]
                                          * dfreq).sum()


for ii in range(ALMA_cutout_dx6):
    for jj in range(ALMA_cutout_dx6):
        Acoustic_flux_Band6[ii, jj, 0] = (T_ALMA6[-2, 1:]
                                          * ALMA_6_PSD_mean[ii, jj, 1:]
                                          * dfreq).sum()
        Acoustic_flux_Band6[ii, jj, 1] = (T_ALMA6[-2, 1:3]
                                          * ALMA_6_PSD_mean[ii, jj, 1:3]
                                          * dfreq).sum()
        Acoustic_flux_Band6[ii, jj, 2] = (T_ALMA6[-2, 3:6]
                                          * ALMA_6_PSD_mean[ii, jj, 3:6]
                                          * dfreq).sum()
        Acoustic_flux_Band6[ii, jj, 3] = (T_ALMA6[-2, 6:]
                                          * ALMA_6_PSD_mean[ii, jj, 6:]
                                          * dfreq).sum()
# Plot the flux

cutout = 1

fig, ax = pl.subplots(1, 2, dpi=300)
im1 = ax[0].imshow(np.log10(Acoustic_flux_Band3[:, :, 0]),
                   origin="lower", cmap='RdBu', vmin=2, vmax=4,
                   extent=[rALMA3[0], rALMA3[1], rALMA3[2], rALMA3[3]])
fig.colorbar(im1, label="Log$_{10}$[Acoustic Flux [W/m$^2$]] ", ax=ax[0],
             shrink=0.6)
ax[0].set_ylabel("Solar Y [arcsec]")
ax[0].set_xlabel("Solar X [arcsec]")

im2 = ax[1].imshow(np.median(alma_data3, axis=2),
                   cmap="plasma", origin="lower",
                   extent=[rALMA3[0], rALMA3[1], rALMA3[2], rALMA3[3]])
fig.colorbar(im2, label="Temperature [K]", ax=ax[1],
             shrink=0.6)
ax[1].set_xlabel("Solar X [arcsec]")
#fig.suptitle("Band 3")
pl.tight_layout()
pl.savefig(figures_dir + "ALMA_Band3_AC_flux.png", transparent=True)
pl.show()

fig, ax = pl.subplots(1, 2, dpi=300)
im1 = ax[0].imshow(np.log10(Acoustic_flux_Band6[:, :, 0]),
                   origin="lower", cmap='RdBu', vmin=2, vmax=4,
                   extent=[rALMA6[0], rALMA6[1], rALMA6[2], rALMA6[3]])
fig.colorbar(im1, label="Log$_{10}$[Acoustic Flux [W/m$^2$]] ", ax=ax[0],
             shrink=0.6)
ax[0].set_ylabel("Solar Y [arcsec]")
ax[0].set_xlabel("Solar X [arcsec]")

im2 = ax[1].imshow(np.median(alma_data6, axis=2),
                   cmap="plasma", origin="lower",
                   extent=[rALMA6[0], rALMA6[1], rALMA6[2], rALMA6[3]],
                   vmin=5000, vmax=9000)
fig.colorbar(im2, label="Temperature [K]", ax=ax[1],
             shrink=0.6)
ax[1].set_xlabel("Solar X [arcsec]")
#fig.suptitle("Band 6")
pl.tight_layout()
pl.savefig(figures_dir + "ALMA_Band6_AC_flux.png", transparent=True)
pl.show()

mask_Band3 = create_circular_mask(358, 358, radius= 110)
mask_Band6 = create_circular_mask(358, 358, radius= 95)
cutout = 1

fig, ax = pl.subplots(1, 2, dpi=300, figsize=(7, 4.4))
im1 = ax[0].imshow(np.log10(Acoustic_flux_Band3[:, :, 0])*mask_Band3,
                   origin="lower", cmap='RdBu', vmin=1.5, vmax=3.25,
                   extent=[rALMA3[0], rALMA3[1], rALMA3[2], rALMA3[3]])
fig.colorbar(im1, label="Log$_{10}$[Acoustic Flux [W/m$^2$]] ", ax=ax[0],
             shrink=0.5)
plot_coord_obs_circle(ax[0], FOV2_ALMA_Band6_center_x,
                      FOV2_ALMA_Band6_center_y, 13.3, "ALMA\nBand 6",
                      "black", 8)
ax[0].set_ylabel("Solar Y [arcsec]")
ax[0].set_xlabel("Solar X [arcsec]")
ax[0].set_title("Band 3")

im1 = ax[1].imshow(np.log10(Acoustic_flux_Band6[:, :, 0])*mask_Band6,
                   origin="lower", cmap='RdBu', vmin=1.5, vmax=3.25,
                   extent=[rALMA6[0], rALMA6[1], rALMA6[2], rALMA6[3]])
fig.colorbar(im1, label="Log$_{10}$[Acoustic Flux [W/m$^2$]] ", ax=ax[1],
             shrink=0.5)
ax[1].set_ylabel("Solar Y [arcsec]")
ax[1].set_xlabel("Solar X [arcsec]")

ax[1].set_title("Band 6")
pl.tight_layout()
pl.savefig(figures_dir + "ALMA_AC_flux.png", transparent=True)
pl.show()

np.savez("ALMA_AC_flux.npz", Acoustic_flux_Band3=Acoustic_flux_Band3,
         Acoustic_flux_Band6=Acoustic_flux_Band6)
'''
fig, ax = pl.subplots(2, 1, dpi=250, figsize=(7.1, 2*4.4))
h1, xedges1, yedges1, im = ax[0].hist2d(np.median(alma_data3[cutout:-1*cutout,
                                                           cutout:-1*cutout, :],
                                        axis=2).flatten(),
                                        np.log10(Acoustic_flux_Band3[cutout:-1*cutout,
                                                                     cutout:-1*cutout,
                                                                     0]* mask_Band3).flatten(),
                                        bins=(50, 50),
                                        range=[[6500, 11500], [1.8, 3.5]])
mean_P1 = np.array([calc_median_hist(yedges1, el) for el in h1])
mean_P1 = yedges1[mean_P1]
ax[0].plot(xedges1[1:], mean_P1, 'r--',  linewidth=2.5)
ax[0].set_title("Band 3 (3.0 mm)")


h1, xedges1, yedges1, im = ax[1].hist2d(np.median(alma_data6[cutout:-1*cutout,
                                                           cutout:-1*cutout, :]* mask_Band6,
                                        axis=2).flatten(),
                                        np.log10(Acoustic_flux_Band6[cutout:-1*cutout,
                                                 cutout:-1*cutout, 0]* mask_Band6).flatten(),
                                        bins=(50, 50),
                                        range=[[6000, 8500], [1.8, 3.5]])
mean_P1 = np.array([calc_median_hist(yedges1, el) for el in h1])
mean_P1 = yedges1[mean_P1]
ax[1].plot(xedges1[1:], mean_P1, 'r--', linewidth=2.5)
ax[1].set_title("Band 6 (1.25 mm)")

ax[1].set_xlabel("Brightness Temperature [K]")
ax[0].set_ylabel("Log$_{10}$[Acoustic Flux [W/m$^2$]]")
ax[1].set_ylabel("Log$_{10}$[Acoustic Flux [W/m$^2$]]")
pl.savefig(figures_dir + "ALMA_AC_flux_vs_temperature.png", transparent=True)
'''


fig, ax = pl.subplots(2, 3, dpi=250, figsize=(10, 6))

for el in range(3):
    im = ax[0, el].imshow(Acoustic_flux_Band3[:, :, (el+1)]/Acoustic_flux_Band3[:, :, 0],
                          vmin=0, vmax=1,
                          extent=[rALMA3[0], rALMA3[1], rALMA3[2], rALMA3[3]],
                          cmap='brg')

    #ax[el].set_xlabel('Solar X, [arcsec]')
    #ax[0, el].set_xlim(x_left_lim, x_right_lim-5.0)
    #ax[0, el].set_ylim(y_lower_lim+5, y_upper_lim)

ax[0, 0].set_title("5-20 mHz")
ax[0, 1].set_title("20-40 mHz")
ax[0, 2].set_title("40-60 mHz")

divider = make_axes_locatable(ax[0, 2])
cax = divider.append_axes("right", size="5%", pad=0.1)
cbar = fig.colorbar(im, cax=cax, label="Relative flux contribution")
ax[0, 0].set_ylabel('Solar Y [arcsec]')
ax[1, 0].set_ylabel("Solar Y [arcsec]")

for el in range(3):
    im = ax[1, el].imshow(Acoustic_flux_Band3[:, :, el+1],
                          extent=[rALMA3[0], rALMA3[1], rALMA3[2], rALMA3[3]],
                          cmap='bwr')

    ax[1, el].set_xlabel('Solar X [arcsec]')
    # ax[1, el].set_xlim(x_left_lim, x_right_lim-5.0)
    # ax[1, el].set_ylim(y_lower_lim+5, y_upper_lim)

divider = make_axes_locatable(ax[1, 2])
cax = divider.append_axes("right", size="5%", pad=0.1)
cbar = fig.colorbar(im, cax=cax, label="Acoustic flux W/m$^2$")

fig.suptitle("Band 3")
pl.savefig(figures_dir+"ALMA_Acoustic_flux_contributions_Band3.png",
           transparent=True,
           bbox_inches='tight')

pl.show()


fig, ax = pl.subplots(2, 3, dpi=250, figsize=(10, 6))

for el in range(3):
    im = ax[0, el].imshow(Acoustic_flux_Band6[:, :,
                                              (el+1)]/Acoustic_flux_Band6[:, :, 0],
                          vmin=0, vmax=1,
                          extent=[rALMA6[0], rALMA6[1], rALMA6[2], rALMA6[3]],
                          cmap='brg')

    #ax[el].set_xlabel('Solar X, [arcsec]')
    #ax[0, el].set_xlim(x_left_lim, x_right_lim-5.0)
    #ax[0, el].set_ylim(y_lower_lim+5, y_upper_lim)

ax[0, 0].set_title("5-20 mHz")
ax[0, 1].set_title("20-40 mHz")
ax[0, 2].set_title("40-60 mHz")

divider = make_axes_locatable(ax[0, 2])
cax = divider.append_axes("right", size="5%", pad=0.1)
cbar = fig.colorbar(im, cax=cax, label="Relative flux contribution")
ax[0, 0].set_ylabel('Solar Y [arcsec]')
ax[1, 0].set_ylabel("Solar Y [arcsec]")

for el in range(3):
    im = ax[1, el].imshow(Acoustic_flux_Band6[:, :, el+1],
                          extent=[rALMA6[0], rALMA6[1], rALMA6[2], rALMA6[3]],
                          cmap='bwr')

    ax[1, el].set_xlabel('Solar X [arcsec]')
    # ax[1, el].set_xlim(x_left_lim, x_right_lim-5.0)
    # ax[1, el].set_ylim(y_lower_lim+5, y_upper_lim)

divider = make_axes_locatable(ax[1, 2])
cax = divider.append_axes("right", size="5%", pad=0.1)
cbar = fig.colorbar(im, cax=cax, label="Acoustic flux W/m$^2$")

fig.suptitle("Band 6")
pl.savefig(figures_dir+"ALMA_Acoustic_flux_contributions_Band6.png",
           transparent=True,
           bbox_inches='tight')

pl.show()

fig, ax = pl.subplots(2, 3, dpi=250, figsize=(10, 6))

for el in range(3):
    im = ax[0, el].imshow(Acoustic_flux_Band3[:, :, (el+1)]/Acoustic_flux_Band3[:, :, 0] * mask_Band3,
                          vmin=0, vmax=1,
                          extent=[rALMA3[0], rALMA3[1], rALMA3[2], rALMA3[3]],
                          cmap='bwr')

    #ax[el].set_xlabel('Solar X, [arcsec]')
    #ax[0, el].set_xlim(x_left_lim, x_right_lim-5.0)
    #ax[0, el].set_ylim(y_lower_lim+5, y_upper_lim)

ax[0, 0].set_title("5-20 mHz")
ax[0, 1].set_title("Band 3 \n20-40 mHz")
ax[0, 2].set_title("40-60 mHz")

divider = make_axes_locatable(ax[0, 2])
cax = divider.append_axes("right", size="5%", pad=0.1)
cbar = fig.colorbar(im, cax=cax, label="Relative flux contribution")
ax[0, 0].set_ylabel('Solar Y [arcsec]')

ax[1, 0].set_ylabel("Solar Y [arcsec]")
ax[1, 1].set_title("Band 6")
for el in range(3):
    im = ax[1, el].imshow(Acoustic_flux_Band6[:, :, (el+1)]/Acoustic_flux_Band6[:, :, 0] * mask_Band6,
                          vmin=0, vmax=1,
                          extent=[rALMA6[0], rALMA6[1], rALMA6[2], rALMA6[3]],
                          cmap='bwr')

    ax[1, el].set_xlabel('Solar X [arcsec]')
    # ax[1, el].set_xlim(x_left_lim, x_right_lim-5.0)
    # ax[1, el].set_ylim(y_lower_lim+5, y_upper_lim)

divider = make_axes_locatable(ax[1, 2])
cax = divider.append_axes("right", size="5%", pad=0.1)
cbar = fig.colorbar(im, cax=cax, label="Relative flux contribution")

pl.savefig(figures_dir+"ALMA_Acoustic_flux_contributions.png",
           transparent=True,
           bbox_inches='tight')

pl.show()
