#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 02:35:10 2020

@author: momo
"""


import numpy as np
import matplotlib.pyplot as plt
from RHlib import RADYN_atmos, calc_v_lc, calc_v_cog
from scipy import signal

RADYN_models = ["radyn_ulmschneider_250_low_dt.cdf",
                "radyn_ulmschneider_625_low_dt.cdf",
                "radyn_ulmschneider_1200_low_dt.cdf",
                "radyn_ulmschneider_3000_low_dt.cdf",
                "radyn_ulmschneider_7500_low_dt.cdf", 
                "radyn_ulmschneider_19000_low_dt.cdf"]

data_dir = "/mnt/Data/Comps_2/comps_data/RADYN_runs/"

labels = ["model_250", "model_625", "model_1200",
          "model_3000", "model_7500", "model_19000"]

sp_line_index = 20 # 20 - Ca II 8542 Å; 2 - Ha 6563 Å

start_index_all = [600, 600, 700, 750, 500, 500]
end_index_all   = [-10, -10, -100, -150, -660, -660]
dt_sim      = 3
count_every = 1

height_v = 1.15e3 


if sp_line_index == 2:
    vel_coefficient = 3e5/6563
if sp_line_index == 20:
    vel_coefficient = 3e5/8542
    

plt.figure(dpi=250)

for el in [0, 1, 2, 3, 4, 5]:
    start_index = start_index_all[el]
    end_index   = end_index_all[el] 
    
    rad = RADYN_atmos(data_dir + RADYN_models[el])
    
    rad.calc_sp_line(sp_line_index)

    int_grid_scale = np.linspace(rad.sp_line_wave[28], rad.sp_line_wave[-28],
                             num=100)
    line_intensity_int = [np.interp(int_grid_scale, rad.sp_line_wave, I) for I in 
                      rad.sp_line_I]
    line_intensity_int = np.array(line_intensity_int)

    Ha_velocity = [calc_v_lc(int_grid_scale, I, 10, -10, 13)[0] for I
               in line_intensity_int[start_index:end_index:count_every, 1:-1]]

    Ha_velocity_metric = vel_coefficient * (np.array(Ha_velocity)
                                            - np.mean(Ha_velocity))
    
    for el1 in range(len(Ha_velocity_metric)):
        if (Ha_velocity_metric[el1]) > 10:
            Ha_velocity_metric[el1] = 0
        elif (Ha_velocity_metric[el1]) < -10:
            Ha_velocity_metric[el1] = 0

    Ha_v_freq, Ha_v_PSD = signal.periodogram(Ha_velocity_metric,
                                         fs=1/dt_sim/count_every) 

    
    rad.calc_Flux_PSD(int((start_index + end_index)/2), 
                      int((end_index - start_index)/2), 
                      height_v, dt_sim)
    num_points = 40
    ratio_PSDs = rad.Flux_eq_PSD/Ha_v_PSD
    ratio_PSDS_mean = np.zeros(num_points)
    ratio_PSDS_freq = np.zeros(num_points)
    
    for el1 in range(num_points):
        ratio_PSDS_mean[el1] = np.mean(ratio_PSDs[(el1*4+4):(el1*4+8)])
        ratio_PSDS_freq[el1] = np.mean(Ha_v_freq[(el1*4+4):(el1*4+8)])
    
    plt.plot(ratio_PSDS_freq*1e3, ratio_PSDS_mean, 'o--', label=labels[el])


plt.xlim(3.5, 70)
plt.xlabel("Frequency [mHz]")
plt.ylabel("PSD[Acoustic Flux @ 1150 km] / PSD[Vel.lc Ca II 8542 ${\AA}$]")
plt.legend(fontsize=6)
plt.grid()
plt.yscale("log")
#plt.tight_layout()
#plt.savefig("")
plt.show()

     