#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 16:38:29 2020

Calculate the noise floor for the PSDs of different diagnostics using
the mask describerd in Section 3 of the manuscript.

@author: molnarad
"""

import numpy as np
import scipy as sp
from scipy.ndimage import gaussian_filter
import matplotlib.pyplot as pl
from astropy.io import fits
from test_color_maps import diverging_colors
from matplotlib.colors import ListedColormap
from matplotlib.cm import register_cmap
import matplotlib.colors as mcolors

from PYBIS import construct_name, load_fft_data_simple, count_zeroes_in_array


pl.style.use('science')

data_dir = ('/Users/molnarad/CU_Boulder/Work/Chromospheric_business/'
            + 'Comps_2/comps_data/')

ab = sp.io.readsav(data_dir + 'IBIS/FOV2_widths_Ha.sav')
ha_width_data = np.array(ab['widths'])

spec_property = 'vel.lc'
time_stamp    = 170444
freq, fft_data = load_fft_data_simple(data_dir, spec_property, time_stamp)

lim_1 = -40
lim_2 = -1
dx    = 20 # border to be cut around the edges to remove edge garbage


#fft_HF_noise = np.mean(np.log10(fft_data[lim_1:lim_2,
#                                         dx:-dx, dx:-dx]), axis=0)

fft_HF_noise = np.log10(np.mean(fft_data[lim_1:lim_2,
                                         dx:-dx, dx:-dx], axis=0))


pl.figure(dpi=250, figsize=(4, 2.8))

labels = ['plage', 'network', 'fibrils', 'internetwork', 'penumbra']
#labels = ['penumbra']
#labels = ['network']

masks_file = 'Masks_FOV2.npz'
fft_HF_noise_all = np.zeros((len(labels), fft_HF_noise.shape[0]-2*dx,
                             fft_HF_noise.shape[1]-2*dx))

i = 0

colors = np.flip(['#332288', '#88CCEE', '#117733', '#DDCC77', '#CC6677'])


fig, ax = pl.subplots(2, 1, dpi=250, figsize=(4.5, 2*3.05))

for mask_el in labels:

    with np.load(masks_file) as file:
        mask = (np.array(file[mask_el]))[dx:-dx, dx:-dx]

    fft_HF_noise_all[i, :, :] = (mask * fft_HF_noise)[dx:-dx, dx:-dx]

    noise_levels = np.ma.masked_equal(fft_HF_noise_all[i, :, :], 0.0)
    noise_levels = noise_levels.compressed()

    n, bins = np.histogram(noise_levels,
                           bins=100, range=[-5.2, -.1],
                           density=True)
    mean_distr = np.median(noise_levels)
    perc10 = np.percentile(noise_levels, 10)
    perc90 = np.percentile(noise_levels, 90)

    print(f"The noise floor of the {mask_el} region is {mean_distr:.2f}"
          + f" +{perc90 - mean_distr:.2f} - {perc10 - mean_distr:.2f}")
    ax[0].plot(bins[1:], n, color=colors[i], label=mask_el)
    f = np.interp(mean_distr, bins[1:], n)
    ax[0].plot([mean_distr, mean_distr], [0, f], '--', color=colors[i])

    i = i + 1

for el in range(len(labels)):
    filename = "Pxx_ROS_" + labels[el] + ".npz"
    foobar = np.load(filename)
    Pxx = foobar["Pxx"]
    Pxx_mean = np.log10(np.mean(Pxx, axis=(1, 2)))

    n, bins = np.histogram(Pxx_mean,
                           bins=20, range=[-5.2, -.1],
                           density=True)
    mean_distr = np.median(Pxx_mean)
    perc10 = np.percentile(Pxx_mean, 10)
    perc90 = np.percentile(Pxx_mean, 90)

    print(f"The noise floor for {mask_el} region simulation is {mean_distr:.2f}"
          + f" +{perc90 - mean_distr:.2f} - {perc10 - mean_distr:.2f}")
    ax[1].plot(bins[1:], n, color=colors[el], label=labels[el])
    f = np.interp(mean_distr, bins[1:], n)
    ax[1].plot([mean_distr, mean_distr], [0, f], '--', color=colors[el])


for el in ax.flatten():
    el.set_xlim(-5.2, -0.1)
    el.set_ylim(1e-3, 2.5)
    el.set_ylabel('Occurence')
    el.set_yscale('log')
    el.grid(alpha=0.3)
    el.legend(prop={'size': 8})

ax[0].set_title("Observations")
ax[1].set_title("Photon-Noise Estimation")
ax[1].set_xlabel('Log$_{10}$(HF White Noise Floor [(km/s)$^{2}$/mHz])')


pl.tight_layout()
pl.savefig('/Users/molnarad/CU_Boulder/Work/Manuscripts/'
           + '2021a_Molnar_et_al_High_freq/git/git/Figures/HF_noise_levels.png')

pl.show()
